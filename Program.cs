﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Reflection;

namespace iERC_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine($"Version: " +$"{Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}");
            CreateWebHostBuilder(args).Build().Run();                               
        }

        class Foo
        {
            public string GetAssemblyVersion()
            {
                return GetType().Assembly.GetName().Version.ToString();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
