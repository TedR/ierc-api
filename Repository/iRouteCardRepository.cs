using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers {


    public interface IRouteCardRepository
    {
        // Gets list of WOQs, parameters: route card active, batch visible, stock reserved, GNC period
        Task<List<LiveBatches>> LiveBatches(Boolean rca, Boolean vis, Boolean alloc, int period, int batch);

        // Gets list of Processes, parameters: 
        // batch number, TLP (TLP = 1, SLP = 0 or ALL = -1 ), un_ix = USER
        Task<List<ProcessModel>> Processes(int batch, int tlp, int un_ix, Boolean myProc);
        
        // Gets s list of processes for given user which are < 100% complete, order by Process Status DESC
        // 2 = Paused
        // 1 = Started
        // 0 - Not Started
        Task<List<ProcessModel>> FilteredProcesses(int batch, int un_ix, Boolean myProc);

        // Gets list of open GNCs for pin number family for given period
        Task<List<GNC>> GNCs(string pin, int period, int userID);

        // Gets list of AOI Images if available with file PTH to open
        Task<List<AOIImage>> AOIImages(string pin);

        // Gets list of AOI Images by PMR if available with file PTH to open
        Task<List<AOIImage>> AOIImagesByPMR(string pin, string pmr);

        PhysicalFileResult GetImage(PCBImage image);

        // Returns PDF of Drawing
        PhysicalFileResult GetPDF(PCBImage image);

    
        // Get Stoppage State        
        Task<List<TimeState>> GetTimeState(Boolean showAll);

        // Get Stoppage Reason Codes        
        Task<List<TimeReason>> GetTimeReasons(Boolean showAll);

        // Get Stoppage Monitor Records       
        Task<List<TimeMonitor>> GetTimeMonitor(int rch, int prc, int rci, int userID);

        // Get Consumable Records       
        Task<List<Consumable>> GetConsumable(int rch, int prc, int rci);

        // Get Legacy Mappings for bitfields       
        Task<List<LegacyMap>> GetLegacyMapping(int prs_idx, int prs_step_ix);

        // Get Trace Code for Consumable Records       
        Task<List<TraceCode>> GetTraceCode(TraceCode traceCode);

        // Get Measurement for Process Step
        Task<List<Measurement>> GetMeasurement(Int32 prc_ix);

        // Insert measurement and returns new dataset
        Task<List<Measurement>> InsertMeasurement(Measurement measurement);

        // Get Panel Layout Details - Fids + Coordinates
        Task<List<PanelLayout>> GetPanelLayout(int rch);

        // Get list of planner timeline nodes
        Task<List<ProdPlan>> PlannerNodes(int batch);

        // Get PPM/Yield Ranking for PCBA
        // ppm 230 = 5 Sigma
        // ar = yield (unique serial # defects v PCBAs)
        // rl = rework cost
        // pr = period in years
        // pin = PCBA
        // byFam = option to include entire family
        Task<List<YieldCube>> YieldRanking(int ppm, int ar, int rl, int pr, string pin, Boolean byFam);
        
        // Gets list of drawings for Job
        Task<List<DrawingModel>> Drawings(int hdrIX);

        // Gets list of drawings for Specific Process
        Task<List<DrawingModel>> DrawingsByProcess(int hdrIX, int prcIX);

        // Secure Login using POST Method - AD Verification
        LoginReturn SecureLogin(LoginCredentialsClass userCredentials);

        Task<LoginReturn> AddDeviceDetails(LoginCredentialsClass userCredentials);

        // Get list of User Roles
        Task<List<UserRoles>> UserRoles(Boolean elevated);

        // Get list of User Request Notifications
        Task<List<RequestNotification>> RequestNotifications(string status);

        // Add Request Notification using POST Method
        Task<POSTStatus> AddRequestNotification(RequestNotification requestNotification);
        
        // Update Request Notification using POST Method
        Task<POSTStatus> UpdateRequestNotification(RequestNotification requestNotification);

        // Gets User Sign Offs
        // This is a list of requested user sign offs, typically for PTH, MECH or Conformal Coat
        Task<List<SignOff>> GetSignOffs(int authorisedUser, int batchNumber, bool showCompleted);

        // Gets a list of process comments
        // If stepIX = -1 then returns all comments for batch identified by hdrIX
        Task<List<ProcessComment>> GetProcessComments(int hdrIX, int stepIX);

        // Add process comment using POST Method
        Task<POSTStatus> AddProcessComment(ProcessComment processComment);

        // Returns feedback intended for Front End Engineers
        Task<List<FeedbackModel>> GetFeedback(int hdrIX);

        // Add Feedback using POST Method
        Task<POSTStatus> UpdateFeedback(FeedbackModel processComment);

        // Add UA of Viewing Alert or GNC using POST Method
        Task<POSTStatus> AddUAConfirmation(UAConfirmation uac);

        Task<Boolean> UAConfirmation(int hdrIX, int userID, int refIX, int stepIX = -1);

        Task<List<Shortage>> GetShortagesAsync (int hdrIX, int prcIX);

        Task<List<ScanHistory>> GetScanHistoryAsync(int stepIX, int stIX, int bhIX);

        Task<POSTStatus> AddShortagesAsync(Shortage shortage);

        Task<POSTStatus> UpdateShortagesAsync(Shortage shortage);

        Task<POSTStatus> AddScanHistory(ScanHistory history);        

        Task<POSTStatus> AddUserDataHistory(ScanHistory history);

        Task<POSTStatus> AddUserSignoff(SignOff signOff);

        Task<POSTStatus> UpdateUserSignoff(SignOff signOff);

        Task<POSTStatus> AddUserConfirmationHistory(ScanHistory history);

        Task<POSTStatus> AddTimeMonitor(TimeMonitor timeMonitor);

        Task<POSTStatus> AddConsumable(Consumable consumable);

        Task<POSTStatus> UpdateInstruction(RCI_Instruction instruction);

        // WIP - Creates stub records for iOracle Inspection Scan
        Task<Boolean> CreateOracleStub(ScanHistory history);

        // TO DO - Create legacy serial records for process scan
        Task<Boolean> CreateLegacyStub(ScanHistory history);
        
        // Required for translating Client Serial Number to Norcott
        Task<String> TranslateSerial(PSNSerial serial);

        // Get Top Level Serial if Bonded Assy
        Task<String> GetTopLevelSerial(PSNSerial serial);

        // Get QOB for given serial number
        Task<Int32> GetSerialQOB(Int32 serialIndex);

        // Checks to see if previous step scanned
        Task<PreviousScanInfo> CheckPreviousProcessScan(Int32 prcIndex, Int32 snIndex, Int32 scanType, Int32 panelIndex);

        // Validate that the amount being added is not greater than added at the previous BATCH SCAN step.
        Task<PreviousBatchInfo> CheckPreviousBatchScan(Int32 prcIndex, Int32 bhIndex);

        // Gets the QOB & Panel Scan Index 
        Task<PanelScanMultiplier> GetPanelScanMultiplier(int serialIndex);

        // Gets QTY QA Alerts to view at WOC (Batch) Level
        Task<Int32> GetQtyBatchQAAlert(Int32 rchIndex, Int32 bhIndex);

        // Gets QA Alerts to view at WOC (Batch) Level
        Task<List<QAAlert>> BatchLevelQAAlert(Int32 rchIndex, Int32 bhIndex);

        // Inserts QA Alert ACK
        Task<POSTStatus> InsertQAAlert(QAAlertAck alertAck);

        // Allows Engineer to Close QA Alert on tablet
        Task<POSTStatus> CloseQAAlert(QAAlert alert);

        // Gets QA Alerts to view at WOC (Batch) Level
        Task<List<QAAlert>> ProcessLevelQAAlert(Int32 rchIndex, Int32 prcIndex, Int32 userIndex);

        // Gets BOM Data by Batch / Placement
        Task<List<BOM>> GetBOM(Int32 batch, String placement);

        // Gets BOM Pin Data by Batch
        Task<List<BOMPin>> GetBOMPinData(Int32 batch);
        // Gets Placement Positions
        Task<List<Placement>> GetPlacement(int _batch);

    }

}