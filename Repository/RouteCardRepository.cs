using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Concurrent;
using Microsoft.AspNetCore.Mvc;
using System.DirectoryServices.AccountManagement;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore.Update;

namespace iERC_API.Controllers
{


    public class RouteCardRepository : ControllerBase, IRouteCardRepository
    {
        private readonly IConfiguration _config;

        private readonly BlockingCollection<FileInfo> collection = new BlockingCollection<FileInfo>();

        public RouteCardRepository(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {

                return new SqlConnection(_config.GetConnectionString("EFModel"));

            }
        }


        async Task<List<YieldCube>> IRouteCardRepository.YieldRanking(int ppm, int ar, int rl, int pr, string pin, Boolean byFam)
        {
            var p = new DynamicParameters();
            string sql = @" DECLARE @IPPM int, @IAR FLOAT, @IRL DECIMAL, @IPR INT, @IPERIOD DATE, @IDS DATE, @IDE DATE, @IPIN NVARCHAR(20);
                            SELECT @IPPM = @PPM, @IAR = @AR, @IRL = @RL, @IPR = @PR, @IDE = GETDATE(), @IPIN = @PIN,
		                    @IPERIOD = DATEADD(year, - @IPR, GETDATE());";
            p.Add("@PPM", ppm, DbType.Int32);
            p.Add("@AR", ar, DbType.Int32);
            p.Add("@RL", rl, DbType.Int32);
            p.Add("@PR", pr, DbType.Int32);
            p.Add("@PIN", pin, DbType.String);
            p.Add("@BYFAMILY", byFam, DbType.Boolean);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<YieldCube>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }


        async Task<List<ProdPlan>> IRouteCardRepository.PlannerNodes(int batch)
        {
            var p = new DynamicParameters();
            string sql = @" SELECT nodes.JobIndex, nodes.JobNo, nodes.BatchNo, nodes.Line, nodes.PQty, nodes.Qty, nodes.Description, nodes.Comment,
                            nodes.TargetStartDate, nodes.TargetDeliveryDate, nodes.TotalDuration, nodes.OPIndex, nodes.WSIndex, nodes.NodeIndex, nodes.ColourCode, 
                            nodes.Operation, COALESCE(CAST( (Convert(float, scans.Scans) / Convert(float, nodes.Qty) * 100) as DECIMAL(18, 2)), 0) as PercentComplete, 
                            COALESCE(scans.Scans, 0) as Scans, nodes.StartTime, nodes.EndTime
                            FROM (
                            SELECT js.IDX_JobSetup as JobIndex, js.JobNo, js.BatchNo, js.Line, js.PQty, js.Qty, js.Description, js.Comment, 
                            js.TargetStartDate, js.TargetDeliveryDate, jr.Duration as TotalDuration, jr.PercentComplete, jr.OP_IDX as OPIndex, jr.WS_IDX as WSIndex, 
                            op.ColourCode, (
                            CASE WHEN op.Operation = 'PTH' THEN 'PTH SCAN' 
                                ELSE
                                CASE WHEN op.Operation = 'PTH-INSP' THEN 'PTH Inspection'
                                    ELSE
                                    CASE WHEN op.Operation = 'SMT' THEN 'SMT SCAN'
                                        ELSE
                                        CASE WHEN op.Operation = 'TEST' THEN 'TEST SCAN'
                                        ELSE
                                            CASE WHEN op.Operation = 'SMT-INSP' THEN 'SMT Inspection'
                                        ELSE op.Operation
                                    END
                                END
                                END
                                END
                            END) as Operation, 
                            n.IDX_Nodes as NodeIndex, Convert(DateTime2(2), n.Start_Time)as StartTime, Convert(DateTime2(2), n.End_Time) as EndTime   
                            FROM dbo.jobsetup js
                            inner join dbo.jobroute jr on jr.jobsetup_IDX_JobSetup = js.IDX_JobSetup
                            inner join dbo.operations op on op.IDX_Operations = jr.OP_IDX
                            inner join dbo.WS_Nodes n on n.ws_station_IDX = jr.idx_jobRoute
                            WHERE js.BatchNo = @BATCH) nodes  
                            left outer join (   
                                SELECT Coalesce(s.SAT_Station, 'Not Defined') AS Station, 
                                Count(distinct p.PSN_IX * p.PSN_QOB) AS Scans
                                FROM dbo.Serial_SAT_Audit_Trail s
                                INNER JOIN dbo.Serial_PSN_Product_Serial_Numbers p on p.PSN_IX = s.SAT_PSN_IX 
                                WHERE p.PSN_Batch_Number = @BATCH and p.PSN_PSS_IX <> 3
                                GROUP BY s.SAT_Station
                                HAVING s.SAT_Station In ('SMT Scan', 'SMT Inspection', 'PTH Scan', 'PTH Inspection', 'Mechanical', 'Test Scan')) scans	
                                ON scans.Station = nodes.Operation";
            p.Add("@BATCH", batch, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<ProdPlan>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<LiveBatches>> IRouteCardRepository.LiveBatches(Boolean rca, Boolean vis, Boolean alloc, int period, int batch)
        {
            var p = new DynamicParameters();
            string sql = @"	Select * From (
                Select Distinct 
                            BH_PIN as PinNumber,  BH_Reserved as StockAllocated, jh.JH_Job_Number as JobNumber, jh.JH_IX as JobIndex, 
                            BH_Number as BatchNumber, Coalesce(BH_Client, '') as ClientCode, Coalesce(gp.GPIN_Client_Part_No, '') as ClientPart,
                            BH_Build_Date as BuildDate, gp.GPIN_Description as GDescription, 
                            BH_Qty as Quantity, Coalesce(BH_Comment, '') as BatchComment, jh.JH_Description as JobDescription, 
                            jh.JH_Client_Order_No as ClientOrder,  BH_First_Serial as FirstSerial, BH_Complete as Complete, 
                            BH_ERC_View as Visible, BH_PMR as PMR,
                            RCH_Created as QMPCreated, RCH_By as QMPBy, BH_Assigned as Engineer, BH_IX as BatchIndex, PRH.RCH_IX as RouteCardHdrIndex,
                            gp.GPIN_Alert as Alert, gp.GPIN_Comment as AlertComment, RCH_Instructions as ERCInstructions, 
                            gp.GPIN_AOI_Data as AOI, gp.GPIN_IX as GPinIndex, PT_Type as Finish,                                                                                                             
                            (Select Count(*) FROM dbo.qms_gnc_log l
                            inner join dbo.qms_gnc_report r on r.[QMS_GNC_LOG_QMS_GNC_LOG_IDX] = l.QMS_GNC_LOG_IDX
                            WHERE                             
                                    CASE 
                                        WHEN (l.GNC_LOG_Pin like SUBSTRING(BH_PIN, 0, LEN(BH_PIN)-4)  + '%') THEN 1 
                                        ELSE 0
                                    END = 1
                                    AND (l.GNC_LOG_Status = 'Open' AND l.GNC_LOG_DateRaised >= dateadd(month, -(@PERIOD), CAST(getdate() AS DATE)))  
                            ) as GNCS,					
                            RCH_IPC_Class as [Class],
                            RCH_Panel_Layout as [Step],

                            COALESCE(
							   (SELECT COUNT(DISTINCT a.QA_IX) as QtyAlerts    
							   FROM Production_QPL_QA_Alert_Process_Link l
							   JOIN Production_QA_Quality_Alerts a ON a.QA_IX = l.QPL_QA_IX                       
							   WHERE l.QPL_RCH_IX = PRH.RCH_IX), 0) as QAAlerts
                                                
                            FROM Production_PRC_Production_Route_Card PRC
                            Inner JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PRC.PRC_PCH_IX
                            Inner join Stock_BH_Batch_Headers bh on bh.BH_IX = PRH.RCH_BH_IX
                            Inner join Project_JH_Job_Headers jh on jh.JH_IX = bh.BH_JH_IX
                            Inner join GPIN_Generic_PINS gp on gp.GPIN = bh_pin
                            INNER JOIN Production_PT_Process_Type PT on PT.PT_IX = RCH_PT_IX
                                                
                            WHERE RCH_Active = @RCH_Active and BH_ERC_View = @BH_ERC_View and BH_Reserved = @ALLOC And PRC_Removed = 0 And BH_Deleted = 0  
                            And (CASE WHEN @BATCH = 0 THEN 
                                        CASE WHEN BH_Number >= 0 THEN 1 ELSE 0 END
							    ELSE 
							            CASE WHEN BH_Number = @BATCH THEN 1 ELSE 0 END
						    END) = 1
                            Group By
                                        BH_PIN, jh.JH_Job_Number, jh.JH_IX, BH_Number, BH_Client, BH_Build_Date, 
                                        BH_Qty, BH_Comment, jh.JH_Description, jh.JH_Client_Order_No, BH_First_Serial, BH_Complete, 
                                        BH_ERC_View, BH_PMR, RCH_Created, RCH_By, BH_IX, PRH.RCH_IX, GP.GPIN_IX,  PT_TYPE,
                                        gp.GPIN_Alert, gp.GPIN_Comment, RCH_Instructions, gp.GPIN_AOI_Data, RCH_IPC_Class, RCH_Panel_Layout,
                                        gp.GPIN_client_Part_No, gp.GPIN_Description, BH_Assigned, BH_Reserved
                            ) Batches
                            
                            INNER JOIN
                            
                            (Select 
                            (States.BH_Number) as Batch,
                            SUM(CASE WHEN States.PRS_Parent_Step_IX = 0 THEN States.Active ELSE 0 END) as ActiveTLP, 
                            SUM(CASE WHEN States.PRS_Parent_Step_IX > 0 THEN States.Active ELSE 0 END) as ActiveSLP,
                            MAX(CASE WHEN States.PRS_IX = 128 THEN 1 ELSE 0 END) as AlertSLP  

                            FROM 
                            (SELECT  PS.PRS_Parent_Step_IX, BN.BH_Number, PS.PRS_IX,
                            SUM(     
                                CASE WHEN  PRC.PRC_ST_IX in (0) THEN 0 ELSE        	
                                CASE WHEN
                                    ROUND((Cast(CASE WHEN PSH.SCANS Is Null THEN 0 ELSE 
                                                CASE WHEN PRC.PRC_ST_IX in (1, 2, 3) THEN 
                                                    SCANS 
                                                    ELSE PSH.PSH_Quantity
                                                END
                                                END as FLOAT) /
                                                
                                    CAST(CASE WHEN PRC.PRC_ST_IX in (4) THEN CASE WHEN TTS2.TTS_IX <> 1 THEN 2 ELSE 1 END 
                                    ELSE BH_Qty END  As FLOAT) * 100.00) , 3) < 100 THEN 1 ELSE 0
                                END 
                                END) AS [Active]  
                            
                            FROM Production_RCI_Route_Card_Instruction RCI
                                INNER JOIN Production_IT_Instruction_Type IT ON RCI.RCI_IT_IX = IT.IT_IX
                                INNER JOIN Production_PRC_Production_Route_Card PRC ON RCI.RCI_PRC_IX = PRC.PRC_IX
                                INNER JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PRC.PRC_PCH_IX  
                                INNER JOIN Production_PRS_Process_Steps PS ON PRC.PRC_PRS_IX = PS.PRS_Step_IX
                                INNER JOIN Production_TTS_Team_To_Sign TTS1 on TTS1.TTS_IX = RCI_TTS_IX 
                                INNER JOIN Production_TTS_Team_To_Sign TTS2 on TTS2.TTS_IX = RCI_TTS_IX_2
                                INNER JOIN Stock_BH_Batch_Headers BN on BN.BH_IX = PRH.RCH_BH_IX 
                                INNER JOIN Production_ST_Scan_Type st on st.ST_IX = PRC_ST_IX
                            
                        
                LEFT OUTER JOIN(
                        SELECT PSH_PRC_IX, SUM(PSH_Quantity) as PSH_Quantity, 
                        COUNT(Distinct(
                            CASE WHEN PSH_ST_IX in (1, 2, 3) 
                                THEN PSH_PSN_IX 
                                ELSE null 
                                END)
                        ) [SCANS] 
                        FROM Production_PSH_Product_Scan_History PH
                        GROUP BY PSH_PRC_IX, PSH_ST_IX) PSH ON PRC.PRC_IX = PSH.PSH_PRC_IX
                        AND PRS_Inactive = 0 AND PRC.PRC_Removed = 0  	           
                    GROUP BY BN.BH_Number, PRS_Parent_Step_IX, PRS_IX		
                    Having PS.PRS_Parent_Step_IX >= 0 

			        And (CASE WHEN @BATCH = 0 THEN 
                              CASE WHEN BN.BH_Number >= 0 THEN 1 ELSE 0 END
							ELSE 
							  CASE WHEN BN.BH_Number = @BATCH THEN 1 ELSE 0 END
						 END) = 1

                    ) States group by States.BH_Number) State on Batches.BatchNumber = State.Batch
                            
		        Order by State.ActiveSLP DESC, State.ActiveTLP DESC, Batches.BuildDate DESC";
            p.Add("@RCH_Active", rca, DbType.Boolean);
            p.Add("@BH_ERC_View", vis, DbType.Boolean);
            p.Add("@ALLOC", alloc, DbType.Boolean);
            p.Add("@PERIOD", period, DbType.Int32);
            p.Add("@BATCH", batch, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                // var result = await conn.QueryAsync<LiveBatches>("RouteCardJobs", p, commandType: CommandType.StoredProcedure);
                Console.WriteLine(sql);
                var result = await conn.QueryAsync<LiveBatches>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<ProcessModel>> IRouteCardRepository.Processes(int batch, int tlp, int un_ix, Boolean myProc)
        {
            var p = new DynamicParameters();
            string sql = @"
            SELECT DISTINCT TLP.*, COALESCE(SLP.Active, 0) as ActiveSLP, 
            COALESCE(DRGS.QTY, 0) as Drawings,
            COALESCE(CS1.PSH_TTS_IX, -1) as TS1, 
            COALESCE(CS2.PSH_TTS_IX, -1) as TS2,
            COALESCE(CSIG1.PSH_BY, '') as TT1SIG,
            COALESCE(CSIG2.PSH_BY, '') as TT2SIG,
            COALESCE(PS.PTM_PTS_IX, 0) as ProcStatus,
            CASE WHEN CS1.PSH_TTS_IX is not null THEN 1 ELSE 0 END + CASE WHEN CS2.PSH_TTS_IX is not null THEN 1 ELSE 0 END  as Signatures
            FROM (
	        SELECT               
            BH.BH_Pin as PinNumber, JH.JH_Job_Number as JobNumber, JH.JH_IX as JobIndex, BH.BH_Number as BatchNumber, BH.BH_IX as BatchIndex,
            PRC.PRC_PCH_IX as RouteCardHdrIndex, PRC.PRC_Parent_PRC_IX  as ParentPRCIndex, PRS_Parent_Step_IX as ParentIndex, PRC_IX as UniqueStepID, PRS_Step_IX as ProcessID, 
            COALESCE((SELECT s2.PRS_Step_Text 
                     FROM Production_PRS_Process_Steps s2 
                     WHERE s2.PRS_Step_IX  = prs.PRS_Parent_Step_IX), PRS.PRS_Step_Text) as ProcessName,
					 PRS.PRS_Step_Text as StepText, 
					 TTS1.TTS_Team as TTS_TEAM_1, TTS1.TTS_IX as TTSIndex_1,			
					 TTS2.TTS_Team as TTS_TEAM_2, TTS2.TTS_IX as TTSIndex_2,
					
					(SELECT COUNT(*)
						FROM Production_UR_User_Roles r
						INNER JOIN UN_User_Names u on u.UN_IX = r.UR_UN_IX
						-- WHERE r.UR_TTS_IX = TTS1.TTS_IX) as Authorised_1,					
                        WHERE r.UR_UN_IX = @USER_IX and r.UR_TTS_IX = TTS1.TTS_IX) as Authorised_1,					
						
					(SELECT COUNT(*)
						FROM Production_UR_User_Roles r
						INNER JOIN UN_User_Names u on u.UN_IX = r.UR_UN_IX
						--WHERE r.UR_TTS_IX = TTS2.TTS_IX) as Authorised_2,											
                        WHERE r.UR_UN_IX = @USER_IX and r.UR_TTS_IX = TTS2.TTS_IX) as Authorised_2,											
					 
					PRC.PRC_Ordering as Ordering, RCI_Position as Position, 
					RCI_IX as InstIndex, COALESCE(RCI_Instruction, '') as Instruction, IT_Type_Text as InstType, 
					PRC_ST_IX as ScanTypeIndex, ST_TEXT as ScanTypeText, 

					 
					COALESCE(SUM(CASE WHEN PRC_ST_IX in (3) THEN ScanHistory.PSH_Quantity END), 0) as [EnteredQuantity],  
					       
					MAX(
                        CASE WHEN PRC.PRC_ST_IX in (1, 2, 3, 6) THEN PRC_Qty 
                         ELSE
                          CASE WHEN PRC.PRC_ST_IX in (5, 7) THEN PRC_Qty 
                            ELSE 
                            CASE WHEN PRC.PRC_ST_IX in (4) and TTS2.TTS_IX <> 1 THEN 2
                            ELSE 
                                CASE WHEN PRC.PRC_ST_IX in (0) THEN 0 ELSE 1 
                            END
                          END
                         END
					    END
					) as [ExpectedScans],     
					           
				  SUM(CASE WHEN ScanHistory.SCANS Is Null THEN 0 ELSE 
						   CASE WHEN PRC.PRC_ST_IX in (1, 2, 5, 6, 7) THEN SCANS 
								ELSE 0
							END
					  END) [ScanCount],          
                   
				  ROUND(MAX(Cast(CASE WHEN ScanHistory.SCANS Is Null THEN 0 ELSE 
							CASE WHEN PRC.PRC_ST_IX in (1, 2, 5, 6, 7) THEN 
								SCANS 
								ELSE COALESCE(ScanHistory.PSH_Quantity, 0)
							END
							END as FLOAT) / 
                 NULLIF(
                            CAST(
                                CASE WHEN PRC.PRC_ST_IX in (4) THEN CASE WHEN TTS2.TTS_IX <> 1 THEN 2 ELSE 1 END 
                                        ELSE                             
                                        CASE WHEN PRC_ST_IX IN (5, 7) THEN PRC.PRC_QTY
                                             ELSE PRC_Qty
                                        END		                            
                                END  
                            As FLOAT), 
                       0) * 100.00) , 3) as ProgressPercent,     
                
                        COUNT(CASE WHEN (COALESCE(SCANS, 0) < 1) And PRC.PRC_Parent_PRC_IX  = 0 And PRC.PRC_ST_IX in (4) THEN 
                                CASE WHEN TTS2.TTS_IX <> 1 THEN 2 ELSE 1 END  
                                ELSE	
                                    CASE WHEN (COALESCE(SCANS, 0) < PRC_QTY) And PRC.PRC_Parent_PRC_IX  = 0 And PRC.PRC_ST_IX in (1,2,3,5,6,7) THEN 1 
                                    ELSE NULL
                                END
                            END)		
				as [ActiveTLP],
                     
                COUNT(DISTINCT(alert.QA_IX)) - SUM(CASE WHEN ack.QAA_UN_IX = @USER_IX THEN 1 ELSE 0 END) as QAAlerts
             	
			FROM Production_RCI_Route_Card_Instruction RCI
				INNER JOIN Production_IT_Instruction_Type IT ON RCI.RCI_IT_IX = IT.IT_IX
				INNER JOIN Production_PRC_Production_Route_Card PRC ON RCI.RCI_PRC_IX = PRC.PRC_IX
				INNER JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PRC.PRC_PCH_IX  
				INNER JOIN Production_PRS_Process_Steps PRS ON PRC.PRC_PRS_IX = PRS.PRS_Step_IX
				INNER JOIN Production_TTS_Team_To_Sign TTS1 on TTS1.TTS_IX = RCI_TTS_IX 
				INNER JOIN Production_TTS_Team_To_Sign TTS2 on TTS2.TTS_IX = RCI_TTS_IX_2
				INNER JOIN Stock_BH_Batch_Headers BH on BH.BH_IX = PRH.RCH_BH_IX 
				INNER JOIN Project_JH_Job_Headers jh on jh.JH_IX = bh.BH_JH_IX
				INNER JOIN Production_ST_Scan_Type st on st.ST_IX = PRC_ST_IX

                LEFT OUTER JOIN Production_QPL_QA_Alert_Process_Link link ON link.QPL_RCH_IX = PRH.RCH_IX AND Link.QPL_PRC_IX = PRC.PRC_IX 
				LEFT OUTER JOIN Production_QA_Quality_Alerts alert ON alert.QA_IX = link.QPL_QA_IX
                LEFT OUTER JOIN Production_QAA_Quality_Alerts_Acknowledged ack on ack.QAA_PRC_IX = prc.PRC_IX and ack.QAA_RCH_IX = prh.RCH_IX and ack.QAA_QA_IX = alert.QA_IX 
                and QAA_QAT_IX = 1 and QAA_UN_IX = @USER_IX

			LEFT OUTER JOIN(
				SELECT PSH_PRC_IX, PSH_RCI_IX, SUM(PSH_Quantity) as PSH_Quantity, 
	            
				-- COUNT(Distinct(PSH_PSN_IX)) as [SCANS] 
                SUM(PSH_Quantity) as [SCANS] 
					FROM Production_PSH_Product_Scan_History PH
					INNER JOIN Production_PRS_Process_Steps PRS ON PH.PSH_PRS_IX = PRS.PRS_Step_IX
					GROUP BY PSH_PRC_IX, PSH_ST_IX, PSH_RCI_IX) ScanHistory ON PRC.PRC_IX = ScanHistory.PSH_PRC_IX  
					-- See if this is a confirmatory signature, in which case check we have INST INDEX Recorded
					 AND (CASE WHEN st.ST_IX = 4 THEN 
							CASE WHEN PSH_RCI_IX = RCI.RCI_IX THEN 1 ELSE 0 END
             			   ELSE 1
						  END)= 1
                
                WHERE BH.BH_Number = @BATCH -- and ST_IX = 1

                    And PRS_Inactive = 0 AND PRC.PRC_Removed = 0  	           
                    And (CASE WHEN @TLP = 1 THEN (CASE WHEN PRS.PRS_Parent_Step_IX = 0 THEN 1 ELSE 0 END) ELSE 
                         CASE WHEN @TLP = 0 THEN (CASE WHEN PRS.PRS_Parent_Step_IX >= 0 And PRC_ST_IX > 0 THEN 1 ELSE 0 END) ELSE 1 END
                        END
                        ) = 1	
            GROUP BY PRC_IX, PRC_PRS_IX, BH_PIN, PRS_Step_Text, PRS_Step_IX, PRC_Parent_PRC_IX, PRS_Parent_Step_IX,
                     PRS_Step_Text, PRC_Ordering, RCI_Position, PRC_PCH_IX, RCI_Instruction, IT_Type_Text, RCI_TTS_IX, RCI_TTS_IX_2,
                     RCI_Active, RCI_IX, TTS1.TTS_Team, TTS2.TTS_Team, PRC_ST_IX, ST_TEXT, BH_Number, BH_IX, JH_Job_Number, JH_IX,
                     TTS1.TTS_IX, TTS2.TTS_IX            
            ) TLP
            
           LEFT OUTER JOIN (
             SELECT sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX                         
			 FROM Production_PSH_Product_Scan_History sh     

           ) CS1 on CS1.PSH_TTS_IX = TTSIndex_1 and CS1.PSH_ST_IX = 4 and CS1.PSH_PRC_IX = UniqueStepID and CS1.psh_rci_ix = InstIndex
           
           LEFT OUTER JOIN (
             SELECT sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX                         
			 FROM Production_PSH_Product_Scan_History sh     

           ) CS2 on CS2.PSH_TTS_IX = TTSIndex_2 and CS2.PSH_ST_IX = 4 and CS2.PSH_PRC_IX = UniqueStepID and CS2.psh_rci_ix = InstIndex


            LEFT OUTER JOIN (
             SELECT DISTINCT sh.PSH_By, sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX, sh.PSH_PSN_IX                   
			 FROM Production_PSH_Product_Scan_History sh     
           ) CSIG1 on CSIG1.PSH_TTS_IX = TTSIndex_1 and CSIG1.PSH_ST_IX = 4 and CSIG1.PSH_PRC_IX = UniqueStepID and CSIG1.psh_rci_ix = InstIndex 
             and CSIG1.PSH_PSN_IX = BatchNumber
           
            LEFT OUTER JOIN (
             SELECT DISTINCT sh.PSH_By, sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX, sh.PSH_PSN_IX                         
			 FROM Production_PSH_Product_Scan_History sh     
           ) CSIG2 on CSIG2.PSH_TTS_IX = TTSIndex_2 and CSIG2.PSH_ST_IX = 4 and CSIG2.PSH_PRC_IX = UniqueStepID and CSIG2.psh_rci_ix = InstIndex 
             and CSIG2.PSH_PSN_IX = BatchNumber

            LEFT OUTER JOIN (
                    SELECT Distinct COUNT(*) as QTY,  PRC.PRC_IX, PRC.PRC_Parent_PRC_IX, DA_RCH_IX 
                    FROM Production_DA_Documents_Attached DA
                    INNER JOIN Production_DPA_Document_Process_Assignment DP ON DP.DPA_RCH_IX = DA.DA_RCH_IX
                    INNER JOIN Production_PRC_Production_Route_Card PRC ON DP.DPA_PRC_IX =
                    CASE WHEN PRC.PRC_Parent_PRC_IX = 0 THEN PRC.PRC_IX ELSE PRC.PRC_Parent_PRC_IX END AND DA.DA_IX = DP.DPA_DA_IX
                    WHERE PRC.PRC_Removed = 0
                    GROUP BY PRC.PRC_IX, PRC.PRC_Parent_PRC_IX, DA_RCH_IX 
            ) DRGS on DRGS.DA_RCH_IX = RouteCardHdrIndex and (DRGS.PRC_IX = UniqueStepID or DRGS.PRC_Parent_PRC_IX = UniqueStepID)
           
           LEFT OUTER JOIN (
				SELECT * FROM
				(
				   SELECT *,
						 ROW_NUMBER() OVER (PARTITION BY ptm_rci_ix ORDER BY ptm_event_time DESC) AS [LAST]
				   FROM Production_PTM_Process_Time_Monitor
				   
				) as a
				WHERE [LAST] = 1
			) PS on PS.PTM_RCH_IX = RouteCardHdrIndex and PS.PTM_PRC_IX = UniqueStepID and PS.PTM_UN_IX = @USER_IX and PS.PTM_RCI_IX = InstIndex 

            LEFT OUTER JOIN
            (
            SELECT PRS_Parent_Step_IX, 
 		      SUM(     
 		        CASE WHEN  PRC.PRC_ST_IX = 0 THEN 0 ELSE        	
                  CASE WHEN
                     ROUND((Cast(CASE WHEN PSH.PSH_Quantity Is Null THEN 0 ELSE 
						PSH.PSH_Quantity
                     
                                END as FLOAT) /
 	                 CAST(CASE WHEN PRC.PRC_ST_IX = 4 THEN CASE WHEN TTS2.TTS_IX <> 1 THEN 2 ELSE 1 END 
 		                   ELSE CASE WHEN @TLP = 1 OR PRC_QTY <= 0 THEN BH_Qty ELSE PRC_Qty END 
                        END  As FLOAT) * 100.00) , 3) < 100 THEN 1 ELSE 0
 		              END 
 		         END)
 		        as [Active]
                         	
                FROM Production_RCI_Route_Card_Instruction RCI
                    INNER JOIN Production_IT_Instruction_Type IT ON RCI.RCI_IT_IX = IT.IT_IX
                    INNER JOIN Production_PRC_Production_Route_Card PRC ON RCI.RCI_PRC_IX = PRC.PRC_IX
                    INNER JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PRC.PRC_PCH_IX  
                    INNER JOIN Production_PRS_Process_Steps PRS ON PRC.PRC_PRS_IX = PRS.PRS_Step_IX
                    INNER JOIN Production_TTS_Team_To_Sign TTS1 on TTS1.TTS_IX = RCI_TTS_IX 
                    INNER JOIN Production_TTS_Team_To_Sign TTS2 on TTS2.TTS_IX = RCI_TTS_IX_2
                    INNER JOIN Stock_BH_Batch_Headers BH on BH.BH_IX = PRH.RCH_BH_IX 
                    INNER JOIN Production_ST_Scan_Type st on st.ST_IX = PRC_ST_IX
     
       LEFT OUTER JOIN(
            SELECT PSH_PRC_IX, SUM(PSH_Quantity) as PSH_Quantity

                FROM Production_PSH_Product_Scan_History PH
                INNER JOIN Production_PRS_Process_Steps PRS ON PH.PSH_PRS_IX = PRS.PRS_Step_IX
                GROUP BY PSH_PRC_IX, PSH_ST_IX) PSH ON PRC.PRC_IX = PSH.PSH_PRC_IX 
                WHERE BH.BH_Number = @BATCH -- and prs.PRS_Parent_Step_IX = 23
                    And PRS_Inactive = 0 AND PRC.PRC_Removed = 0  	           
                    And (CASE WHEN @TLP = 1 THEN (CASE WHEN PRS.PRS_Parent_Step_IX = 0 THEN 1 ELSE 0 END) ELSE 
                           CASE WHEN @TLP = 0 THEN (CASE WHEN PRS.PRS_Parent_Step_IX > 0 THEN 1 ELSE 0 END) ELSE 1			 
                           END
                         END
                        ) = 0	
            GROUP BY PRS_Parent_Step_IX) SLP
            ON SLP.PRS_Parent_Step_IX = TLP.ProcessID     
            WHERE 
             (CASE WHEN @MYPROCESSES = 1 THEN 
                CASE WHEN (Authorised_1 = 1 or Authorised_2 = 1) THEN 1 ELSE 0 END
               ELSE 1
             END) = 1 
            ORDER BY TLP.Ordering, TLP.InstIndex, TLP.Position ASC";
            p.Add("@BATCH", batch, DbType.Int32);
            p.Add("@TLP", tlp, DbType.Int32);
            p.Add("@USER_IX", un_ix, DbType.Int32);
            p.Add("@MYPROCESSES", myProc, DbType.Boolean);
            Console.WriteLine(sql);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<ProcessModel>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<ProcessModel>> IRouteCardRepository.FilteredProcesses(int batch, int un_ix, Boolean myProc)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT ProcessName, BatchNumber, PinNumber, BuildDate, RouteCardHdrIndex, ParentIndex, ParentPRCIndex, CAST(BuildDate as DATE) BuildDate, 
            ProcStatus, Count(UniqueStepID) as ActiveSLP
            FROM (

            SELECT RouteCardHdrIndex, ParentIndex, ParentPRCIndex, UniqueStepID, ProcessName, BuildDate, BatchNumber, PinNumber, ProcStatus, ProgressPercent,
                ROW_NUMBER() OVER (ORDER BY Override, ProcStatus DESC, BuildDate, BatchNumber, Ordering, Position) as RowOrder
            FROM (
                SELECT TOP 1000
                A.BH_PIN as PinNumber, 
                A.JobNumber, 
                A.JobIndex, 
                A.BatchNumber, 
                A.BatchIndex, 
                A.RouteCardHdrIndex, 
                A.ParentPRCIndex,
                A.ParentIndex,
                A.UniqueStepID, 
                A.ProcessID, 
                A.ProcessName, 
                A.StepText, 
                A.TTS_TEAM_1, 
                A.TS1 as TTSIndex_1,			
                A.TTS_TEAM_2, 
                A.TS2 as TTSIndex_2,
                COALESCE(B.Auth1, 0) as Authorised_1, 
                COALESCE(C.Auth2, 0) as Authorised_2,
                A.BuildDate, A.DeliveryDate, 
                A.Ordering, 
                A.Position, 
                A.[Override],
                A.InstIndex, 
                A.Instruction, 
                A.InstType,
                A.ScanTypeIndex,
                COALESCE(CSIG1.PSH_BY, '') as TT1SIG,
                COALESCE(CSIG2.PSH_BY, '') as TT2SIG,
                COALESCE(CSIG1.PSH_TTS_IX, -1) as TS1, 
                COALESCE(CSIG2.PSH_TTS_IX, -1) as TS2,
                A.ExpectedScans, 
                A.ScanCount, 
                A.EnteredQuantity,
                CASE WHEN CSIG1.PSH_TTS_IX is not null  THEN 1 ELSE 0 END + CASE WHEN CSIG2.PSH_TTS_IX is not null THEN 1 ELSE 0 END  as Signatures,
                
                CASE WHEN ScanTypeIndex = 4 THEN 
                CAST((CASE WHEN CSIG1.PSH_TTS_IX is not null THEN 1 ELSE 0 END + CASE WHEN CSIG2.PSH_TTS_IX is not null THEN 1 ELSE 0 END)  as Float)  / A.ExpectedScans  * 100.0
                ELSE  A.ProgressPercent END as ProgressPercent,
                
                COALESCE(DRGS.QTY, 0) as Drawings,
                COALESCE(PS.PTM_PTS_IX, 0) as ProcStatus
                    
            FROM (
                Select
                    PRS_Step_Text as StepText, 
                    COALESCE((SELECT s2.PRS_Step_Text 
                            FROM Production_PRS_Process_Steps s2 
                            WHERE s2.PRS_Step_IX  = prs.PRS_Parent_Step_IX), PRS.PRS_Step_Text) as ProcessName,
                PRC_IX as UniqueStepID, RCI_IX as InstIndex, PRS_Step_IX as ProcessID, PRC_Ordering as Ordering, RCI_Position as Position, PRC_Sort_Override as [Override],
                    TTS1.TTS_IX as TS1, TTS2.TTS_IX as TS2, RCI_Instruction as Instruction, IT_Type_Text as InstType,
                    TTS1.TTS_Team as TTS_TEAM_1, TTS2.TTS_Team as TTS_TEAM_2, 
                    BH.BH_Number as BatchNumber, BH.BH_IX as BatchIndex, BH.BH_Build_Date as BuildDate, BH.BH_Pin, 
                    BH.BH_Delivery_Date as DeliveryDate, JH.JH_Job_Number as JobNumber, JH.JH_IX as JobIndex,
                    RCH_IX as RouteCardHdrIndex, 
                    PRC_ST_IX as ScanTypeIndex, PRC_Parent_PRC_IX as ParentPRCIndex, PRS_Parent_Step_IX as ParentIndex,
                    SUM(CASE WHEN SH.PSH_IX Is Null THEN 0 ELSE 
                                    CASE WHEN PRC.PRC_ST_IX in (1, 2, 5, 6, 7) THEN 1 
                                            ELSE 0
                                        END
                                END) as [ScanCount],
                    COALESCE(SUM(CASE WHEN PRC_ST_IX in (3) THEN SH.PSH_Quantity END), 0) as [EnteredQuantity],
                    
                    MAX(CASE WHEN PRC.PRC_ST_IX in (1, 2, 3, 6) THEN PRC_Qty 
                         ELSE CASE WHEN PRC.PRC_ST_IX in (5, 7) THEN PRC_Qty 
                              ELSE 
                                    CASE WHEN PRC.PRC_ST_IX in (4) THEN 
                                        CASE WHEN (TTS1.TTS_IX > 1 and TTS2.TTS_IX > 1) THEN 2 ELSE
                                        CASE WHEN (TTS1.TTS_IX > 1) THEN 1 ELSE 0 END
                                        END 
                        
                                    ELSE
                                    CASE WHEN PRC.PRC_ST_IX in (0) THEN 0  END
                                END 
                            END
                          END
                        ) as [ExpectedScans],
                                
                    ROUND(SUM(Cast(CASE WHEN SH.PSH_IX Is Null THEN 0 ELSE 
                                        CASE WHEN PRC_ST_IX in (1, 2, 5, 6, 7) THEN 1 
                                            ELSE COALESCE(SH.PSH_Quantity, 0)
                                        END
                                        END as FLOAT) /						
                                        CASE WHEN PRC_ST_IX = 5 THEN CAST(PRC.PRC_QTY As FLOAT) 
                                                ELSE CAST(PRC_Qty As FLOAT) 				
                                        END	
                            ) * 100.00 , 3) as ProgressPercent
                    
                    FROM Production_RCI_Route_Card_Instruction RCI
                            INNER JOIN Production_IT_Instruction_Type IT ON RCI.RCI_IT_IX = IT.IT_IX
                            INNER JOIN Production_PRC_Production_Route_Card PRC ON RCI.RCI_PRC_IX = PRC.PRC_IX
                            INNER JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PRC.PRC_PCH_IX  
                            INNER JOIN Production_PRS_Process_Steps PRS ON PRC.PRC_PRS_IX = PRS.PRS_Step_IX
                            INNER JOIN Production_TTS_Team_To_Sign TTS1 on TTS1.TTS_IX = RCI_TTS_IX 
                            INNER JOIN Production_TTS_Team_To_Sign TTS2 on TTS2.TTS_IX = RCI_TTS_IX_2
                            INNER JOIN Stock_BH_Batch_Headers BH on BH.BH_IX = PRH.RCH_BH_IX 
                            INNER JOIN Project_JH_Job_Headers JH on JH.JH_IX = BH.BH_JH_IX
                            INNER JOIN Production_ST_Scan_Type st on st.ST_IX = PRC_ST_IX
                            LEFT OUTER JOIN Production_PSH_Product_Scan_History SH on SH.PSH_PRC_IX = PRC.PRC_IX --and SH.psh_rci_ix = PRC_IX
                        
                            
                        WHERE (CASE WHEN @BATCH > 0 THEN CASE WHEN BH.BH_Number = @BATCH THEN 1 ELSE 0 END ELSE 1 END) = 1
                        and RCH_Active = 1 and BH_Reserved = 1
                        And PRC_Removed = 0 and BH_Deleted = 0 And PRS_Inactive = 0 
                        
                        And PRS_Parent_Step_IX >= 0 -- ????

                        GROUP BY PRS_Step_Text, PRC_IX, RCI_IX, PRS_Step_IX, PRC_Ordering, PRC_Sort_Override, RCI_Position, 
                        PRC_ST_IX, TTS1.TTS_IX, TTS2.TTS_IX, TTS1.TTS_Team, TTS2.TTS_Team, JH.JH_Job_Number, JH.JH_IX, RCI_Instruction, IT_Type_Text,
                        BH_Number, BH_IX, BH_PIN, BH_Build_Date, BH.BH_Delivery_Date, RCH_IX, PRC_Parent_PRC_IX, PRS_Parent_Step_IX
                        ) as A
                
                        
                        LEFT OUTER JOIN (
                                    SELECT COUNT(*) as Auth1, r.UR_TTS_IX as T1
                                    FROM Production_UR_User_Roles r
                                    INNER JOIN UN_User_Names u on u.UN_IX = r.UR_UN_IX
                                    WHERE r.UR_UN_IX = @USER_IX
                                    GROUP BY UR_TTS_IX
                                    ) B on B.T1 = A.TS1
                                    
                        LEFT OUTER JOIN (
                                    SELECT COUNT(*) as Auth2, r.UR_TTS_IX as T2
                                    FROM Production_UR_User_Roles r
                                    INNER JOIN UN_User_Names u on u.UN_IX = r.UR_UN_IX
                                    WHERE r.UR_UN_IX = @USER_IX
                                    GROUP BY UR_TTS_IX
                                    ) C on C.T2 = A.TS2
                                    
                    LEFT OUTER JOIN (
                        SELECT DISTINCT sh.PSH_By, sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX, sh.PSH_PSN_IX                   
                        FROM Production_PSH_Product_Scan_History sh   
                    ) CSIG1 on CSIG1.PSH_TTS_IX = TS1 and CSIG1.PSH_ST_IX = 4 and CSIG1.PSH_PRC_IX = UniqueStepID and CSIG1.psh_rci_ix = InstIndex 
                        and CSIG1.PSH_PSN_IX = BatchNumber
                        
                    
                        LEFT OUTER JOIN (
                        SELECT DISTINCT sh.PSH_By, sh.PSH_TTS_IX, sh.PSH_ST_IX, sh.PSH_PRS_IX, sh.psh_rci_ix, sh.PSH_PRC_IX, sh.PSH_PSN_IX                           
                        FROM Production_PSH_Product_Scan_History sh     
                    ) CSIG2 on CSIG2.PSH_TTS_IX = TS2 and CSIG2.PSH_ST_IX = 4 and CSIG2.PSH_PRC_IX = UniqueStepID and CSIG2.psh_rci_ix = InstIndex 
                        and CSIG2.PSH_PSN_IX = BatchNumber
                        
                        LEFT OUTER JOIN (
                            SELECT * FROM
                            (
                            SELECT *,
                                    ROW_NUMBER() OVER (PARTITION BY PSH_By ORDER BY PSH_IX DESC) AS [ROW]
                            FROM Production_PSH_Product_Scan_History sh 
                            
                            ) as D
                        )	PS1 on PS1.PSH_ST_IX = 4 and PS1.PSH_TTS_IX = B.T1  and PS1.PSH_PRC_IX = UniqueStepID and PS1.psh_rci_ix = InstIndex and PS1.PSH_PSN_IX = BatchNumber 
                        
                        LEFT OUTER JOIN (
                            SELECT * FROM
                            (
                            SELECT *,
                                    ROW_NUMBER() OVER (PARTITION BY PSH_By ORDER BY PSH_IX DESC) AS [ROW]
                            FROM Production_PSH_Product_Scan_History sh 
                            
                            ) as D
                        )	PS2 on PS2.PSH_ST_IX = 4 and PS2.PSH_TTS_IX = C.T2  and PS2.PSH_PRC_IX = UniqueStepID and PS2.psh_rci_ix = InstIndex and PS2.PSH_PSN_IX = BatchNumber 
                        
                        
                    LEFT OUTER JOIN (
                            SELECT Distinct COUNT(*) as QTY,  PRC.PRC_IX, PRC.PRC_Parent_PRC_IX, DA_RCH_IX 
                            FROM Production_DA_Documents_Attached DA
                            INNER JOIN Production_DPA_Document_Process_Assignment DP ON DP.DPA_RCH_IX = DA.DA_RCH_IX
                            INNER JOIN Production_PRC_Production_Route_Card PRC ON DP.DPA_PRC_IX =
                            CASE WHEN PRC.PRC_Parent_PRC_IX = 0 THEN PRC.PRC_IX ELSE PRC.PRC_Parent_PRC_IX END AND DA.DA_IX = DP.DPA_DA_IX
                            WHERE PRC.PRC_Removed = 0
                            GROUP BY PRC.PRC_IX, PRC.PRC_Parent_PRC_IX, DA_RCH_IX 
                    ) DRGS on DRGS.DA_RCH_IX = RouteCardHdrIndex and (DRGS.PRC_IX = UniqueStepID or DRGS.PRC_Parent_PRC_IX = UniqueStepID)
                        
                    LEFT OUTER JOIN (
                            SELECT * FROM
                            (
                            SELECT *,
                                    ROW_NUMBER() OVER (PARTITION BY ptm_rci_ix ORDER BY ptm_event_time DESC) AS [LAST]
                            FROM Production_PTM_Process_Time_Monitor
                            
                            ) as a
                            WHERE [LAST] = 1
                        )	PS 	on PS.PTM_RCH_IX = RouteCardHdrIndex and PS.PTM_PRC_IX = UniqueStepID and PS.PTM_UN_IX = @USER_IX and PS.PTM_RCI_IX = InstIndex 
                        
                                    
                        WHERE 
                                (CASE WHEN @MYPROCESSES = 1 THEN 
                                    CASE WHEN (B.Auth1 = 1 or C.Auth2 = 1) THEN 1 ELSE 0 END
                                ELSE 1
                                END) = 1 
                
                    ) as [FULL]
                    
                    -- WHERE ProgressPercent < 100.00 and ProcStatus between 0 and 2

                    WHERE ProcStatus between 0 and 2
                
                    ) FILTERED
                    
                    GROUP BY ProcessName, BatchNumber, PinNumber, RouteCardHdrIndex, ParentIndex, ParentPRCIndex, BuildDate, ProcStatus
                    
                    HAVING ParentPRCIndex > 0
                    
                    ORDER BY MIN(RowOrder)";

            Console.WriteLine(sql);
            p.Add("@BATCH", batch, DbType.Int32);
            p.Add("@USER_IX", un_ix, DbType.Int32);
            p.Add("@MYPROCESSES", myProc, DbType.Boolean);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<ProcessModel>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<GNC>> IRouteCardRepository.GNCs(string pin, int period, int userID)
        {
            var p = new DynamicParameters();
            string validPin = this.RestoreRoHSPin(pin);
            string sql = @" SELECT 
                            l.QMS_GNC_LOG_IDX as GNCIndex,
                            l.GNC_LOG_Pin as PinNumber,
                            l.GNC_LOG_Number as GNCNumber, 
                            l.GNC_LOG_DateRaised as Raised, 
                            l.GNC_LOG_Description as [Description],
                            r.GNC_REPORT_PrevAction as PreventiveAction,
                            l.GNC_LOG_Status as Status,
                            CASE WHEN c.pac_ix is not null THEN 1 ELSE 0 END as Accepted
                            FROM dbo.qms_gnc_log l
                            inner join dbo.qms_gnc_report r on r.QMS_GNC_LOG_QMS_GNC_LOG_IDX = l.QMS_GNC_LOG_IDX
                            left join dbo.production_ua_confirmation c on c.pac_ref_number = l.GNC_LOG_Number and c.pac_un_ix = @USER_IX
                            WHERE                             
                                    CASE  WHEN LEN(@PIN) >= 3 AND (@PIN is not null) THEN 
                                    CASE 
                                        WHEN (l.GNC_LOG_Pin like SUBSTRING(@PIN, 0, LEN(@PIN) -4)  + '%') THEN 1 
                                        ELSE 0
                                    END
                                END = 1      
                                AND (l.GNC_LOG_Status = 'Open' OR l.GNC_LOG_DateRaised >= dateadd(month, -(@PERIOD), CAST(getdate() AS DATE)))        
                            Order by Status desc, GNCNumber desc, Raised desc";
            p.Add("@PIN", validPin, DbType.String);
            p.Add("@PERIOD", period, DbType.Int32);
            p.Add("@USER_IX", userID, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<GNC>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<ProcessComment>> IRouteCardRepository.GetProcessComments(int hdrIX, int stepIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT PC.PUC_IX, PC.PUC_PCH_IX, PC.PUC_PRC_IX, PC.PUC_UN_IX,
                           PC.PUC_Edited, PC.PUC_Comments, UN.UN_Initials, UN.UN_Full_Name,
                           COALESCE((Select s.PRS_Step_Text from Production_PRS_Process_Steps s where s.PRS_STEP_IX = PRS.PRS_Parent_Step_IX ), PRS.PRS_Step_Text)  as TLP,
                           PRS.PRS_Step_Text as SLP  
	                       FROM Production_PUC_Route_Card_Data_Comments PC
                           INNER JOIN [dbo].[UN_User_Names] UN on UN.UN_IX = PC.PUC_UN_IX
                           INNER JOIN Production_RCH_Route_Card_Header PRH on PRH.RCH_IX = PC.PUC_PCH_IX  
						   INNER JOIN Production_PRC_Production_Route_Card PRC on PRC.PRC_PCH_IX = PC.PUC_PCH_IX and PRC.PRC_IX = PC.PUC_PRC_IX
						   INNER JOIN [Production_PRS_Process_Steps] PRS on PRS.PRS_Step_IX = PRC.PRC_PRS_IX
	                       WHERE PC.PUC_PCH_IX = @PRC_PCH_IX and PRC.PRC_Removed = 0 and PRS.PRS_Inactive = 0
                           AND
	                       CASE 
							WHEN @PDC_PRC_IX = -1 THEN 
							   CASE 
								  WHEN (PC.PUC_PRC_IX>= 0) THEN 1
								  ELSE 0
							   END
							ELSE 
							   CASE 
								  WHEN (PC.PUC_PRC_IX = @PDC_PRC_IX ) THEN 1
								  ELSE 0
							   END
						   END <> 0
                           Order by PRC.PRC_Ordering, PUC_IX";
            p.Add("@PRC_PCH_IX", hdrIX, DbType.Int32);
            p.Add("@PDC_PRC_IX", stepIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<ProcessComment>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }


        // Gets User Sign Offs
        // This is a list of requested user sign offs, typically for PTH, MECH or Conformal Coat
        async Task<List<SignOff>> IRouteCardRepository.GetSignOffs(int authorisedUser, int batchNumber, bool showCompleted)
        {
            var p = new DynamicParameters();
            string sql = string.Empty;
            if (showCompleted) {
            sql = @"Select bh.BH_Number, bh.BH_PIN, Coalesce(bh.BH_First_Serial, '') as BH_First_Serial,
                    s.PSO_IX, s.PSO_PRC_IX, s.PSO_PCH_IX, s.PSO_RCI_IX, s.PSO_PRS_Step_IX, s.PSO_User, u.UN_Full_Name, 
                    s.PSO_Request_Date, s.PSO_RN_Tracking_ID, s.PSO_PSN_IX, 
                    Coalesce(s.PSO_PSN_Serial, '') as PSO_PSN_Serial, s.PSO_Quantity, s.PSO_Completed, 
                    Coalesce(s.PSO_Comments, '') as PSO_Comments, s.PSO_Approved_By, u2.UN_Full_Name as Auth_Name, u2.UN_Initials,
                    r.RN_UN_IX, r.RN_Status, s.PSO_Status, i.RCI_Step, i.RCI_Instruction   
                    From Production_RN_Requests r
                    Inner Join Production_PSO_Sign_Offs s on r.RN_Tracking_ID = s.pso_RN_Tracking_ID and s.PSO_RCI_IX = r.RN_RCI_IX  
                    Inner Join Production_RCI_Route_Card_Instruction i on i.RCI_IX = r.RN_RCI_IX and r.RN_RCI_TTS_IX = i.RCI_TTS_IX 
                    Inner Join UN_User_Names u on u.UN_IX = s.PSO_User 
                    Inner Join UN_User_Names u2 on u2.UN_IX = r.RN_UN_IX  
                    Inner join Production_RCH_Route_Card_Header h on h.RCH_IX = i.RCI_RCH_IX
                    Inner join Stock_BH_Batch_Headers bh on bh.BH_IX = h.RCH_BH_IX
                    Where s.PSO_Status  = 'Approved' and bh.BH_Number = @BATCH
                    UNION
                    Select bh.BH_Number, bh.BH_PIN, Coalesce(bh.BH_First_Serial, '') as BH_First_Serial
                    s.PSO_IX, s.PSO_PRC_IX, s.PSO_PCH_IX, s.PSO_RCI_IX, s.PSO_PRS_Step_IX, s.PSO_User, u.UN_Full_Name, 
                    s.PSO_Request_Date, s.PSO_RN_Tracking_ID, s.PSO_PSN_IX, 
                    Coalesce(s.PSO_PSN_Serial, '') as PSO_PSN_Serial, s.PSO_Quantity, s.PSO_Completed, 
                    Coalesce(s.PSO_Comments, '') as PSO_Comments, s.PSO_Approved_By, u2.UN_Full_Name as Auth_Name, u2.UN_Initials,
                    r.RN_UN_IX, r.RN_Status, s.PSO_Status, i.RCI_Step, i.RCI_Instruction   
                    From Production_RN_Requests r
                    Inner Join Production_PSO_Sign_Offs s on r.RN_Tracking_ID = s.pso_RN_Tracking_ID and s.PSO_RCI_IX = r.RN_RCI_IX  
                    Inner Join Production_RCI_Route_Card_Instruction i on i.RCI_IX = r.RN_RCI_IX and r.RN_RCI_TTS_IX = i.RCI_TTS_IX 
                    Inner Join UN_User_Names u on u.UN_IX = s.PSO_User 
                    Inner Join UN_User_Names u2 on u2.UN_IX = r.RN_UN_IX  
                    Inner join Production_RCH_Route_Card_Header h on h.RCH_IX = i.RCI_RCH_IX
                    Inner join Stock_BH_Batch_Headers bh on bh.BH_IX = h.RCH_BH_IX 
                    Where r.RN_UN_IX = @USER and r.RN_Status = 'Open'
                    Order by RN_Status DESC, PSO_Request_Date DESC";
            } else
            {
                sql = @"Select bh.BH_Number, bh.BH_PIN, Coalesce(bh.BH_First_Serial, '') as BH_First_Serial,
                           s.PSO_IX, s.PSO_PRC_IX, s.PSO_PCH_IX, s.PSO_RCI_IX, s.PSO_PRS_Step_IX, s.PSO_User, u.UN_Full_Name, 
                           s.PSO_Request_Date, s.PSO_RN_Tracking_ID, s.PSO_PSN_IX, 
                           Coalesce(s.PSO_PSN_Serial, '') as PSO_PSN_Serial, s.PSO_Quantity, s.PSO_Completed, 
                           Coalesce(s.PSO_Comments, '') as PSO_Comments, s.PSO_Approved_By, u2.UN_Full_Name as Auth_Name, u2.UN_Initials,
                           r.RN_UN_IX, r.RN_Status, s.PSO_Status, i.RCI_Step, i.RCI_Instruction
                           From Production_RN_Requests r
                           Inner Join Production_PSO_Sign_Offs s on r.RN_Tracking_ID = s.pso_RN_Tracking_ID and s.PSO_RCI_IX = r.RN_RCI_IX
                           Inner Join Production_RCI_Route_Card_Instruction i on i.RCI_IX = r.RN_RCI_IX and r.RN_RCI_TTS_IX = i.RCI_TTS_IX
                           Inner Join UN_User_Names u on u.UN_IX = s.PSO_User
                           Inner Join UN_User_Names u2 on u2.UN_IX = r.RN_UN_IX
                           Inner join Production_RCH_Route_Card_Header h on h.RCH_IX = i.RCI_RCH_IX
                           Inner join Stock_BH_Batch_Headers bh on bh.BH_IX = h.RCH_BH_IX
                           Where r.RN_UN_IX = @USER and r.RN_Status = 'Open'
                           Order by RN_Status DESC, PSO_Request_Date DESC";
            }
            p.Add("@USER", authorisedUser, DbType.Int32);
            p.Add("@BATCH", batchNumber, DbType.Int32);
             
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<SignOff>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }


        async Task<List<DrawingModel>> IRouteCardRepository.Drawings(int hdrIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT DA_IX, DA_RCH_IX, DA_Doc_Type, COALESCE(DA_File_Path, '') as DA_File_Path, DA_Origin, 
                        COALESCE(DA_Drawing_No, '') as DA_Drawing_No ,DA_Date, DA_Qty_Issued
                        FROM Production_DA_Documents_Attached WHERE DA_RCH_IX = @RCH_IX";
            p.Add("@RCH_IX", hdrIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<DrawingModel>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<DrawingModel>> IRouteCardRepository.DrawingsByProcess(int hdrIX, int prcIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT Distinct AT.*
                            FROM Production_DPA_Document_Process_Assignment DA
                            INNER JOIN Production_PRC_Production_Route_Card PRC ON DA.DPA_PRC_IX =
                            CASE WHEN PRC.PRC_Parent_PRC_IX = 0 
                                THEN PRC.PRC_IX 
                                ELSE PRC.PRC_Parent_PRC_IX 
                            END
                            INNER JOIN Production_DA_Documents_Attached AT on AT.DA_IX = DA.DPA_DA_IX 
                            WHERE DPA_RCH_IX = @RCH_IX AND (PRC.PRC_IX = @PRC_IX or PRC.PRC_Parent_PRC_IX = @PRC_IX)
                            AND PRC_Removed = 0";
            p.Add("@RCH_IX", hdrIX, DbType.Int32);
            p.Add("@PRC_IX", prcIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<DrawingModel>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<PanelLayout>> IRouteCardRepository.GetPanelLayout(int rch)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT  
                            h.PCB_IX, h.PCB_RCH_IX, 
                            Case When h.PCB_PM_IX = 2 Then 'Top'
                                When h.PCB_PM_IX = 3 Then 'Bottom'
                                When h.PCB_PM_IX = 4 Then 'Panel'
                                else 'Unknown'
                            End as [PCB_Category],
                            t.PIT_Item,
                            Case When h.PCB_PM_IX = 2 Then 'Top ' +  t.PIT_Item
                                When h.PCB_PM_IX = 3 Then 'Bottom ' +  t.PIT_Item
                                When h.PCB_PM_IX = 4 Then 'Panel' 
                                else 'Unknown'
                            End as [PCB_Group],
                            h.PCB_Item_Description, h.PCB_Item_Number,
                            h.PCB_X_Value, h.PCB_Y_Value, h.PCB_Thickness, h.PCB_Diameter, h.PCB_Style         

                            FROM Production_PCB_Specification_Details h
                            left outer join Production_PNR_PCB_Details d on h.PCB_PLH_IX = d.PCB_IX
                            inner join Production_PIT_PCB_Item_Type t on t.PIT_IX = h.PCB_PIT_IX 
                            where h.PCB_RCH_IX = @RCH_IX";
            p.Add("@RCH_IX", rch, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<PanelLayout>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<TimeState>> IRouteCardRepository.GetTimeState(Boolean showAll)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT *
                           FROM Production_PTS_Process_State
                           WHERE 
                            CASE 
                                WHEN @PTS_Display = 1 THEN 
                                CASE 
                                    WHEN (PTS_Display >= 0) THEN 1
                                    ELSE 0
                                END
                                ELSE 
                                CASE 
                                    WHEN (PTS_Display = 1) THEN 1
                                    ELSE 0
                                END
                            END <> 0";
            p.Add("@PTS_Display", showAll, DbType.Boolean);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<TimeState>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<TimeReason>> IRouteCardRepository.GetTimeReasons(Boolean showAll)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT *
                           FROM Production_PTR_Reason
                           WHERE 
                            CASE 
                                WHEN @PTR_Display = 1 THEN 
                                CASE 
                                    WHEN (PTR_Display >= 0) THEN 1
                                    ELSE 0
                                END
                                ELSE 
                                CASE 
                                    WHEN (PTR_Display = 1) THEN 1
                                    ELSE 0
                                END
                            END <> 0";
            p.Add("@PTR_Display", showAll, DbType.Boolean);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<TimeReason>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<TimeMonitor>> IRouteCardRepository.GetTimeMonitor(int rch, int prc, int rci, int userID)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT *
                           FROM Production_PTM_Process_Time_Monitor
                           WHERE PTM_RCH_IX = @RCH_IX and PTM_PRC_IX = @PRC_IX and PTM_RCI_IX = @RCI_IX and PTM_UN_IX = @UN_IX";
            p.Add("@RCH_IX", rch, DbType.Int32);
            p.Add("@PRC_IX", prc, DbType.Int32);
            p.Add("@RCI_IX", rci, DbType.Int32);
            p.Add("@UN_IX", userID, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<TimeMonitor>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<Consumable>> IRouteCardRepository.GetConsumable(int rch, int prc, int rci)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT CI.*, UN.UN_Initials as PCI_UN_Initials
                           FROM Production_PCI_Product_Consumable_Items CI
                           INNER JOIN UN_User_Names UN on UN.UN_IX = CI.PCI_UN_IX
                           WHERE PCI_RCH_IX = @RCH_IX and PCI_PRC_IX = @PRC_IX and PCI_RCI_IX = @RCI_IX";
            p.Add("@RCH_IX", rch, DbType.Int32);
            p.Add("@PRC_IX", prc, DbType.Int32);
            p.Add("@RCI_IX", rci, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                return (await conn.QueryAsync<Consumable>(sql, p, commandType: CommandType.Text)).ToList();
            }
        }

        async Task<List<TraceCode>> IRouteCardRepository.GetTraceCode(TraceCode traceCode)
        {
            var p = new DynamicParameters();
            string sql = @"Select Top 1 TC_IX, TC_Trace_Code, TC_PIN, CAST(TC_Date_Code as DATE) as TC_Date_Code, 
                           CAST(TC_Shelf_Expiry_Date as DATE) as TC_Shelf_Expiry_Date, TCX_Description, GPIN_Description
                           From Stock_TC_Trace_Codes 
                           Inner Join Stock_TCX_Trace_Entry_Index on TCX_IX = TC_Alloc_Code_IX
                           Inner Join GPIN_Generic_PINS on GPIN = TC_PIN
                           Where TC_Void = 0 and TC_Alloc_Code_IX <> 7 and TC_Trace_Code = @TC_Trace_Code";
            p.Add("@TC_Trace_Code", traceCode.TC_Trace_Code, DbType.String);
            using (IDbConnection conn = Connection)
            {
                return (await conn.QueryAsync<TraceCode>(sql, p, commandType: CommandType.Text)).ToList();
            }
        }

        async Task<List<Measurement>> IRouteCardRepository.GetMeasurement(int prs_ix)
        {
            var p = new DynamicParameters();
            string sql = @"Select * From Production_PMR_Measurements Where PMR_PRS_IX = @PMR_PRS_IX";
            p.Add("@PMR_PRS_IX", prs_ix, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                return (await conn.QueryAsync<Measurement>(sql, p, commandType: CommandType.Text)).ToList();
            }
        }

        async Task<List<Measurement>> IRouteCardRepository.InsertMeasurement(Measurement measurement)
        {
            var p = new DynamicParameters();
            p.Add("@PMR_PRS_IX", measurement.PMR_PRS_IX, DbType.Int32);
            string sql = @"INSERT INTO 
	                       Production_PMR_Measurements(PMR_PRC_IX, PMR_PRS_IX, PMR_RCI_IX, PMR_BH_IX, PMR_Type, PMR_Data)
                           
                           VALUES(@PMR_PRC_IX, @PMR_PRS_IX, @PMR_RCI_IX, @PMR_BH_IX, @PMR_Type, @PMR_Data);
                           
                           SELECT CAST(SCOPE_IDENTITY() as int)";
            // Create conection
            using (IDbConnection conn = Connection)
            {
                var newID = await conn.ExecuteScalarAsync<int>(sql, measurement);
                if (newID > 0)
                {
                    sql = @"Select * From Production_PMR_Measurements Where PMR_PRS_IX = @PMR_PRS_IX";
                    return (await conn.QueryAsync<Measurement>(sql, p, commandType: CommandType.Text)).ToList();
                }
                // Return error object if not inserted
                List<Measurement> measurements = new List<Measurement>();
                measurements.Add(new Measurement() { PMR_IX = -1 });
                return measurements;
            }
        }

        async Task<List<Shortage>> IRouteCardRepository.GetShortagesAsync(int hdrIX, int prcIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT s.*, u.UN_Initials as CSD_UN_Initials_Reported, 
                           CASE WHEN CSD_Sys_Shortage = 1 THEN 'Known Shortage' ELSE[master].PRS_Step_Text END as CSD_Process
                           FROM Production_CSD_Component_Shortage_Details s
                           LEFT OUTER JOIN Production_PRC_Production_Route_Card c on c.PRC_IX = s.CSD_PRC_IX and c.PRC_PCH_IX = s.CSD_RCH_IX
                           LEFT OUTER JOIN Production_PRS_Process_Steps [master] on [master].PRS_Step_IX = s.CSD_Parent_PRS_IX
                           LEFT OUTER JOIN UN_User_Names u on u.UN_IX = s.CSD_UN_IX_Reported
                           WHERE CSD_RCH_IX = @RCH_IX
                           AND (CASE WHEN @PRC_IX > 0 THEN 
                                    CASE 
                                    WHEN @PRC_IX = s.CSD_PRC_IX or CSD_Sys_Shortage = 1 THEN 1 ELSE 0 END
                                    ELSE 1
                               END) = 1
                            ORDER BY [master].PRS_Order";
            p.Add("@RCH_IX", hdrIX, DbType.Int32);
            p.Add("@PRC_IX", prcIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<Shortage>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<ScanHistory>> IRouteCardRepository.GetScanHistoryAsync(int stepIX, int stIX, int bhIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT * FROM Production_PSH_Product_Scan_History
                           WHERE PSH_PRC_IX = @PSH_PRC_IX AND PSH_ST_IX = @PSH_ST_IX AND PSH_BH_IX = @PSH_BH_IX";
            p.Add("@PSH_PRC_IX", stepIX, DbType.Int32);
            p.Add("@PSH_ST_IX", stIX, DbType.Int32);
            p.Add("@PSH_BH_IX", bhIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<ScanHistory>(sql, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<POSTStatus> IRouteCardRepository.AddProcessComment(ProcessComment processComment)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@PRC_PCH_IX", processComment.PUC_PCH_IX, DbType.Int32);
            p.Add("@PDC_PRC_IX", processComment.PUC_PRC_IX, DbType.Int32);
            p.Add("@PUC_UN_IX", processComment.PUC_UN_IX, DbType.Int32);
            p.Add("@PUC_Comments", processComment.PUC_Comments, DbType.String);
            string sql = @"INSERT INTO 
	                       Production_PUC_Route_Card_Data_Comments(PUC_PCH_IX, PUC_PRC_IX, PUC_UN_IX, PUC_Edited, PUC_Comments)	
	                       VALUES(@PRC_PCH_IX, @PDC_PRC_IX, @PUC_UN_IX, CURRENT_TIMESTAMP, @PUC_Comments);";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Added: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Comment" });

        }

        async Task<POSTStatus> IRouteCardRepository.AddUAConfirmation(UAConfirmation uac)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@PAC_RCH_IX", uac.PAC_RCH_IX, DbType.Int32);
            p.Add("@PAC_PRC_IX", uac.PAC_PRC_IX, DbType.Int32);
            p.Add("@PAC_UN_IX", uac.PAC_UN_IX, DbType.Int32);
            p.Add("@PAC_Comment", uac.PAC_Comment, DbType.String);
            p.Add("@PAC_Ref_Number", uac.PAC_Ref_Number, DbType.Int32);
            string sql = @"INSERT INTO 
	                       Production_UA_Confirmation(PAC_RCH_IX, PAC_PRC_IX, PAC_UN_IX, PAC_Time, PAC_Comment, PAC_Ref_Number)	
	                       VALUES(@PAC_RCH_IX, @PAC_PRC_IX, @PAC_UN_IX, CURRENT_TIMESTAMP, @PAC_Comment, @PAC_Ref_Number);";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Added: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding UA Confirmation" });
        }

        async Task<Boolean> IRouteCardRepository.UAConfirmation(int hdrIX, int userID, int refIX, int stepIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT COUNT(DISTINCT PAC_Ref_Number) as Confirmed  
                            FROM Production_UA_Confirmation
                            WHERE PAC_RCH_IX = @RCH_IX and PAC_UN_IX = @UN_IX and PAC_Ref_Number = @REF_IX
                            AND (CASE WHEN @STEP_IX > -1 THEN 
                                        CASE WHEN PAC_PRC_IX = @STEP_IX THEN 1 ELSE 0 END
                                    ELSE 1
                                 END) = 1;";
            p.Add("@RCH_IX", hdrIX, DbType.Int32);
            p.Add("@UN_IX", userID, DbType.Int32);
            p.Add("@STEP_IX", stepIX, DbType.Int32);
            p.Add("@REF_IX", refIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<Boolean>(sql, p, commandType: CommandType.Text);

                return result.First();
            }
        }

        PhysicalFileResult IRouteCardRepository.GetImage(PCBImage image)
        {
            // var path = "//norserver03/Mirtec Data/PcbModel/MB-MIC-00611_001T_PMR015/SmallTop.jpg";
            //var path = "\\\\norserver03\\Mirtec Data\\PcbModel\\MB-MIC-00611_001T_PMR015\\SmallTop.jpg";
            // var decodedPath = System.Web.HttpUtility.UrlDecode(image.Param1).Trim();
            var decodedPath = image.Param1;
            return PhysicalFile(decodedPath, "image/jpeg");

        }

        PhysicalFileResult IRouteCardRepository.GetPDF(PCBImage image)
        {
            // var path = "//norserver03/Mirtec Data/PcbModel/MB-MIC-00611_001T_PMR015/SmallTop.jpg";
            //var path = "\\\\norserver03\\Mirtec Data\\PcbModel\\MB-MIC-00611_001T_PMR015\\SmallTop.jpg";
            // var decodedPath = System.Web.HttpUtility.UrlDecode(image.Param1).Trim();
            var decodedPath = image.Param1;
            PhysicalFileResult octet = PhysicalFile(decodedPath, "application/octet-stream");
            return PhysicalFile(decodedPath, "application/octet-stream");

        }

        // Get Images for ANY PMR Level        
        async Task<List<AOIImage>> IRouteCardRepository.AOIImages(string pin)
        {
            List<AOIImage> images = ImagesFromServer(pin);
            return await Task.Run(() => images.ToList());
        }

        // Get Images for specific PMR Level
        async Task<List<AOIImage>> IRouteCardRepository.AOIImagesByPMR(string pin, string pmr)
        {
            List<AOIImage> images = ImagesFromServer(pin, pmr);
            return await Task.Run(() => images.ToList());
        }

        async Task<List<FeedbackModel>> IRouteCardRepository.GetFeedback(int hdrIX)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT h.RCH_IX, h.RCH_Feedback, h.RCH_Last_Edit_By, h.RCH_Last_Edit_Date    
	                       FROM Production_RCH_Route_Card_Header h
	                       WHERE h.RCH_IX = @RCH_IX and h.RCH_Feedback is not null";
            p.Add("@RCH_IX", hdrIX, DbType.Int32);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<FeedbackModel>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<POSTStatus> IRouteCardRepository.UpdateFeedback(FeedbackModel feedback)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@RCH_IX", feedback.RCH_IX, DbType.Int32);
            p.Add("@RCH_Feedback", feedback.RCH_Feedback, DbType.String);
            p.Add("@RCH_Last_Edit_By", feedback.RCH_Last_Edit_By, DbType.String);
            string sql = @"UPDATE Production_RCH_Route_Card_Header
	                       SET RCH_Feedback = CASE WHEN RCH_Feedback IS null THEN + @RCH_Feedback + CHAR(13) ELSE RCH_Feedback + @RCH_Feedback + CHAR(13) END, 
                           RCH_Last_Edit_By = @RCH_Last_Edit_By, 
		                   RCH_Last_Edit_Date = CURRENT_TIMESTAMP 
	                       WHERE RCH_IX = @RCH_IX";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Feedback" });

        }

        async Task<POSTStatus> IRouteCardRepository.UpdateShortagesAsync(Shortage shortage)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@CSD_IX", shortage.CSD_IX, DbType.Int32);
            p.Add("@RCH_IX", shortage.CSD_RCH_IX, DbType.Int32);
            p.Add("@PRS_IX", shortage.CSD_PRS_IX, DbType.Int32);
            p.Add("@PRS_Parent_IX", shortage.CSD_Parent_PRS_IX, DbType.Int32);
            p.Add("@PRC_IX", shortage.CSD_PRC_IX, DbType.Int32);
            p.Add("@UN_IX_Fitted", shortage.CSD_UN_IX_Fitted_By, DbType.Int32);
            p.Add("@Confirm_Fitted", shortage.CSD_Confirmed_Fitted, DbType.String);
            p.Add("@CSD_Detail", shortage.CSD_Detail, DbType.String);
            string sql = @"UPDATE Production_CSD_Component_Shortage_Details
	                       SET
                           CSD_PRS_IX = @PRS_IX, 
                           CSD_Parent_PRS_IX = @PRS_Parent_IX,
                           CSD_PRC_IX = @PRC_IX,
                           CSD_UN_IX_Fitted_By = @UN_IX_Fitted,
                           CSD_Confirmed_Fitted = @Confirm_Fitted,
                           CSD_Detail = @CSD_Detail,
		                   CSD_Fitted = CURRENT_TIMESTAMP 
	                       WHERE CSD_RCH_IX = @RCH_IX AND CSD_IX = @CSD_IX";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Shortage" });

        }

        async Task<PreviousScanInfo> IRouteCardRepository.CheckPreviousProcessScan(Int32 prcIndex, Int32 snIndex, Int32 scanType, Int32 panelIndex)
        {
            string panelScanSQL = @"SELECT PSN_IX, PSN_Serial 
                                    FROM Serial_PSN_Product_Serial_Numbers 
                                    WHERE PSN_PIN_IX > 0 AND PSN_PIN_IX = @INDEX";                                    

            string sql = @"
                SELECT TOP 1 X.PRS_Step_Text as StepText, CONVERT(BIT, COALESCE(PSH.Scanned, 0)) as Scanned, Serial
                FROM(
                SELECT TOP 1 PRC.PRC_IX, PRS.PRS_Step_Text, PRC.PRC_ST_IX,
                  (SELECT PSN_Serial FROM Serial_PSN_Product_Serial_Numbers where PSN_IX = @sn_Index) as Serial
                FROM
                Production_PRC_Production_Route_Card PRC
                INNER JOIN Production_PRS_Process_Steps PRS ON PRC.PRC_PRS_IX = PRS.PRS_Step_IX
                WHERE PRC_Removed = 0 AND PRC_Parent_PRC_IX > 0 
                AND PRC_ST_IX IN(1, 2, 6)
                AND PRC_PCH_IX = (SELECT PRC_PCH_IX FROM Production_PRC_Production_Route_Card WHERE PRC_IX = @prc_Index) 
                AND PRC_Ordering <  (SELECT PRC_Ordering FROM Production_PRC_Production_Route_Card WHERE PRC_IX = @prc_Index)
                ORDER BY PRC_Ordering DESC) X
                LEFT JOIN(
                SELECT PSH_PRC_IX, PSH_PSN_IX, COUNT(PSH_IX) [Scanned] 
                FROM Production_PSH_Product_Scan_History
                GROUP BY PSH_PRC_IX, PSH_PSN_IX) PSH ON X.PRC_IX = PSH.PSH_PRC_IX AND PSH.PSH_PSN_IX = @sn_Index";
            try {
                using (IDbConnection conn = Connection)
                {
                    // open connection
                    conn.Open();


                    // If a panel scan and validate and panel index > 0 ...
                    // Build a list of serials for this panel and loop
                    // Else, just process as a singleton.
                    IEnumerable<int> panelSerials = new List<int>();
                    if ((scanType == 6) && panelIndex > 0)
                    {
                        panelSerials = await conn.QueryAsync<int>(panelScanSQL, new
                        {
                            INDEX = panelIndex 
                        },
                        commandType: CommandType.Text);
                    }
                    else
                    {
                        panelSerials = panelSerials.Append(snIndex);
                    }

                    PreviousScanInfo scanInfo = null;
                    foreach (int serialIndex in panelSerials)
                    {
                        scanInfo = await conn.QueryFirstOrDefaultAsync<PreviousScanInfo>(sql, new
                        {
                            sn_index = serialIndex,
                            prc_index = prcIndex,
                            scan_Type = scanType
                        },
                        commandType: CommandType.Text);
                        if (!String.IsNullOrEmpty(scanInfo.StepText) && (scanInfo.Scanned == false))
                        {
                            return (PreviousScanInfo)scanInfo;
                        }
                    }
                    return scanInfo == null ? new PreviousScanInfo { StepText = "No Data", Scanned = null, Serial = string.Empty } : scanInfo; 
                }
            }
            catch (Exception e) {
                    var error = e;
                    return new PreviousScanInfo { StepText = "No Data", Scanned = null, Serial = string.Empty };
            }
         
        }

        async Task<PreviousBatchInfo> IRouteCardRepository.CheckPreviousBatchScan(int prcIndex, int bhIndex)
        {
            string sql = @"
                SELECT  X.PRS_Step_Text as StepText, COALESCE(PSH.Quantity, 0) as Quantity
                FROM(
                SELECT TOP 1 PRC.PRC_IX, PRS.PRS_Step_Text, PRC.PRC_ST_IX
                FROM
                Production_PRC_Production_Route_Card PRC
                INNER JOIN Production_PRS_Process_Steps PRS ON PRC.PRC_PRS_IX = PRS.PRS_Step_IX
                WHERE PRC_Removed = 0 AND PRC_Parent_PRC_IX > 0 
                AND PRC_ST_IX = 3
                AND PRC_PCH_IX = (SELECT PRC_PCH_IX FROM Production_PRC_Production_Route_Card WHERE PRC_IX = @prc_Index) 
                AND PRC_Ordering <  (SELECT PRC_Ordering FROM Production_PRC_Production_Route_Card WHERE PRC_IX = @prc_Index)
                ORDER BY PRC_Ordering DESC) X
                LEFT JOIN(
                SELECT PSH_PRC_IX, PSH_BH_IX, SUM(PSH_Quantity) [Quantity] 
                FROM Production_PSH_Product_Scan_History
                GROUP BY PSH_PRC_IX, PSH_BH_IX) PSH ON X.PRC_IX = PSH.PSH_PRC_IX AND PSH.PSH_BH_IX = @bh_Index";
        try {
            using (IDbConnection conn = Connection)
            {
                // open connection
                conn.Open();
                var previousInfo = await conn.QueryAsync<PreviousBatchInfo>(sql, new
                {
                    bh_index = bhIndex,
                    prc_index = prcIndex
                },
                    commandType: CommandType.Text);
                return (PreviousBatchInfo)previousInfo.First();
            }
            }
            catch (Exception)
            {
                return new PreviousBatchInfo { StepText = "No Data", Quantity = 0 };
            }
        }

        async Task<POSTStatus> IRouteCardRepository.AddShortagesAsync(Shortage shortage)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@CSD_RCH_IX", shortage.CSD_RCH_IX, DbType.Int32);
            p.Add("@CSD_Pin", shortage.CSD_Pin, DbType.String);
            p.Add("@CSD_Detail", shortage.CSD_Detail, DbType.String);
            p.Add("@CSD_Qty", shortage.CSD_Qty, DbType.Int32);
            p.Add("@CSD_Location", shortage.CSD_Location, DbType.String);
            p.Add("@CSD_PRS_IX", shortage.CSD_PRS_IX, DbType.Int32);
            p.Add("@CSD_Parent_PRS_IX", shortage.CSD_Parent_PRS_IX, DbType.Int32);
            p.Add("@CSD_PRC_IX", shortage.CSD_PRC_IX, DbType.Int32);
            p.Add("@CSD_UN_IX_Reported", shortage.CSD_UN_IX_Reported, DbType.Int32);
            string sql = @"
                    INSERT INTO 
                    Production_CSD_Component_Shortage_Details(CSD_RCH_IX,CSD_Pin,CSD_Detail,CSD_Qty,CSD_Location,CSD_PRS_IX,
                                                              CSD_Parent_PRS_IX,CSD_PRC_IX,CSD_UN_IX_Reported)
                    VALUES(@CSD_RCH_IX, @CSD_Pin, @CSD_Detail, @CSD_Qty, @CSD_Location, 
                           @CSD_PRS_IX, @CSD_Parent_PRS_IX, @CSD_PRC_IX, @CSD_UN_IX_Reported);";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Shortage" });
        }

        async Task<POSTStatus> IRouteCardRepository.AddScanHistory(ScanHistory history)
        {
            // Feature flag for PSN_BITFIELD Updates
            var updateBitfields = _config.GetSection("LegacyBitfield").GetValue("UseLegacy", false);
            // main insertion script
            string insertSQL = @"INSERT INTO 
                                Production_PSH_Product_Scan_History(PSH_PSN_IX, PSH_PRC_IX, PSH_PRS_IX, PSH_RCI_IX, PSH_BH_IX, PSH_ST_IX, PSH_Result, 
                                                        PSH_UN_IX, PSH_TTS_IX, PSH_By, PSH_Quantity, PSH_DateTime)
                                VALUES(@PSH_PSN_IX, @PSH_PRC_IX, @PSH_PRS_IX, @PSH_RCI_IX, @PSH_BH_IX, @PSH_ST_IX, @PSH_Result, @PSH_UN_IX, 
                                                        @PSH_TTS_IX, @PSH_By, @PSH_Quantity, CURRENT_TIMESTAMP);";

            // This checks to see if already processed in ERC system
            string foundSQL = @"SELECT TOP 1
                                PSH_PSN_IX as Found
                                FROM Production_PSH_Product_Scan_History
                                WHERE PSH_PRS_IX = @PSH_PRS_IX and PSH_ST_IX = @PSH_ST_IX and PSH_PSN_IX = @PSH_PSN_IX and PSH_PRC_IX = @PSH_PRC_IX";

            // This checks if serial valid for this batch
            string validSQL = @"SELECT TOP 1
                                PSN_IX
                                FROM Stock_BH_Batch_Headers BH
                                INNER JOIN Serial_PSN_Product_Serial_Numbers SN on SN.PSN_BH_IX = BH.BH_IX   
                                WHERE SN.PSN_IX = @PSH_PSN_IX and BH.BH_IX = @PSH_BH_IX";

            // Checks to see if we have legacy mapping
            string legacySQL = @"SELECT * FROM Production_LM_Legacy_Mapping
                                WHERE LM_PRS_Step_IX = @PSH_PRS_IX
                                ORDER BY LM_Legacy_Bitfield";

            // Gets PSN Serial record
            string serialSQL = @"SELECT TOP 1 
                                GPIN_Description, BH_Client,
                                    CASE 
										WHEN Line = 'LINE-1' THEN 1 
										WHEN Line = 'LINE-2' THEN 2
										WHEN Line = 'LINE-3' THEN 3
										ELSE 0 
									END as LINE,
                                    st.PSS_Text,
                                    s.* 
                                    From Stock_BH_Batch_Headers as b
                                    Inner Join GPIN_Generic_PINS g On b.BH_PIN = g.GPIN
                                    Inner Join Serial_PSN_Product_Serial_Numbers s on s.PSN_BH_IX = BH_IX
                                    Left Outer Join jobsetup j on j.BatchNo = b.BH_Number
                                    Left Outer Join Serial_PSS_Serial_Number_Status st on st.PSS_IX = s.PSN_PSS_IX
                                 WHERE PSN_IX = @INDEX";

            string panelScanSQL = @"SELECT PSN_IX 
                                    FROM Serial_PSN_Product_Serial_Numbers 
                                    WHERE PSN_PIN_IX > 0 AND PSN_PIN_IX = @INDEX";

            using (IDbConnection conn = Connection)
            {
                // open connection
                conn.Open();
                // create the transaction
                using (var transaction = conn.BeginTransaction())
                try 
                {
                    {
                        // Check to see if the serial is part of this batch
                        var valid = await conn.ExecuteScalarAsync<int>(validSQL, new { 
                                                                                        PSH_PSN_IX = history.PSH_PSN_IX, 
                                                                                        PSH_BH_IX = history.PSH_BH_IX 
                                                                                     }, 
                                                                                     transaction, commandType: CommandType.Text);

                        if (valid <= 0)
                        {
                            transaction.Rollback();
                            return await Task.Run(() => new POSTStatus
                            { StatusCode = -2, StatusMessage = "Serial not found in this batch" });
                        }

                        // Check to see if the serial has already been scanned for this process step combination in the ERC system
                        var found = await conn.ExecuteScalarAsync<int>(foundSQL,  new { PSH_PRS_IX = history.PSH_PRS_IX, 
                                                                                        PSH_ST_IX = history.PSH_ST_IX,
                                                                                        PSH_PSN_IX = history.PSH_PSN_IX, 
                                                                                        PSH_PRC_IX = history.PSH_PRC_IX 
                                                                                      }, 
                                                                                      transaction, commandType: CommandType.Text);
                        if (found > 0)
                        {
                            transaction.Rollback();
                            return await Task.Run(() => new POSTStatus
                            { StatusCode = -2, StatusMessage = "Serial already scanned for this process step" });
                        }

                        // If a panel scan and validate and panel index > 0 ...
                        // Build a list of serials for this panel and loop
                        // Else, just process as a singleton.
                        IEnumerable<int> panelSerials  = new List<int>();
                        if ((history.PSH_ST_IX == 6 || history.PSH_ST_IX == 7) && history.PSN_PIN_IX > 0) 
                        {
                                panelSerials = await conn.QueryAsync<int>(panelScanSQL, new
                                {
                                    INDEX = history.PSN_PIN_IX
                                },
                                transaction, commandType: CommandType.Text);
                        } else 
                        {
                            panelSerials = panelSerials.Append(history.PSH_PSN_IX);
                        }

                        var affectedRows = 0;
                        foreach (int serialIndex in panelSerials)
                        {          

                            // Now look at legacy mapping to see if we need to update PSN & SAT tables ...               
                            var legacy = await conn.QueryAsync<LegacyMap>(legacySQL, new { 
                                                                                            PSH_PRS_IX = history.PSH_PRS_IX 
                                                                                         }, 
                                                                                         transaction, commandType: CommandType.Text);
                            // If we have a legacy mapping then process accordingly
                            if (legacy.Count() > 0)
                            {
                                // 0. This might be a double sided scan so see if bitfield_2 populated by querying PSN records
                                // 1. Determine bitfield to update - 1 or 2 for this process from legacy mapping
                                // 2. Update PSN, if bitfield 1 also set then put into build
                                // 3. Add SAT Audit reecord
                                // 4. Commit transaction

                                // Set the relevant bitfields from mapping record
                                // If BF2 is already set and we have DS SMT and BF1 is set then set IN BUILD

                                var psnSerial =  await conn.QueryAsync<PSNSerial>(serialSQL,  new { 
                                                                                                    INDEX = serialIndex // history.PSH_PSN_IX 
                                                                                                }, 
                                                                                                    transaction, commandType: CommandType.Text);
                                // If not DS AND SMT just check to see if the serial sentry has already been scanned
                                if (psnSerial.First().PSN_SMT_Dbl_Sided == false)                             
                                    {
                                        // if NOT then also update the serial bitfield and add audit record
                                        if (CheckBitfield(psnSerial.First().PSN_Bitfield, (legacy.First().LM_Legacy_Bitfield + legacy.First().LM_Legacy_Bitfield_2)) == false) 
                                        {
                                                // create SQL to Update PSN Serial
                                                string updateSSSerialSQL = @"UPDATE Serial_PSN_Product_Serial_Numbers
                                                                            SET PSN_BITFIELD = 
                                                                                CASE WHEN PSN_BITFIELD IS NOT NULL 
                                                                                    THEN PSN_BITFIELD + @BF1
                                                                                    ELSE @BF1
                                                                                END,  
                                                                                PSN_STA_IX =                                                                
                                                                                CASE WHEN @LINE > 0
                                                                                    THEN @LINE
                                                                                    ELSE PSN_STA_IX
                                                                                END,
                                                                                PSN_PSS_IX = 
                                                                                CASE 
                                                                                WHEN PSN_PSS_IX = 2
                                                                                    THEN @INBUILD 
                                                                                    ELSE PSN_PSS_IX 
                                                                                END
                                                                            WHERE PSN_IX = @PSH_PSN_IX";
                                            // Check if worked ..      
                                            if (updateBitfields) 
                                            {                                      
                                                affectedRows = await conn.ExecuteAsync(updateSSSerialSQL,  new {  BF1 = legacy.First().LM_Legacy_Bitfield + legacy.First().LM_Legacy_Bitfield_2,
                                                                                                                LINE = psnSerial.First().LINE,
                                                                                                                INBUILD =  4,
                                                                                                                PSH_PSN_IX = psnSerial.First().PSN_IX 
                                                                                                            }, 
                                                                                                            transaction, commandType: CommandType.Text);     
                                            }                                   
                                                // Add audit record ....
                                        }
                                    } 
                                    // if DS AND NOT SMT
                                    else    
                                    if ((psnSerial.First().PSN_SMT_Dbl_Sided == true) & (!legacy.First().LM_Legacy_Process.ToUpper().StartsWith("SMT")))
                                    {
                                        if (CheckBitfield(psnSerial.First().PSN_Bitfield, (legacy.First().LM_Legacy_Bitfield + legacy.First().LM_Legacy_Bitfield_2)) == false) {
                                                // create SQL to Update PSN Serial
                                                string updateSSSerialSQL = @"UPDATE Serial_PSN_Product_Serial_Numbers
                                                                            SET PSN_BITFIELD = 
                                                                                CASE WHEN PSN_BITFIELD IS NOT NULL 
                                                                                    THEN PSN_BITFIELD + @BF1
                                                                                    ELSE @BF1
                                                                                END,  
                                                                                PSN_STA_IX =                                                                
                                                                                CASE WHEN @LINE > 0
                                                                                    THEN @LINE
                                                                                    ELSE PSN_STA_IX
                                                                                END,
                                                                                PSN_PSS_IX = 
                                                                                CASE 
                                                                                WHEN PSN_PSS_IX = 2
                                                                                    THEN @INBUILD 
                                                                                    ELSE PSN_PSS_IX 
                                                                                END
                                                                            WHERE PSN_IX = @PSH_PSN_IX";
                                                // Check if worked ..
                                                if (updateBitfields)
                                                {
                                                    affectedRows += await conn.ExecuteAsync(updateSSSerialSQL, new {  
                                                                                                                BF1 = legacy.First().LM_Legacy_Bitfield + legacy.First().LM_Legacy_Bitfield_2,
                                                                                                                LINE = psnSerial.First().LINE,
                                                                                                                INBUILD =  4,
                                                                                                                PSH_PSN_IX = psnSerial.First().PSN_IX  
                                                                                                            }, 
                                                                                                            transaction, commandType: CommandType.Text);  
                                                }                                     
                                                // Add audit record ....
                                        }
                                    }
                                // If DS AND SMT
                                else 
                                // if DS SMT and the current process is SMT [Bottom || Top]
                                if ((psnSerial.First().PSN_SMT_Dbl_Sided == true) & (legacy.First().LM_Legacy_Process.ToUpper().StartsWith("SMT"))) 
                                {
                                        // if not scanned then update bitfield
                                        if (CheckBitfield(psnSerial.First().PSN_Bitfield2, legacy.First().LM_Legacy_Bitfield_2) == false) {
                                            // create SQL to Update PSN Serial
                                            string updateBF2SerialSQL = @"UPDATE Serial_PSN_Product_Serial_Numbers
                                                                        SET PSN_BITFIELD2 = 
                                                                        CASE WHEN PSN_BITFIELD2 IS NOT NULL 
                                                                                THEN PSN_BITFIELD2 + @BF2
                                                                                ELSE @BF2
                                                                        END,        
                                                                        PSN_STA_IX =                                                                
                                                                        CASE WHEN @LINE > 0
                                                                            THEN @LINE
                                                                            ELSE PSN_STA_IX
                                                                        END
                                                                        WHERE PSN_IX = @PSH_PSN_IX";
                                            // Check if worked ..
                                            if (updateBitfields)
                                            {
                                                affectedRows += await conn.ExecuteAsync(updateBF2SerialSQL, new {  
                                                                                                                BF2 = legacy.First().LM_Legacy_Bitfield_2,
                                                                                                                LINE = psnSerial.First().LINE,
                                                                                                                PSH_PSN_IX = psnSerial.First().PSN_IX 
                                                                                                        },  
                                                                                                        transaction, commandType: CommandType.Text);
                                            }
                                            // Add audit record ....
                                        } else 
                                            // if bitfield 2 is set so then check to see if bitfield 1 is not
                                            if (CheckBitfield(psnSerial.First().PSN_Bitfield2, legacy.First().LM_Legacy_Bitfield_2) & 
                                            (CheckBitfield(psnSerial.First().PSN_Bitfield, legacy.First().LM_Legacy_Bitfield) == false)) 
                                            {
                                                    // create SQL to Update PSN Serial
                                                    // Also place into build as both sides scanned
                                                    string updateBF1SerialSQL = @"UPDATE Serial_PSN_Product_Serial_Numbers
                                                                                SET PSN_BITFIELD = 
                                                                                CASE WHEN PSN_BITFIELD IS NOT NULL 
                                                                                    THEN PSN_BITFIELD + @BF1
                                                                                    ELSE @BF1
                                                                                END,  
                                                                                PSN_STA_IX =                                                                
                                                                                CASE WHEN @LINE > 0
                                                                                    THEN @LINE
                                                                                    ELSE PSN_STA_IX
                                                                                END,
                                                                                PSN_PSS_IX = CASE 
                                                                                    WHEN PSN_PSS_IX = 2
                                                                                        THEN @INBUILD 
                                                                                        ELSE PSN_PSS_IX 
                                                                                    END
                                                                                WHERE PSN_IX = @PSH_PSN_IX";
                                                    // Check if worked ..
                                                    if (updateBitfields)
                                                    {
                                                        affectedRows += await conn.ExecuteAsync(updateBF1SerialSQL, new {  
                                                                                                                BF1 = legacy.First().LM_Legacy_Bitfield,
                                                                                                                LINE = psnSerial.First().LINE,
                                                                                                                INBUILD =  4,
                                                                                                                PSH_PSN_IX = psnSerial.First().PSN_IX  
                                                                                                            },   
                                                                                                            transaction, commandType: CommandType.Text);
                                                    }
                                                    // Add audit record ....
                                        }
                                }
                                    // Now create SAT Audit Record
                                    // Create PARAMS
                                    var p = new DynamicParameters();
                                    p.Add("@SAT_PSN_IX", psnSerial.First().PSN_IX, DbType.Int32);
                                    p.Add("@SAT_Side", legacy.First().LM_SAT_Side, DbType.Int32);
                                    p.Add("@SAT_Serial", psnSerial.First().PSN_Serial, DbType.String);
                                    p.Add("@SAT_PSN_PIN", psnSerial.First().PSN_PIN, DbType.String);
                                    p.Add("@SAT_Client", psnSerial.First().BH_Client, DbType.String);
                                    p.Add("@SAT_Description", psnSerial.First().GPIN_Description, DbType.String);
                                    p.Add("@SAT_Batch", psnSerial.First().PSN_Batch_Number, DbType.Int32);
                                    p.Add("@SAT_PSS_Text", psnSerial.First().PSS_Text, DbType.String);
                                    p.Add("@SAT_Event", "Station Scan", DbType.String);
                                    p.Add("@SAT_Kit_Batch", psnSerial.First().PSN_Kit_Batch, DbType.Int32);
                                    p.Add("@SAT_Station", legacy.First().LM_PRS_Step_Text, DbType.String);
                                    p.Add("@SAT_STA_ID", history.Device_Name, DbType.String);
                                    p.Add("@SAT_Action_By", history.PSH_By, DbType.String);
                                    p.Add("@SAT_Comments", psnSerial.First().PSN_Stock_Trace, DbType.String);
                                    p.Add("@SAT_BNA_IX", psnSerial.First().PSN_BNA_IX, DbType.Int32);
                                    p.Add("@SAT_Trace", psnSerial.First().PSN_Stock_Trace, DbType.String);
                                    p.Add("@SAT_Extended", "Added via iERC", DbType.String);
                                    p.Add("@SAT_STA_IX", psnSerial.First().LINE, DbType.Int32);


                                    string satAuditSQL = @"INSERT INTO Serial_SAT_Audit_Trail(SAT_PSN_IX, SAT_Side, SAT_Serial, SAT_PSN_PIN, SAT_Client, 
                                                        SAT_Description, SAT_Batch, SAT_PSS_Text, SAT_Event, SAT_Kit_Batch, SAT_Date_Time,
                                                        SAT_Station, SAT_STA_ID, SAT_Action_By, SAT_Comments, SAT_BNA_IX, SAT_Trace, SAT_Extended, SAT_STA_IX)
                                                        VALUES(@SAT_PSN_IX, @SAT_Side, @SAT_Serial, @SAT_PSN_PIN, @SAT_Client, 
                                                            @SAT_Description, @SAT_Batch, @SAT_PSS_Text, @SAT_Event, @SAT_Kit_Batch, CURRENT_TIMESTAMP,
                                                            @SAT_Station, @SAT_STA_ID, @SAT_Action_By, @SAT_Comments, @SAT_BNA_IX, @SAT_Trace, @SAT_Extended, @SAT_STA_IX)";

                                // Check if worked ..
                                if (updateBitfields)
                                {
                                    affectedRows += await conn.ExecuteAsync(satAuditSQL, p, transaction, commandType: CommandType.Text);
                                }

                            }


                                // Now update scan history to avoid trigger issue
                                affectedRows += await conn.ExecuteAsync(insertSQL, new
                                    {
                                        PSH_PSN_IX = serialIndex, // history.PSH_PSN_IX,
                                        PSH_PRC_IX = history.PSH_PRC_IX,
                                        PSH_PRS_IX = history.PSH_PRS_IX,
                                        PSH_RCI_IX = history.PSH_RCI_IX,
                                        PSH_BH_IX = history.PSH_BH_IX,
                                        PSH_ST_IX = history.PSH_ST_IX,
                                        PSH_Result = history.PSH_Result,
                                        PSH_UN_IX = history.PSH_UN_IX,
                                        PSH_TTS_IX = history.PSH_TTS_IX,
                                        PSH_Quantity = history.PSH_Quantity,
                                        PSH_By = history.PSH_By
                                    },
                                    transaction, commandType: CommandType.Text);


                        }

                    
                        // Commit at this point
                        transaction.Commit();
                        // Else we're already done so return status
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row(s)") });
                
                    } 
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    // Arrived here as update failed so return an error object
                    return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = $"Error Updating Serial Entry: {ex.Message}" });
                }                
            } // Connection
        }

        async Task<POSTStatus> IRouteCardRepository.AddUserDataHistory(ScanHistory history)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@PSH_PSN_IX", history.PSH_PSN_IX, DbType.Int32);
            p.Add("@PSH_PRC_IX", history.PSH_PRC_IX, DbType.Int32);
            p.Add("@PSH_PRS_IX", history.PSH_PRS_IX, DbType.Int32);
            p.Add("@PSH_RCI_IX", history.PSH_RCI_IX, DbType.Int32);
            p.Add("@PSH_BH_IX", history.PSH_BH_IX, DbType.Int32);
            p.Add("@PSH_ST_IX", history.PSH_ST_IX, DbType.Int32);
            p.Add("@PSH_Result", history.PSH_Result, DbType.Int32);
            p.Add("@PSH_UN_IX", history.PSH_UN_IX, DbType.Int32);
            p.Add("@PSH_TTS_IX", history.PSH_TTS_IX, DbType.Int32);
            p.Add("@PSH_Quantity", history.PSH_Quantity, DbType.Int32);
            p.Add("@PSH_By", history.PSH_By, DbType.String);
            // Main insertion script
            string insertSQL = @"INSERT INTO 
                                    Production_PSH_Product_Scan_History(PSH_PSN_IX, PSH_PRC_IX, PSH_PRS_IX, PSH_RCI_IX, PSH_BH_IX, PSH_ST_IX, PSH_Result, 
                                                        PSH_UN_IX, PSH_TTS_IX, PSH_By, PSH_Quantity, PSH_DateTime)
                                    VALUES(@PSH_PSN_IX, @PSH_PRC_IX, @PSH_PRS_IX, @PSH_RCI_IX, @PSH_BH_IX, @PSH_ST_IX, @PSH_Result, @PSH_UN_IX, 
                                                        @PSH_TTS_IX, @PSH_By, @PSH_Quantity, CURRENT_TIMESTAMP);";
            // Remaining validation
            string checkSQL = @"SELECT TOP 1 MAX(BH_QTY) as Remainers 
                                FROM Stock_BH_Batch_Headers b 
                                LEFT OUTER join Production_PSH_Product_Scan_History SH ON sh.PSH_BH_IX = b.BH_IX
                                AND PSH_PRS_IX = @PSH_PRS_IX and PSH_ST_IX = @PSH_ST_IX and PSH_PSN_IX = @PSH_PSN_IX and PSH_PRC_IX = @PSH_PRC_IX
                                WHERE b.BH_Number = @PSH_PSN_IX";

            using (IDbConnection conn = Connection)
            {
                // Check to see if the remaining quantity is less than presented amount ..
                var remaining = await conn.ExecuteScalarAsync<int>(checkSQL, p);
                if (remaining < history.PSH_Quantity)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = -2, StatusMessage = String.Format("Quantity exceeds remaining for this process step.\n\nRemaining: {0}", remaining) });
                }
                // Not found so do the update as planned ....
                var affectedRows = await conn.ExecuteAsync(insertSQL, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding User Data History" });
        }


        async Task<POSTStatus> IRouteCardRepository.AddUserConfirmationHistory(ScanHistory history)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@PSH_PSN_IX", history.PSH_PSN_IX, DbType.Int32);
            p.Add("@PSH_PRC_IX", history.PSH_PRC_IX, DbType.Int32);
            p.Add("@PSH_PRS_IX", history.PSH_PRS_IX, DbType.Int32);
            p.Add("@PSH_RCI_IX", history.PSH_RCI_IX, DbType.Int32);
            p.Add("@PSH_BH_IX", history.PSH_BH_IX, DbType.Int32);
            p.Add("@PSH_ST_IX", history.PSH_ST_IX, DbType.Int32);
            p.Add("@PSH_Result", history.PSH_Result, DbType.Int32);
            p.Add("@PSH_UN_IX", history.PSH_UN_IX, DbType.Int32);
            p.Add("@PSH_TTS_IX", history.PSH_TTS_IX, DbType.Int32);
            p.Add("@PSH_Quantity", history.PSH_Quantity, DbType.Int32);
            p.Add("@PSH_By", history.PSH_By, DbType.String);
            // Add validation to ensure we can't have multiple sign-offs for each TTS in concurrent usage
            string userAckSQL = @"SELECT TOP 1 UN_Initials as [User]
                                  FROM Production_PSH_Product_Scan_History sh
                                  INNER JOIN UN_User_Names un on un.UN_IX = sh.PSH_UN_IX 
                                  WHERE PSH_PRC_IX = @PSH_PRC_IX AND PSH_PSN_IX = @PSH_PSN_IX AND PSH_ST_IX = @PSH_ST_IX 
                                  AND PSH_PRS_IX = @PSH_PRS_IX AND PSH_TTS_IX = @PSH_TTS_IX";
            // Main insertion script
            string insertSQL = @"INSERT INTO 
                                    Production_PSH_Product_Scan_History(PSH_PSN_IX, PSH_PRC_IX, PSH_PRS_IX, PSH_RCI_IX, PSH_BH_IX, PSH_ST_IX, PSH_Result, 
                                                        PSH_UN_IX, PSH_TTS_IX, PSH_By, PSH_Quantity, PSH_DateTime)
                                    VALUES(@PSH_PSN_IX, @PSH_PRC_IX, @PSH_PRS_IX, @PSH_RCI_IX, @PSH_BH_IX, @PSH_ST_IX, @PSH_Result, @PSH_UN_IX, 
                                                        @PSH_TTS_IX, @PSH_By, @PSH_Quantity, CURRENT_TIMESTAMP);";

            using (IDbConnection conn = Connection)
            {

                var user = await conn.ExecuteScalarAsync<string>(userAckSQL, p);
                if (!string.IsNullOrEmpty(user))
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = -2, StatusMessage = String.Format("Process step already has a sign-off for Team Member: {0}", user) });
                }
                var affectedRows = await conn.ExecuteAsync(insertSQL, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding User Confirmation History" });
        }

        async Task<POSTStatus> IRouteCardRepository.AddUserSignoff(SignOff signOff)
        {
            if (signOff.PSO_PRC_IX > 0)
            {
                // Create PARAMS
                var p = new DynamicParameters();
                p.Add("@PSO_RN_Tracking_ID", signOff.PSO_RN_Tracking_ID, DbType.String);
                p.Add("@PSO_PRC_IX", signOff.PSO_PRC_IX, DbType.Int32);
                p.Add("@PSO_PCH_IX", signOff.PSO_PCH_IX, DbType.Int32);
                p.Add("@PSO_RCI_IX", signOff.PSO_RCI_IX, DbType.Int32);
                //p.Add("@PSO_PRS_IX", signOff.PSO_PRS_IX, DbType.Int32); // This field is deprecated
                p.Add("@PSO_PRS_Step_IX", signOff.PSO_PRS_Step_IX, DbType.Int32);
                p.Add("@PSO_User", signOff.PSO_User, DbType.Int32);
                p.Add("@PSO_Approved_By", signOff.PSO_Approved_By, DbType.Int32);
                p.Add("@PSO_PSN_IX", signOff.PSO_PSN_IX, DbType.Int32);
                p.Add("@PSO_PSN_Serial", signOff.PSO_PSN_Serial, DbType.String);
                p.Add("@PSO_Quantity", signOff.PSO_Quantity, DbType.Int32);
                p.Add("@PSO_Comments", signOff.PSO_Comments, DbType.String);
                // Main insertion script
                string insertSQL = @"INSERT INTO 
                                        Production_PSO_Sign_Offs(PSO_RN_Tracking_ID, PSO_PRC_IX, PSO_PCH_IX, PSO_RCI_IX, PSO_PRS_Step_IX, PSO_User, PSO_Request_Date,
                                                                 PSO_Approved_By, PSO_PSN_IX, PSO_PSN_Serial, PSO_Quantity, PSO_Comments)
                                        VALUES(@PSO_RN_Tracking_ID, @PSO_PRC_IX, @PSO_PCH_IX, @PSO_RCI_IX, @PSO_PRS_Step_IX, @PSO_User, CURRENT_TIMESTAMP,
                                               @PSO_Approved_By, @PSO_PSN_IX, @PSO_PSN_Serial, @PSO_Quantity, @PSO_Comments);";

                using (IDbConnection conn = Connection)
                {

                    var affectedRows = await conn.ExecuteAsync(insertSQL, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Added: {0} {1}", affectedRows, "Row") });
                    }
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Authorisation User Signoff" });
        }

        async Task<POSTStatus> IRouteCardRepository.UpdateUserSignoff(SignOff signOff)
        {
            if (signOff.PSO_IX > 0)
            {
                // Create PARAMS
                var p = new DynamicParameters();
                p.Add("@PSO_IX", signOff.PSO_IX, DbType.Int32);
                p.Add("@PSO_Approved_By", signOff.PSO_Approved_By, DbType.Int32);
                p.Add("@PSO_PSN_IX", signOff.PSO_PSN_IX, DbType.Int32);
                p.Add("@PSO_PSN_Serial", signOff.PSO_PSN_Serial, DbType.String);
                p.Add("@PSO_Quantity", signOff.PSO_Quantity, DbType.Int32);
                p.Add("@PSO_Comments", signOff.PSO_Comments + Environment.NewLine, DbType.String);
                p.Add("@PSO_Completed", DateTime.Now, DbType.DateTime);
                p.Add("@PSO_Status", signOff.PSO_Status, DbType.String);
                // Main insertion script
                string insertSQL = @"UPDATE Production_PSO_Sign_Offs
                                     SET PSO_Approved_By = @PSO_Approved_By, PSO_PSN_IX = @PSO_PSN_IX, PSO_PSN_Serial = @PSO_PSN_Serial,
                                     PSO_Quantity = @PSO_Quantity, PSO_Comments = PSO_Comments + @PSO_Comments, 
                                     PSO_Completed = @PSO_Completed, PSO_Status = @PSO_Status
                                     WHERE PSO_IX = @PSO_IX";


                using (IDbConnection conn = Connection)
                {

                    var affectedRows = await conn.ExecuteAsync(insertSQL, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated Authorisation User Signoff: {0} {1}", affectedRows, "Row") });
                    }
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Authorisation User Signoff" });
        }


        async Task<POSTStatus> IRouteCardRepository.AddTimeMonitor(TimeMonitor timeMonitor)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@RCH_IX", timeMonitor.PTM_RCH_IX, DbType.Int32);
            p.Add("@PRC_IX", timeMonitor.PTM_PRC_IX, DbType.Int32);
            p.Add("@RCI_IX", timeMonitor.PTM_RCI_IX, DbType.Int32);
            p.Add("@UN_IX", timeMonitor.PTM_UN_IX, DbType.Int32);
            p.Add("@PTR_IX", timeMonitor.PTM_PTR_IX, DbType.Int32);
            p.Add("@PTS_IX", timeMonitor.PTM_PTS_IX, DbType.Int32);

            // This returns current status of process step .. to see if already completed
            string statusSQL = @"SELECT TOP 1
                                PTM_PTS_IX as procStatus
                                FROM Production_PTM_Process_Time_Monitor m
                                INNER JOIN Production_PTS_Process_State s on m.PTM_PTS_IX = s.PTS_State
                                WHERE PTM_RCH_IX = @RCH_IX AND PTM_PRC_IX = @PRC_IX AND PTM_UN_IX = @UN_IX AND PTM_RCI_IX = @RCI_IX
                                AND s.PTS_Description = 'Complete'
                                ORDER BY PTM_IX DESC";

            // Main insertion script
            string insertSQL = @"INSERT INTO 
                                Production_PTM_Process_Time_Monitor(PTM_RCH_IX, PTM_PRC_IX, PTM_RCI_IX, PTM_UN_IX, PTM_PTR_IX, PTM_PTS_IX, PTM_Event_Time)
                                VALUES(@RCH_IX, @PRC_IX, @RCI_IX, @UN_IX, @PTR_IX, @PTS_IX, CURRENT_TIMESTAMP);";

            using (IDbConnection conn = Connection)
            {
                // Check to see what current status of process step is for this user
                var status = await conn.ExecuteScalarAsync<int>(statusSQL, p);
                if (status > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = -2, StatusMessage = "This process step already complete" });
                }

                // Valid Entry as state change and not completed (PTS_State)
                var affectedRows = await conn.ExecuteAsync(insertSQL, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Time Monitor Entry" });
        }


        async Task<POSTStatus> IRouteCardRepository.AddConsumable(Consumable consumable)
        {
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@RCH_IX", consumable.PCI_RCH_IX, DbType.Int32);
            p.Add("@PRC_IX", consumable.PCI_PRC_IX, DbType.Int32);
            p.Add("@RCI_IX", consumable.PCI_RCI_IX, DbType.Int32);
            p.Add("@Pin", consumable.PCI_Pin, DbType.String);
            p.Add("@Trace", consumable.PCI_Trace, DbType.String);
            p.Add("@Expiry", consumable.PCI_Expiry, DbType.DateTime);
            p.Add("@Comments", consumable.PCI_Comments, DbType.String);
            p.Add("@Department", consumable.PCI_Department, DbType.String);
            p.Add("@UN_IX", consumable.PCI_UN_IX, DbType.Int32);
            string sql = @"
                    INSERT INTO 
                    Production_PCI_Product_Consumable_Items(PCI_RCH_IX, PCI_PRC_IX, PCI_RCI_IX, PCI_Pin, PCI_Trace, PCI_Expiry, 
                               PCI_Comments, PCI_Department, PCI_UN_IX, PCI_Date)
                    VALUES(@RCH_IX, @PRC_IX, @RCI_IX, @Pin, @Trace, @Expiry, @Comments, @Department, @UN_IX, CURRENT_TIMESTAMP);";

            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                // Check if worked ..
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Consumable" });
        }




        private List<AOIImage> ImagesFromServer(string pin, string pmr = "")
        {
            var images = new List<AOIImage>();
            Task.Factory.StartNew(() => CollectAOIData(pin, pmr));
            Parallel.ForEach(collection.GetConsumingEnumerable(), file =>
            {
                images.Add(new AOIImage
                {
                    IMG_Path = file.FullName,
                    IMG_Pin = this.RestoreRoHSPin(pin),
                    IMG_PMR = file.FullName.Substring(file.FullName.Length - 7, 3),
                    IMG_Location = @"Archive PCB Library",
                    IMG_Side = (file.Name.Substring(16, 1).Equals("B") ? @"Bottom" : @"Top")
                });
            });


            return images.OrderBy(o => o.IMG_PMR).ThenByDescending(s => s.IMG_Location).ToList();
        }

        private void CollectAOIData(string pin, string pmr = "")
        {
            try
            {
                string path1 = _config.GetSection("AOIPaths").GetValue("MV3", "");
                string path2 = _config.GetSection("AOIPaths").GetValue("OMNI", ""); ;
                string path3 = _config.GetSection("AOIPaths").GetValue("Archive", "") + pin.Substring(3, 3) + @"\";
                // Just get the actual PIN section = 16 chars
                string pinNumber = this.ConvertRoHSPin(pin);

                if (pmr.Length > 0)
                {

                    /*
                    if (new DirectoryInfo(path1).Exists)                    
                    {
                        foreach (var dir in new DirectoryInfo(path1).EnumerateDirectories("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true & d.Name.Contains(pmr)))
                        {
                            foreach (var file in dir.EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(f => f.Extension == ".jpg"))
                            {
                                collection.Add(file);
                            }
                        }
                    }

                    if (new DirectoryInfo(path2).Exists)
                    {
                        foreach (var dir in new DirectoryInfo(path2).EnumerateDirectories("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true & d.Name.Contains(pmr)))
                        {
                            foreach (var file in dir.EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(f => f.Extension == ".jpg"))
                            {
                                collection.Add(file);
                            }
                        }
                    }
                    */

                    if ((new DirectoryInfo(path3).Exists) & (collection.Count <= 0))
                    {
                        foreach (var file in new DirectoryInfo(path3).EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true & d.Name.Contains(pmr)))
                        {
                            collection.Add(file);
                        }
                    }

                }
                else
                {

                    /*
                    if (new DirectoryInfo(path1).Exists)
                    {
                        foreach (var dir in new DirectoryInfo(path1).EnumerateDirectories("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true))
                        {
                            foreach (var file in dir.EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(f => f.Extension == ".jpg"))
                            {
                                collection.Add(file);
                            }
                        }
                    }

                    if (new DirectoryInfo(path2).Exists)
                    {
                        foreach (var dir in new DirectoryInfo(path2).EnumerateDirectories("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true))
                        {
                            foreach (var file in dir.EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(f => f.Extension == ".jpg"))
                            {
                                collection.Add(file);
                            }
                        }
                    }
                    */

                    if ((new DirectoryInfo(path3).Exists) & (collection.Count <= 0))
                    {
                        foreach (var file in new DirectoryInfo(path3).EnumerateFiles("*", SearchOption.TopDirectoryOnly).Where(d => d.Name.Contains(pinNumber.ToString()) == true))
                        {
                            collection.Add(file);
                        }
                    }

                }

            }
            finally
            {
                collection.CompleteAdding();
            }
        }

        private string ConvertRoHSPin(string pin)
        {
            StringBuilder sb = new StringBuilder(pin.Substring(0, 16));
            if (sb.ToString().IndexOf("/") == pin.Length - 4)
            {
                sb.Replace("/", "_", sb.Length - 4, 1);
            }
            return sb.ToString();
        }

        private string RestoreRoHSPin(string pin)
        {
            StringBuilder sb = new StringBuilder(pin.Substring(0, 16));
            if (sb.ToString().IndexOf("_") != sb.Length - 4)
            {
                return sb.ToString();
            }
            sb.Replace("_", "/", sb.Length - 4, 1);
            return sb.ToString();
        }

        // TO DO
        // Get Legacy Mappings for bitfields       
        async Task<List<LegacyMap>> IRouteCardRepository.GetLegacyMapping(int prs_idx, int prs_step_ix)
        {
            string legacySQL = @"SELECT * FROM Production_LM_Legacy_Mapping
                                WHERE LM_PRS_Parent_Step_IX = @LM_PRS_Parent_Step_IX and LM_PRS_Step_IX = @LM_PRS_Step_IX
                                ORDER BY LM_Legacy_Bitfield";
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@LM_PRS_Parent_Step_IX", prs_idx, DbType.Int32);
            p.Add("@LM_PRS_Step_IX", prs_step_ix, DbType.Int32);
            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                // Return legacy mapping
                var result = await conn.QueryAsync<LegacyMap>(legacySQL, p, commandType: CommandType.Text);
                return result.ToList();
            }
        }


        // TO DO
        // Creates a stub record in iOracle to support basic legacy Inspection Scan Counts
        async Task<Boolean> IRouteCardRepository.CreateLegacyStub(ScanHistory history)
        {
            string serialSQL = @"";
            // Create PARAMS
            var p = new DynamicParameters();
            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                // open connection
                conn.Open();
                // create the transaction
                using (var transaction = conn.BeginTransaction())
                {

                    // Check to see if the Assy Serial exists for this pin in Oracle
                    var serial = await conn.ExecuteScalarAsync<int>(serialSQL, p, transaction, commandType: CommandType.Text);
                    if (serial > 0)
                    {
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }


        // TO DO
        // Creates a stub record in iOracle to support basic legacy Inspection Scan Counts
        async Task<Boolean> IRouteCardRepository.CreateOracleStub(ScanHistory history)
        {

            int lib_idx = -1;
            int job_idx = -1;
            int assy_idx = -1;
            string pin = String.Empty;

            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@PIN", pin, DbType.String);
            p.Add("@BH_INDEX", history.PSH_BH_IX, DbType.Int32);
            p.Add("@PSN_INDEX", history.PSH_PSN_IX, DbType.Int32);
            p.Add("@STATUS", history.PSH_Result, DbType.Boolean);


            // This checks if a library entity exists in Oracle
            string existsLibrarySQL = @"SELECT TOP 1 libEntity_IDX FROM qdr_libraryentity WHERE libEntity_Pin = @PIN";

            // This checks if a job entity exists in Oracle
            string existsJobSQL = @"SELECT TOP 1 JobEntity_IDX FROM qdr_jobentity WHERE JobEntity_BH_IDX = @BH_INDEX";


            // This checks if an assy entity exists in Oracle
            string existsAssySQL = @"SELECT TOP 1 AssyEntity_IDX FROM qdr_assyentity WHERE AssyEntity_Serial_IDX = @PSN_INDEX";

            // This gets the serial record from PinParts
            string serialSQL = @"SELECT TOP 1 PSN_PIN FROM Serial_PSN_Product_Serial_Numbers WHERE PSN_IX = @PSN_INDEX";

            // This returns header information from PinParts should we need to create JE
            string jobInfoSQL = @"Select TOP 1 
                                    BH_PIN, JH_Description, GPIN_Description, JH_Client_Code,
                                    JH_IX, JH_Job_Number, BH_IX, BH_Number, BH_Qty, COALESCE(BH_Comment, '') as BH_Comment, 
                                    BH_Panelised, PH_PMR_Level, PSN_STA_IX 
                                    From Stock_BH_Batch_Headers as b
                                    Inner Join Project_JH_Job_Headers j On b.BH_JH_IX = j.JH_IX
                                    Inner Join PH_Parts_List_Headers p On b.BH_PIN = p.PH_PIN
                                    Inner Join GPIN_Generic_PINS g On b.BH_PIN = g.GPIN
                                    Left Outer join Serial_PSN_Product_Serial_Numbers s on s.PSN_BH_IX = BH_IX
                                    Where b.BH_IX = @BH_INDEX";

            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                // open connection
                conn.Open();
                // create the transaction
                using (var transaction = conn.BeginTransaction())
                {

                    // Check to see if the Assy Serial exists for this pin in Oracle
                    assy_idx = await conn.ExecuteScalarAsync<int>(existsAssySQL, p, transaction, commandType: CommandType.Text);

                    // If we have this serial already in iOracle then no need to do anything!
                    if (assy_idx <= 0)
                    {

                        // We don't have a serial in iOracle so grab from PinParts
                        var serial = await conn.ExecuteScalarAsync<PSNSerial>(serialSQL, p, transaction, commandType: CommandType.Text);

                        // Check to see if the library header exists for this pin in Oracle
                        var libExists = await conn.ExecuteScalarAsync<int>(existsLibrarySQL, p, transaction, commandType: CommandType.Text);
                        // If does not exist in Oracle, attempt to return header data from PinParts
                        if (libExists <= 0)
                        {
                            // Execute command to return header data
                            var result = await conn.QueryAsync<OracleStubHeader>(jobInfoSQL, p, transaction, commandType: CommandType.Text);
                            // We have pulled a header record with info to create stub ..
                            if (result.Count() > 0)
                            {
                                p.Add("@PIN", result.First().BH_Pin);
                                p.Add("@JDESC", result.First().JH_Description);
                                p.Add("@GDESC", result.First().GPIN_Description);
                                p.Add("@JCC", result.First().JH_Client_Code);

                                // Now create the LE in Oracle
                                string insertLibrarySQL = @"INSERT INTO

                                                        qdr_libraryentity(LibEntity_Pin, LibEntity_JH_Description, LibEntity_GPIN_Desc, LibEntity_Client_Code) 

                                                        VALUES(@PIN, @JDESC, @GDESC, @JCC)                                                
                                                        
                                                        SELECT CAST(SCOPE_IDENTITY() as int)";

                                lib_idx = await conn.ExecuteAsync(insertLibrarySQL, p, transaction);
                            }
                            else
                            // No result from PinParts
                            {
                                transaction.Rollback();
                                return false;
                            }

                            // If we have a result then LE created
                            if (lib_idx > 0)
                            {
                                // Check to see if Job Entity Exists
                                var jobExists = await conn.ExecuteScalarAsync<int>(existsJobSQL, p, transaction);
                                // Test to see if job entity exists
                                if (jobExists <= 0)
                                {
                                    // request information from PinParts to populate
                                    // if not found to be safe just rollback and exit
                                    if (result.Count() > 0)
                                    {

                                        p.Add("@JH_IDX", result.First().JH_IX);
                                        p.Add("@JH_Job_Number", result.First().JH_Job_Number);
                                        p.Add("@JH_Job_Number", result.First().JH_Job_Number);
                                        p.Add("@BH_IDX", result.First().BH_IX);
                                        p.Add("@Batch_No", result.First().BH_Number);
                                        p.Add("@Qty_Req", result.First().BH_Qty);
                                        p.Add("@Comments", result.First().BH_Comment);
                                        p.Add("@Panelised", result.First().BH_Panelised);
                                        p.Add("@PanelPMR_Levelised", result.First().PH_PMR_Level);
                                        p.Add("@Eng_Comments", String.Empty);
                                        p.Add("@Line_IDX", result.First().PSN_STA_IX);
                                        p.Add("@LibEntity_IDX", lib_idx);


                                        // Now create the JE in Oracle
                                        string insertJobSQL = @"INSERT INTO

                                                            qdr_jobentity(JobEntity_Date, JobEntity_JH_IDX, JobEntity_JH_Job_Number, JobEntity_BH_IDX,
                                                            JobEntity_Batch_No, JobEntity_Qty_Req, JobEntity_Comments, JobEntity_Panelised, JobEntity_PMR_Level,
                                                            JobEntity_Eng_Comments, JobEntity_Line_IDX, QDR_LibraryEntity_LibEntity_IDX) 

                                                            VALUES(CURRENT_TIMESTAMP, @JH_IDX, @JH_Job_Number, @BH_IDX,
                                                            @Batch_No, @Qty_Req, @Comments, @Panelised, @PMR_Level,
                                                            @Eng_Comments, @Line_IDX, @LibEntity_IDX)
                                                            
                                                            SELECT CAST(SCOPE_IDENTITY() as int)";

                                        job_idx = await conn.ExecuteAsync(insertJobSQL, p, transaction);
                                        // If created then commit transaction
                                        if (job_idx > 0)
                                        {
                                            // Commit transaction as both JE & LE Created
                                            // Ensure this is added following FINAL successful INSERT ..
                                            {
                                                transaction.Commit();
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                            return false;
                                        }

                                    }
                                    else
                                    // No PinParts record
                                    {
                                        transaction.Rollback();
                                        return false;
                                    }
                                }
                                else
                                // But JE exists so abort
                                {
                                    transaction.Rollback();
                                    return false;
                                }

                            }
                            else
                            // No LE Created
                            {
                                transaction.Rollback();
                                return false;
                            }


                        }
                        else
                        // LE Exists
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                    // Serial Exists, no need to do anything
                    else
                    {
                        transaction.Rollback();
                        return false;
                    }

                }
            }
        }


        // Translates client serial to Norcott serial number
        async Task<String> IRouteCardRepository.TranslateSerial(PSNSerial serial)
        {
            if (String.IsNullOrWhiteSpace(serial.PSN_Serial)) {
                return String.Empty;
            }
            // Allowable set of states
            string sqlText = @"Select TOP 1 PSN_Serial 
                               From Serial_PSN_Product_Serial_Numbers 
                               Inner Join Stock_BH_Batch_Headers On BH_IX = PSN_BH_IX 
                               Where REPLACE(REPLACE(PSN_Client_Supplied_SN, '\\r', ''), '\\n', '') = @SERIAL And PSN_PSS_IX in (2, 4, 7, 8, 9)";

            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                var p = new DynamicParameters();
                var serCorrected = serial.PSN_Serial.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                p.Add("@SERIAL", serCorrected, DbType.String);
                // Translate client to NTL Serial
                string translated = await conn.ExecuteScalarAsync<string>(sqlText, p, commandType: CommandType.Text);
                if (!String.IsNullOrEmpty(translated))
                {
                    return translated;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        // Get Top Level Serial if Bonded Assy
        async Task<String> IRouteCardRepository.GetTopLevelSerial(PSNSerial serial) 
        {
            string sqlText = @"
                    SELECT  
                        CASE WHEN  
                        (SELECT Count(*)   
                        FROM Serial_PSN_Product_Serial_Numbers
                        Where PSN_Serial = @SERIAL AND PSN_BNA_IX > 0) > 0 THEN 
                        (Select PSN_Serial 
                                FROM dbo.Serial_BIN_Bond_Serial_Number_Data b 
                                INNER JOIN dbo.Serial_PSN_Product_Serial_Numbers s ON b.BIN_PSN_IX = s.PSN_IX
                                Inner Join dbo.Serial_PSS_Serial_Number_Status st on st.PSS_IX = s.PSN_PSS_IX
                                Inner Join dbo.Stock_BH_Batch_Headers bh on bh.BH_IX = s.PSN_BH_IX
                                Inner Join GPIN_Generic_PINS g On bh.BH_PIN = g.GPIN
                                Inner Join dbo.Serial_TSN_Test_Status t on t.TSN_IX = s.PSN_TSN_IX
                                WHERE s.PSN_BNA_IX =                                 
                                (SELECT PSN_BNA_IX   
                                FROM Serial_PSN_Product_Serial_Numbers
                                WHERE PSN_Serial = @SERIAL)
                                AND b.BIN_Active = 1 and b.BIN_TLB = 1)
                            ELSE 
                            (SELECT PSN_Serial    
                                FROM Serial_PSN_Product_Serial_Numbers
                                WHERE PSN_Serial = @SERIAL) 
                        END as PSN_IX";
            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                return await conn.ExecuteScalarAsync<string>(sqlText,  new { SERIAL = serial.PSN_Serial }, commandType: CommandType.Text);
            }
        }

        // Get Serial QOB
        async Task<Int32> IRouteCardRepository.GetSerialQOB(Int32 serialIndex)
        {
            string sqlText = @"SELECT PSN_QOB    
                               FROM Serial_PSN_Product_Serial_Numbers
                               WHERE PSN_IX = @SERIAL";
            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                var result =  await conn.ExecuteScalarAsync<Int32>(sqlText,  new { SERIAL = serialIndex }, commandType: CommandType.Text);
                return result; 
            }

        }

        // Get Serial QOB and Panel Scan Index
        async Task<PanelScanMultiplier> IRouteCardRepository.GetPanelScanMultiplier(int serialIndex)
        {
            string sqlText = @"SELECT TOP 1 PSN_IX, PSN_QOB, PSN_PIN_IX,
                                    COALESCE(PIN_Panel_Number, -1) as PIN_Panel_Number,  
                                    COALESCE(PIN_Validated, 0) as PIN_Validated      
                                    FROM Serial_PSN_Product_Serial_Numbers 
                                    LEFT OUTER JOIN Serial_PIN_Panel_Index_Numbering on PIN_IX = PSN_PIN_IX
                                WHERE PSN_IX = @SERIAL";
            // Create SQL Connection
            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<PanelScanMultiplier>(sqlText, new { SERIAL = serialIndex }, commandType: CommandType.Text);
                return result.FirstOrDefault() ;
            }

        }

        LoginReturn IRouteCardRepository.SecureLogin(LoginCredentialsClass userCredentials)
        {
            // var principalContext = new PrincipalContext(ContextType.Domain, "NORSERVER01");
            // "192.168.33.4"
            var principalContext = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
            bool loginOK = principalContext.ValidateCredentials(userCredentials.UserName, userCredentials.Password);
            LoginReturn userAuth = GetUserInitials(userCredentials.UserName);
            if (!loginOK | (userAuth == null || userAuth.UserID < 0))
            {
                return (new LoginReturn()
                {
                    UserID = -1,
                    UserInitials = string.Empty,
                    UserRights = -1
                });
            }
            else
            {
                return (new LoginReturn()
                {
                    UserID = userAuth.UserID,
                    UserInitials = userAuth.UserInitials,
                    UserRights = userAuth.UserRights,
                    DeviceIndex = userAuth.DeviceIndex,
                    DeviceToken = userAuth.DeviceToken,
                    DeviceName = userAuth.DeviceName,
                    DeviceUUID = userAuth.DeviceUUID,
                    LastLogin = userAuth.LastLogin
                });
            }
        }
        async Task<LoginReturn> IRouteCardRepository.AddDeviceDetails(LoginCredentialsClass userCredentials)
        {
            Int32 affectedRows = -1;
            // Create PARAMS
            var p = new DynamicParameters();
            p.Add("@UserID", userCredentials.UserID, DbType.Int32);
            p.Add("@Password", userCredentials.Password, DbType.String);
            p.Add("@DeviceName", userCredentials.DeviceName, DbType.String);
            p.Add("@DeviceToken", userCredentials.DeviceToken, DbType.String);
            p.Add("@DeviceUUID", userCredentials.DeviceUUID, DbType.String);

            string existsSQL = @"Select Top 1 PDR_UN_IX From Production_PDR_Device_Reg Where PDR_UN_IX = @UserID";

            string sqlUpdate = @"UPDATE Production_PDR_Device_Reg
	                             SET PDR_Device_Name = @DeviceName, PDR_Device_Token = @DeviceToken, PDR_Device_UUID = @DeviceUUID, PDR_Last_Login = CURRENT_TIMESTAMP 
	                             WHERE PDR_UN_IX = @UserID";

            string sqlInsert = @"INSERT INTO 
	                             Production_PDR_Device_Reg(PDR_UN_IX, PDR_Device_Token, PDR_Device_Name, PDR_Device_UUID, PDR_Last_Login)	
	                             VALUES(@UserID, @DeviceToken, @DeviceName, @DeviceUUID, CURRENT_TIMESTAMP);";

            using (IDbConnection conn = Connection)
            {
                var paramExists = new DynamicParameters();
                paramExists.Add("@UserID", userCredentials.UserID, DbType.Int32);
                // Check to see if the device already exists
                var found = await conn.ExecuteScalarAsync<int>(existsSQL, paramExists);
                // if not found then we need to add
                if (found <= 0)
                {
                    affectedRows = await conn.ExecuteAsync(sqlInsert, p, commandType: CommandType.Text);

                }
                else
                {
                    affectedRows = await conn.ExecuteAsync(sqlUpdate, p, commandType: CommandType.Text);
                }
                // Check if worked ..
                // If so, return user details :)
                if (affectedRows > 0)
                {
                    return this.GetUserByID(userCredentials.UserID);
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new LoginReturn { UserID = -2, UserInitials = string.Empty, UserRights = -1 });

        }

        private LoginReturn GetUserInitials(string username)
        {
            using (IDbConnection conn = Connection)
            {
                var p = new DynamicParameters();
                string sql = @"Select TOP 1 UN_Initials as UserInitials, UN_IX as UserID,
                             PDR_IX as DeviceIndex, UN_ERC_MODE as UserRights,
                             Coalesce(PDR_Device_Token, '') as DeviceToken, 
                             Coalesce(PDR_Device_Name, '') as DeviceName, 
                             Coalesce(PDR_Device_UUID, '') as DeviceUUID, 
                             CASE WHEN PDR_Last_Login is null THEN CURRENT_TIMESTAMP ELSE PDR_Last_Login END as LastLogin
                             From dbo.UN_User_Names u
                             Left Outer Join Production_PDR_Device_Reg d on d.PDR_UN_IX = u.UN_IX
                             Where UPPER(UN_User_Name) = @NAME And UN_Display = 1";
                p.Add("@NAME", username.ToUpper().Trim(), DbType.String);
                var result = conn.Query<LoginReturn>(sql, p, commandType: CommandType.Text);
                if (!result.Any())
                {
                    LoginReturn loginReturn = new LoginReturn() { UserID = -2, UserInitials = string.Empty, UserRights = -1 };
                    return loginReturn;
                }
                else
                    return result.First();
            }
        }

        // Used once logged in to get record back for device token querying
        private LoginReturn GetUserByID(Int32? userID)
        {
            using (IDbConnection conn = Connection)
            {
                var p = new DynamicParameters();
                string sql = @"Select TOP 1 UN_Initials as UserInitials, UN_IX as UserID,
                             PDR_IX as DeviceIndex, UN_ERC_MODE as UserRights,
                             PDR_Device_Token as DeviceToken, 
                             PDR_Device_Name as DeviceName, 
                             PDR_Device_UUID as DeviceUUID, 
                             CASE WHEN PDR_Last_Login is null THEN CURRENT_TIMESTAMP ELSE PDR_Last_Login END as LastLogin
                             From dbo.UN_User_Names u
                             Left Outer Join Production_PDR_Device_Reg d on d.PDR_UN_IX = u.UN_IX
                             Where UN_IX = @User_ID And UN_Display = 1";
                p.Add("@User_ID", userID, DbType.Int32);
                var result = conn.Query<LoginReturn>(sql, p, commandType: CommandType.Text);
                if (!result.Any())
                {
                    LoginReturn loginReturn = new LoginReturn() { UserID = -2, UserInitials = string.Empty, UserRights = -1 };
                    return loginReturn;
                }
                else
                    return result.First();
            }
        }


        async Task<List<UserRoles>> IRouteCardRepository.UserRoles(Boolean elevated)
        {
            var p = new DynamicParameters();
            string sql = @" SELECT r.UR_IX as UserRoleIndex, 
                            u.UN_Initials as UserInitials, u.UN_User_Name as UserName, u.UN_Full_Title as UserTitle, u.UN_IX as UserID,
                            PDR_IX as DeviceIndex, d.PDR_Device_Token as DeviceToken, d.PDR_Device_Name as DeviceName, d.PDR_Device_UUID as DeviceUUID, 
                            t.TTS_IX as TTSIndex, t.TTS_Team as TTSTeam, u.UN_ERC_MODE as UserRights     
                            FROM Production_TTS_Team_To_Sign t
                            Inner Join Production_UR_User_Roles r on t.TTS_IX = r.UR_TTS_IX
                            Inner Join UN_User_Names u on u.UN_IX = r.UR_UN_IX 
                            Left Outer Join Production_PDR_Device_Reg d on d.PDR_UN_IX = r.UR_UN_IX 
                            Where (CASE WHEN @ELEVATED = 1 and u.UN_ERC_MODE = 1 THEN 1
                                    ELSE
                                    CASE WHEN @ELEVATED = 0 THEN 1 
                                        ELSE 0
                                    END
                                    END) = 1
                            order by u.UN_Initials";
            p.Add("@ELEVATED", elevated, DbType.Boolean);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<UserRoles>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        async Task<List<RequestNotification>> IRouteCardRepository.RequestNotifications(string status)
        {
            var p = new DynamicParameters();
            string sql = @"SELECT * FROM Production_RN_Requests 
                            Where (CASE WHEN @RN_Status = 'All' THEN 1 ELSE 
                                CASE WHEN RN_Status = @RN_Status THEN 1 ELSE 0 
                                END
                            END) = 1";
            p.Add("@RN_Status", status, DbType.String);
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<RequestNotification>(sql, p, commandType: CommandType.Text);

                return result.ToList();
            }
        }

        // Add Notification
        async Task<POSTStatus> IRouteCardRepository.AddRequestNotification(RequestNotification requestNotification)
        {
            Int32 affectedRows = -1;
            if (requestNotification.RN_RCH_IX > 0)
            {
                // Create PARAMS
                var p = new DynamicParameters();
                p.Add("@RN_RCH_IX", requestNotification.RN_RCH_IX, DbType.Int32);
                p.Add("@RN_PRC_IX", requestNotification.RN_PRC_IX, DbType.Int32);
                p.Add("@RN_RCI_IX", requestNotification.RN_RCI_IX, DbType.Int32);
                p.Add("@RN_RCI_TTS_IX", requestNotification.RN_RCI_TTS_IX, DbType.Int32);
                p.Add("@RN_UN_IX", requestNotification.RN_UN_IX, DbType.Int32);
                p.Add("@RN_PDR_IX", requestNotification.RN_PDR_IX, DbType.Int32);
                p.Add("@RN_Tracking_ID", requestNotification.RN_Tracking_ID, DbType.String);
                p.Add("@RN_Status", requestNotification.RN_Status, DbType.String);
                p.Add("@RN_User_Name", requestNotification.RN_User_Name, DbType.String);

                string sqlInsert = @"INSERT INTO 
                                    Production_RN_Requests(RN_RCH_IX, RN_PRC_IX, RN_RCI_IX, RN_RCI_TTS_IX, RN_UN_IX, RN_PDR_IX,RN_Tracking_ID, 
                                                            RN_Date_Added, RN_Status, RN_User_Name)	
                                                    VALUES(@RN_RCH_IX, @RN_PRC_IX, @RN_RCI_IX, @RN_RCI_TTS_IX, @RN_UN_IX, @RN_PDR_IX, @RN_Tracking_ID, 
                                                            CURRENT_TIMESTAMP, @RN_Status, @RN_User_Name);";

                using (IDbConnection conn = Connection)
                {
                    affectedRows = await conn.ExecuteAsync(sqlInsert, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        // Return POST Status
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Added: {0} {1}", affectedRows, "Row") });

                    }
                }
            }

            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Request Notification Entry" });

        }

        // Update Status of Notification
        async Task<POSTStatus> IRouteCardRepository.UpdateRequestNotification(RequestNotification requestNotification)
        {
            Int32 affectedRows = -1;
            if (requestNotification.RN_RCH_IX > 0)
            {
                // Create PARAMS
                var p = new DynamicParameters();
                p.Add("@RN_RCH_IX", requestNotification.RN_RCH_IX, DbType.Int32);
                p.Add("@RN_Status", requestNotification.RN_Status, DbType.String);
                p.Add("@RN_Tracking_ID", requestNotification.RN_Tracking_ID, DbType.String);
                p.Add("@RN_UN_IX", requestNotification.RN_UN_IX, DbType.Int32);
                string sqlUpdate = @"Update Production_RN_Requests Set RN_Status = @RN_Status 
                                     Where RN_RCH_IX = @RN_RCH_IX 
                                     And RN_Tracking_ID = @RN_Tracking_ID";

                // And RN_UN_IX = @RN_UN_IX;

                using (IDbConnection conn = Connection)
                {
                    affectedRows = await conn.ExecuteAsync(sqlUpdate, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        // Return POST Status
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated Request Notification Entry: {0} {1}", affectedRows, "Row") });

                    }
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Request Notification Entry" });

        }

        // Update RCI Instruction 
        async Task<POSTStatus> IRouteCardRepository.UpdateInstruction(RCI_Instruction instruction)
        {
            Int32 affectedRows = -1;
            if (instruction != null)
            {
                // Create PARAMS
                var p = new DynamicParameters();
                p.Add("@RCI_RCH_IX", instruction.RCI_RCH_IX, DbType.Int32);
                p.Add("@RCI_PRC_IX", instruction.RCI_PRC_IX, DbType.Int32);
                p.Add("@RCI_Instructions", instruction.RCI_Instructions, DbType.String);
                string sqlUpdate = @"Update Production_RCI_Route_Card_Instruction Set RCI_Instruction = @RCI_Instructions
                                     Where RCI_RCH_IX = @RCI_RCH_IX 
                                     And RCI_PRC_IX = @RCI_PRC_IX";

                using (IDbConnection conn = Connection)
                {
                    affectedRows = await conn.ExecuteAsync(sqlUpdate, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        // Return POST Status
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated Works Instruction Entry: {0} {1}", affectedRows, "Row") });

                    }
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Works Instruction Entry" });

        }

        bool CheckBitfield(Int32 bitfield, Int32 legacyBitfield) 
        {
                return (bitfield & legacyBitfield) == legacyBitfield;
        }


        async Task<Int32> IRouteCardRepository.GetQtyBatchQAAlert(Int32 _rchIndex, Int32 _bhIndex)
        {
            string sql = @"SELECT COUNT(DISTINCT a.QA_IX) as QtyAlerts    
                           FROM Production_QPL_QA_Alert_Process_Link l
                           INNER JOIN Production_QA_Quality_Alerts a ON a.QA_IX = l.QPL_QA_IX                      
                           WHERE l.QPL_RCH_IX = @rchIndex AND l.QPL_BH_IX = @bhIndex";

            using (IDbConnection conn = Connection)
            {
                var result = await conn.ExecuteScalarAsync<Int32>(sql, new { rchIndex = _rchIndex, bhIndex = _bhIndex }, commandType: CommandType.Text);
                return result;
            }
        }

        async Task<List<QAAlert>> IRouteCardRepository.BatchLevelQAAlert(Int32 _rchIndex, Int32 _bhIndex)
        {
            string sql = @"SELECT QA_IX, QA_QAS_IX, QA_Raised_By, QA_Raised_Date, QA_Alert_Detail, QAA_By, QAA_Date, PRS.PRS_Step_Text, PRS.QPL_RCH_IX, BH.BH_Number,
                                  QA_Closed_By, QA_Closed_Date, QA_Comments
                            FROM Production_QA_Quality_Alerts QA
                            INNER JOIN Production_QPL_QA_Alert_Process_Link QPL ON QA_IX = QPL_QA_IX
                            INNER JOIN Production_RCH_Route_Card_Header RCH ON QPL.QPL_RCH_IX = RCH.RCH_IX
                            INNER JOIN Stock_BH_Batch_Headers BH ON QPL_BH_IX = BH.BH_IX
                            INNER JOIN(
                            SELECT * FROM(
                            SELECT QPL_QA_IX, QPL_RCH_IX, 
                            SUBSTRING(STUFF((SELECT ', ' + CONVERT(VARCHAR,x.PRS_Step_Text) 
                            FROM(SELECT TOP 1000 QPL_QA_IX, QPL_RCH_IX, PRS_Step_Text
                            FROM Production_QPL_QA_Alert_Process_Link QPL1
                            INNER JOIN Production_PRC_Production_Route_Card ON QPL_PRC_IX = PRC_IX
                            INNER JOIN Production_PRS_Process_Steps ON PRC_PRS_IX = PRS_Step_IX
                            WHERE QPL1.QPL_QA_IX = QPL2.QPL_QA_IX) X FOR XML PATH ('')), 1, 1, ''),2,1000) PRS_Step_Text 
                            FROM Production_QPL_QA_Alert_Process_Link QPL2
                            INNER JOIN Production_PRC_Production_Route_Card ON QPL_PRC_IX = PRC_IX
                            INNER JOIN Production_PRS_Process_Steps ON PRC_PRS_IX = PRS_Step_IX
                            GROUP BY QPL_QA_IX, QPL_RCH_IX) X) PRS ON QPL.QPL_QA_IX = PRS.QPL_QA_IX AND QPL.QPL_RCH_IX = PRS.QPL_RCH_IX
                            LEFT JOIN(
                            SELECT QAA_QA_IX, QAA_By, QAA_Date 
                            FROM Production_QAA_Quality_Alerts_Acknowledged
                            WHERE QAA_QAT_IX = 2 AND QAA_RCH_IX = @rchIndex) QAA ON QA_IX = QAA_QA_IX
                            WHERE RCH_IX = @rchIndex AND QPL_BH_IX = @bhIndex
                            GROUP BY QA_Raised_By, QA_Raised_Date, QA_Alert_Detail, QAA_By, QAA_Date, PRS_Step_Text, PRS.QPL_RCH_IX, QA_IX, QA_QAS_IX, BH_Number,
                                     QA_Closed_By, QA_Closed_Date, QA_Comments";

            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<QAAlert>(sql, new { rchIndex = _rchIndex, bhIndex = _bhIndex } , commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<POSTStatus> IRouteCardRepository.InsertQAAlert(QAAlertAck alertAck)
        {
            string sqlInsert = @"INSERT INTO 
                                 Production_QAA_Quality_Alerts_Acknowledged(QAA_QA_IX, QAA_QPL_IX, QAA_RCH_IX, QAA_PRC_IX, QAA_QAT_IX, 
                                 QAA_UN_IX, QAA_By, QAA_Date)
                                 VALUES(@QAA_QA_IX, @QAA_QPL_IX, @QAA_RCH_IX, @QAA_PRC_IX, @QAA_QAT_IX, 
                                 @QAA_UN_IX, @QAA_By, CURRENT_TIMESTAMP)";
            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sqlInsert, new { 
                                                                            alertAck.QAA_QA_IX,
                                                                            alertAck.QAA_QPL_IX,
                                                                            alertAck.QAA_RCH_IX,
                                                                            alertAck.QAA_PRC_IX,
                                                                            alertAck.QAA_QAT_IX,
                                                                            alertAck.QAA_UN_IX,
                                                                            alertAck.QAA_By
                                                                          }, commandType: CommandType.Text);
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Inserted Alert Ack: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Inserting Alert Ack" });
        }

        async Task<POSTStatus> IRouteCardRepository.CloseQAAlert(QAAlert alert)
        {
            string sqlUpdate =
                @"UPDATE Production_QA_Quality_Alerts
                    SET QA_Comments = @QA_Comments,
                        QA_Closed_By = @QA_Closed_By,                            
                        QA_Closed_Date = CURRENT_TIMESTAMP,  
		                QA_QAS_IX = @QA_QAS_IX  
	                WHERE QA_IX = @QA_IX";
            using (IDbConnection conn = Connection)
            {
                var affectedRows = await conn.ExecuteAsync(sqlUpdate, new
                {
                    alert.QA_Comments,
                    alert.QA_Closed_By,
                    alert.QA_IX,
                    alert.QA_QAS_IX 
                }, commandType: CommandType.Text);
                if (affectedRows > 0)
                {
                    return await Task.Run(() => new POSTStatus
                    { StatusCode = 1, StatusMessage = String.Format("Status: OK, Updated Alert: {0} {1}", affectedRows, "Row") });
                }
            }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Updating Alert" });
        }

        async Task<List<QAAlert>> IRouteCardRepository.ProcessLevelQAAlert(Int32 _rchIndex, Int32 _prcIndex, Int32 _userIndex)
        {
            string sql = @"SELECT QA_IX, QA_QAS_IX, QA_Raised_By, QA_Raised_Date, QA_Alert_Detail, QPL_RCH_IX, BH.BH_Number, QA_Pin, ack.QAA_By, ack.QAA_Date, QPL_PRC_IX, 
                            PRS_Step_Text, QPL_IX,
                            (SELECT DISTINCT s.PRS_Step_Text from Production_PRS_Process_Steps s where s.PRS_Step_IX  = s1.PRS_Parent_Step_IX) as TLP,
                            CAST((CASE WHEN ack2.QAA_QAT_IX = 2 THEN 1 ELSE 0 END) AS BIT) as EngAck

                            FROM Production_QPL_QA_Alert_Process_Link L
                            INNER JOIN Production_PRC_Production_Route_Card ON QPL_PRC_IX = PRC_IX
                            INNER JOIN Production_PRS_Process_Steps s1 ON PRC_PRS_IX = s1.PRS_Step_IX
                            INNER JOIN Stock_BH_Batch_Headers BH on BH.BH_IX  = L.QPL_BH_IX
                            INNER JOIN Production_QA_Quality_Alerts A on A.QA_IX = L.QPL_QA_IX 
                                LEFT OUTER JOIN Production_QAA_Quality_Alerts_Acknowledged ack on ack.QAA_QPL_IX = l.QPL_IX and ack.QAA_RCH_IX = l.QPL_RCH_IX and ack.QAA_PRC_IX = l.QPL_PRC_IX 
                                 and QAA_QAT_IX = 1 and QAA_UN_IX = @userIndex
                                LEFT OUTER JOIN Production_QAA_Quality_Alerts_Acknowledged ack2 on ack2.QAA_QA_IX = l.QPL_QA_IX and ack2.QAA_RCH_IX = l.QPL_RCH_IX and ack2.QAA_QAT_IX = 2
                            WHERE QPL_RCH_IX = @rchIndex AND QPL_PRC_IX = @prcIndex
                            ORDER BY QPL_Assigned_Date ASC";

            using (IDbConnection conn = Connection)
            {
                var result = await conn.QueryAsync<QAAlert>(sql, new { rchIndex = _rchIndex, prcIndex = _prcIndex, userIndex = _userIndex }, commandType: CommandType.Text);
                return result.ToList();
            }
        }

        async Task<List<BOM>> IRouteCardRepository.GetBOM(int _batch, string _placement)  
        {
            string sql = @"GetConcessionedBOM";
            using (IDbConnection conn = Connection)
            {
           
                var result = await conn.QueryAsync<BOM>(sql, new { batchNo = _batch, Placement = _placement }, commandType: CommandType.StoredProcedure);

                return result.ToList();

            }
        }

        async Task<List<BOMPin>> IRouteCardRepository.GetBOMPinData(int _batch)
        {
            string sql = @"GetConcessionedBOMPinData";
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<BOMPin>(sql, new { batchNo = _batch }, commandType: CommandType.StoredProcedure);

                return result.ToList();

            }
        }

        async Task<List<Placement>> IRouteCardRepository.GetPlacement(int _batch)
        {
            string sql = @"GetConcessionedBOM";
            List<Placement> placements = new List<Placement>();
            using (IDbConnection conn = Connection)
            {

                var result = await conn.QueryAsync<BOM>(sql, new { batchNo = _batch }, commandType: CommandType.StoredProcedure);
                var items  = result.Select(x => x.Placement).Distinct();

                foreach (var item in items)              
                {

                    placements.Add(new Placement { Position = item });
                }
            }
            return placements;
        }

    }

}