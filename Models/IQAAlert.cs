using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IQAAlert   
    {
        int QA_IX { get; set; } 
        int QA_QAS_IX { get; set; }
        string QA_Alert_Detail { get; set; }
        string QA_Raised_By { get; set; }
        DateTime? QA_Raised_Date { get; set; }
        string QAA_By { get; set; }
        DateTime? QAA_Date { get; set; }
        int QPL_RCH_IX { get; set; }
        string PRS_Step_Text { get; set; }
        int QPL_IX { get; set; }
        int BH_Number { get; set; }
        string TLP { get; set; }
        bool EngAck { get; set; }
        string QA_Closed_By { get; set; }
        DateTime? QA_Closed_Date { get; set; }
        string QA_Comments { get; set; }

    }
}