using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers
{
    public class TimeMonitor : ITimeMonitor
    {


        public TimeMonitor()
        {
        }

        public int PTM_IX { get; set; }
        public int PTM_RCH_IX { get; set; }
        public int PTM_PRC_IX { get; set; }
        public int PTM_RCI_IX { get; set; }
        public int PTM_UN_IX { get; set; }
        public DateTime? PTM_Event_Time { get; set; }
        public int? PTM_PTS_IX { get; set; }
        public int? PTM_PTR_IX { get; set; }

    }
}
