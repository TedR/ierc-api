using System;

namespace iERC_API.Controllers

{
    public interface IProdPlan
    {

        int JobIndex { get; set; }
        int JobNo { get; set; }
        int BatchNo { get; set; }
        int PQty { get; set; }
        int Qty { get; set; }
        string Description { get; set; }
        string Comment { get; set; }
        DateTime TargetStartDate { get; set; }
        DateTime TargetDeliveryDate { get; set; }
        int TotalDuration { get; set; }
        int OPIndex { get; set; }
        int WSIndex { get; set; }
        string ColourCode { get; set; }
        string Operation { get; set; }
        int NodeIndex { get; set; }
        DateTime StartTime { get; set; }
        DateTime EndTime { get; set; }
        double PercentComplete { get; set; }
        int Scans { get; set; }
        


    }

}