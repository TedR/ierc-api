using System;

namespace iERC_API.Controllers

{
    public class TraceCode : ITraceCode
    {

        public TraceCode()
        {
        }
        public Int32 TC_IX { get; set; }
        public string TC_Trace_Code { get; set; }
        public string TC_PIN { get; set; }
        public DateTime? TC_Date_Code { get; set; }
        public DateTime? TC_Shelf_Expiry_Date { get; set; }
        public string TCX_Description { get; set; }
        public string GPIN_Description { get; set; }

    }

}