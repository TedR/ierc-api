using System;

namespace iERC_API.Controllers

{
    public interface ILegacyMap
    {
        int LM_IX { get; set; }
        string LM_Legacy_Process { get; set; }
        int LM_Legacy_Bitfield { get; set; }
        int LM_Legacy_Bitfield_2 { get; set; }
        int LM_PRS_Parent_Step_IX { get; set; }
        int LM_PRS_Step_IX { get; set; }
        string LM_Parent_Step_Text { get; set; }
        string LM_PRS_Step_Text { get; set; }
        int LM_SAT_Side { get; set; }

    }

}

