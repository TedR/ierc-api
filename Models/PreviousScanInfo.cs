using System;

namespace iERC_API.Controllers

{
    public class PreviousScanInfo : IPreviousScanInfo
    {

        public PreviousScanInfo()
        {
        }

        public string StepText { get; set; }
        public bool? Scanned { get; set; }
        public string Serial { get; set; }

    }

}