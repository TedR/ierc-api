using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IBOMPin
    {

        string Type { get; set; }
        string GPIN { get; set; }
        string Alert { get; set; }

    }

}