using System;

namespace iERC_API.Controllers

{
    public class UAConfirmation : IUAConfirmation
    {

        public UAConfirmation()
        {
        }
        public int PAC_IX { get; set; }
        public int PAC_RCH_IX { get; set; }
        public int PAC_PRC_IX { get; set; }
        public int PAC_UN_IX { get; set; }
        public DateTime PAC_Time { get; set; }
        public string PAC_Comment { get; set; }
        public int PAC_Ref_Number { get; set; }

    }

}