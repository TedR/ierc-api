using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IMeasurement
        {

        Int32 PMR_IX { get; set; }
        Int32 PMR_PRC_IX { get; set; }
        Int32? PMR_PRS_IX { get; set; }
        Int32? PMR_RCI_IX { get; set; }
        Int32? PMR_BH_IX { get; set; }
        Int32? PMR_Type { get; set; }
        string PMR_Data { get; set; }


    }

}


