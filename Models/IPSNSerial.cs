using System;

namespace iERC_API.Controllers

{
    public interface IPSNSerial
    {

        int PSN_IX { get; set; }
        int PSN_CC_IX { get; set; }
        string PSN_Serial { get; set; }
        string PSN_Client_Supplied_SN { get; set; }
        string PSN_PIN { get; set; }
        int PSN_Batch_Number { get; set; }
        int PSN_TSN_IX { get; set; }
        int PSN_PSS_IX { get; set; }
        int PSN_BNA_IX { get; set; }
        int PSN_STA_IX { get; set; }
        Boolean PSN_SMT_Dbl_Sided { get; set; }
        Boolean PSN_PTH_Dbl_Sided { get; set; }
        int PSN_JH_IX { get; set; }
        int PSN_BH_IX { get; set; }
        int PSN_QOB { get; set; }
        int PSN_Bitfield { get; set; }
        int PSN_Bitfield2 { get; set; }
        Boolean PSN_First_Off { get; set; }
        string GPIN_Description { get; set; }
        string BH_Client { get; set; }
        int LINE { get; set; }
        string PSS_Text { get; set; }
        int PSN_Kit_Batch { get; set; }
        string PSN_Stock_Trace { get; set; }






    }

}