using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IProcessComment    
    {
        int PUC_IX { get; set; }
        int PUC_PCH_IX { get; set; }
        int PUC_PRC_IX { get; set; }
        int PUC_UN_IX { get; set; }   
        DateTime PUC_Edited { get; set; }
        string PUC_Comments { get; set; }
        string UN_Initials { get; set; }
        string UN_Full_Name { get; set; } 
        string TLP { get; set; }
        string SLP { get; set; }




    }

}