using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers
{
    public class LoginCredentialsClass : ILoginCredentialsClass
    {


        public LoginCredentialsClass()
        {
        }
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserRights { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceName { get; set; }
        public string DeviceUUID { get; set; }
        public DateTime? LastLogin { get; set; }
  

    }
}
