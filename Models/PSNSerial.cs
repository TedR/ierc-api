using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class PSNSerial : IPSNSerial
    {

        public PSNSerial()
        {
        }
        public int PSN_IX { get; set; }
        public int PSN_CC_IX { get; set; }
        public string PSN_Serial { get; set; }
        public string PSN_Client_Supplied_SN { get; set; }
        public int PSN_Batch_Number { get; set; }
        public string PSN_PIN { get; set; }
        public int PSN_TSN_IX { get; set; }
        public int PSN_PSS_IX { get; set; }
        public int PSN_BNA_IX { get; set; }
        public int PSN_STA_IX { get; set; }
        public Boolean PSN_SMT_Dbl_Sided { get; set; }
        public Boolean PSN_PTH_Dbl_Sided { get; set; }
        public int PSN_JH_IX { get; set; }
        public int PSN_BH_IX { get; set; }
        public int PSN_QOB { get; set; }
        public int PSN_Bitfield { get; set; }
        public int PSN_Bitfield2 { get; set; }
        public Boolean PSN_First_Off { get; set; }
        public string GPIN_Description { get; set; }
        public string BH_Client { get; set; }
        public int LINE { get; set; }
        public string PSS_Text { get; set; }
        public int PSN_Kit_Batch { get; set; }
        public string PSN_Stock_Trace { get; set; }

    }

}