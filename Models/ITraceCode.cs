using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface ITraceCode
    {

        Int32 TC_IX { get; set; }
        string TC_Trace_Code { get; set; }
        string TC_PIN { get; set; }
        DateTime? TC_Date_Code { get; set; }
        DateTime? TC_Shelf_Expiry_Date { get; set; }
        string TCX_Description { get; set; }
        string GPIN_Description { get; set; }

    }

}


