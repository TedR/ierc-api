using System;
namespace iERC_API.Controllers
{
    public interface ILoginCredentialsClass
    {
        Int32 UserID { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int UserRights { get; set; }
        string DeviceToken { get; set; }
        string DeviceName { get; set; }
        string DeviceUUID { get; set; }
        DateTime? LastLogin { get; set; }




    }
}
