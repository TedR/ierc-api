using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class BOMPin : IBOMPin
    {

        public BOMPin()
        {
        }

        public string Type { get; set; }
        public string GPIN { get; set; }
        public string Alert { get; set; }


    }

}