using System;

namespace iERC_API.Controllers

{
    public class ScanHistory : IScanHistory
    {

        public ScanHistory()
        {
        }

        public int PSH_IX { get; set; }
        public int PSH_PSN_IX { get; set; }
        public int PSH_PRC_IX { get; set; }
        public int PSH_PRS_IX { get; set; }
        public int PSH_RCI_IX { get; set; }
        public int PSH_BH_IX { get; set; }
        public int PSH_ST_IX { get; set; }
        public int PSH_Result { get; set; }
        public int PSH_UN_IX { get; set; }
        public int PSH_TTS_IX { get; set; }
        public int PSH_Quantity { get; set; }
        public string PSH_By { get; set; }
        public DateTime? PSH_DateTime { get; set; }
        public string Device_Name { get; set; }
        public int PSN_PIN_IX { get; set; }

    }

}