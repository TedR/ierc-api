using System;

namespace iERC_API.Controllers

{
    public interface ITimeState
    {
        int PTS_IX { get; set; }
        int PTS_State { get; set; }
        String PTS_Description { get; set; }
        Boolean PTS_Display { get; set; }
        int PTS_Order { get; set; }

    }

}