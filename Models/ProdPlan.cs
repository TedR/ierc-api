using System;

namespace iERC_API.Controllers

{
    public class ProdPlan : IProdPlan
    {

        public ProdPlan()
        {
        }

        public int JobIndex { get; set; }
        public int JobNo { get; set; }
        public int BatchNo { get; set; }
        public int PQty { get; set; }
        public int Qty { get; set; }
        public string Description { get; set; }
        public string Comment {get; set; }
        public DateTime TargetStartDate { get; set; }
        public DateTime TargetDeliveryDate { get; set; }
        public int TotalDuration { get; set; }
        public double PercentComplete { get; set; }
        public int OPIndex { get; set; }
        public int WSIndex { get; set; }
        public string ColourCode { get; set; }
        public string Operation { get; set; }
        public int NodeIndex { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Scans { get; set; }


    }

}