using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class PanelLayout : IPanelLayout
    {

        public PanelLayout()
        {
        }
        
        public int PCB_IX { get; set; }
        public int PCB_RCH_IX { get; set; }
        public string PCB_Category { get; set; }
        public string PIT_Item { get; set; }
        public string PCB_Group { get; set; }
        public string PCB_Item_Description { get; set; }
        public int PCB_Item_Number { get; set; }
        public double PCB_X_Value { get; set; }
        public double PCB_Y_Value { get; set; }
        public double PCB_Thickness { get; set; }
        public double PCB_Diameter { get; set; }
        public int PCB_Style { get; set; }
    
    
    }

}