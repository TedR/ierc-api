using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IConsumable    {

        int PCI_IX { get; set; }
        int PCI_RCH_IX { get; set; }
        int PCI_PRC_IX { get; set; }
        int PCI_RCI_IX { get; set; }
        String PCI_Pin { get; set; }
        String PCI_Trace { get; set; }
        DateTime? PCI_Expiry { get; set; }
        String PCI_Comments { get; set; }
        String PCI_Department { get; set; }
        int PCI_UN_IX { get; set; }
        string PCI_UN_Initials { get; set; }
        DateTime PCI_Date { get; set; }







    }

}

