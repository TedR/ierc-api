using System;

namespace iERC_API.Controllers

{
    public class Consumable : IConsumable
    {

   
        public int PCI_IX { get; set; }
        public int PCI_RCH_IX { get; set; }
        public int PCI_PRC_IX { get; set; }
        public int PCI_RCI_IX { get; set; }
        public String PCI_Pin { get; set; }
        public String PCI_Trace { get; set; }
        public DateTime? PCI_Expiry { get; set; }
        public String PCI_Comments { get; set; }
        public String PCI_Department { get; set; }
        public int PCI_UN_IX { get; set; }
        public string PCI_UN_Initials { get; set; }
        public DateTime PCI_Date { get; set; }

        public Consumable()
        {
        }

        public Consumable(IConsumable c)
        {
            this.PCI_IX = c.PCI_IX;
            this.PCI_RCH_IX = c.PCI_RCH_IX;
            this.PCI_PRC_IX = c.PCI_PRC_IX;
            this.PCI_RCI_IX  = c.PCI_RCI_IX;
            this.PCI_Pin = c.PCI_Pin;
            this.PCI_Trace = c.PCI_Trace;
            this.PCI_Expiry  = c.PCI_Expiry;
            this.PCI_Comments = c.PCI_Comments;
            this.PCI_Department = c.PCI_Department;
            this.PCI_UN_IX = c.PCI_UN_IX;
            this.PCI_Date = c.PCI_Date;
        }

    }

}