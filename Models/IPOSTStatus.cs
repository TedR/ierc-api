using System;

namespace iERC_API.Controllers

{
    public interface IPOSTStatus
    {
        int StatusCode { get; set; }
        string StatusMessage { get; set; }
    }

}