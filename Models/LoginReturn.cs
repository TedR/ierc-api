using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class LoginReturn : ILoginReturn
    {

        public LoginReturn()
        {
        }
    
        public string UserInitials { get; set; }
        public int UserID { get; set; }
        public int UserRights { get; set; }
        public int DeviceIndex { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceName { get; set; }
        public string DeviceUUID { get; set; }
        public DateTime? LastLogin { get; set; }



    }

}