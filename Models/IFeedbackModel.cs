


using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IFeedbackModel
    {

        int RCH_IX { get; set; }
        string RCH_Last_Edit_By { get; set; }
        DateTime RCH_Last_Edit_Date { get; set; }
        string RCH_Feedback { get; set; }
        
    }

}