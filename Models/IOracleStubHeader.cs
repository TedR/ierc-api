using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IOracleStubHeader
    {

        Int32 JH_IX { get; set; }
        Int32 JH_Job_Number { get; set; }
        Int32 BH_IX{ get; set; }
        Int32 BH_Number { get; set; }
        Int32 BH_Qty { get; set; }
        Boolean BH_Panelised { get; set; }
        Int32 PH_PMR_Level { get; set; }
        Int32 PSN_STA_IX { get; set; }
        String BH_Pin { get; set; }
        String JH_Description { get; set; }
        String GPIN_Description { get; set; }
        String JH_Client_Code { get; set; }
        String BH_Comment { get; set; }


    }

}


