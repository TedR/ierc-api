using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers
{
    public class TimeState : ITimeState
    {

        public TimeState()
        {
        }

        public int PTS_IX { get; set; }
        public int PTS_State { get; set; }
        public String PTS_Description { get; set; }
        public Boolean PTS_Display { get; set; }
        public int PTS_Order { get; set; }

    }
    
}
