using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IDrawingModel
    {

        int DA_IX { get; set; }
        int DA_RCH_IX { get; set; }
        string DA_Doc_Type { get; set; }
        string DA_File_Path { get; set; }
        string DA_Origin { get; set; }
        string DA_Drawing_No { get; set; }
        DateTime? DA_Date { get; set ;}
        int DA_Qty_Issued { get; set; }


    }

}