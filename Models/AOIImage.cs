using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class AOIImage : IAOIImage
    {

        public AOIImage()
        {
        }

    public string IMG_Pin { get;  set; }
    public string IMG_PMR { get; set; }
    public  string IMG_Side { get; set; }
    public string IMG_Path { get; set; }
    public string IMG_Location { get; set; }
    
    
    }

}