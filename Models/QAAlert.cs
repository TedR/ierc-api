using System;

namespace iERC_API.Controllers

{
    public class QAAlert : IQAAlert
    {

        public QAAlert()
        {
        }

        public int QA_IX { get; set; }
        public int QA_QAS_IX { get; set; }
        public string QA_Alert_Detail { get; set; }
        public string QA_Raised_By { get; set; }
        public DateTime? QA_Raised_Date { get; set; }
        public string QAA_By { get; set; }
        public DateTime? QAA_Date { get; set; }
        public int QPL_RCH_IX { get; set; }
        public string PRS_Step_Text { get; set; }
        public int QPL_IX { get; set; }
        public int BH_Number { get; set; }
        public string TLP { get; set; }
        public bool EngAck { get; set; }
        public string QA_Closed_By { get; set; }
        public DateTime? QA_Closed_Date { get; set; }
        public string QA_Comments {get; set; }
    }

}