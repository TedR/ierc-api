using System;

namespace iERC_API.Controllers

{
    public interface IGNC
    {
        int GNCIndex { get; set ;}
        string PinNumber { get; set; }
        int GNCNumber { get; set; }
        DateTime Raised { get; set; }
        string Description { get; set; }
        string PreventiveAction { get; set; }
        string Status { get; set; }
        Boolean Accepted { get; set; }

    }

}