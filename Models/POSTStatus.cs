using System;

namespace iERC_API.Controllers

{
    public class POSTStatus : IPOSTStatus
    {

        public POSTStatus()
        {
        }

        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }

    }

}