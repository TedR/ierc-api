using System;

namespace iERC_API.Controllers

{
    public interface ISignOff
    {

        int BH_Number { get; set; }
        string BH_PIN { get; set; }
        int PSO_IX { get; set; }
        string PSO_RN_Tracking_ID { get; set; }
        int PSO_PRC_IX { get; set; }
        int PSO_PCH_IX { get; set; }
        int PSO_RCI_IX { get; set; }
        // int PSO_PRS_IX { get; set; } // This field is deprecated
        int PSO_PRS_Step_IX { get; set; }
        int PSO_User { get; set; }
        string UN_Full_Name { get; set; }
        int PSO_Approved_By { get; set; }
        string Auth_Name { get; set; }
        string UN_Initials { get; set; }
        int PSO_PSN_IX { get; set; }
        string PSO_PSN_Serial { get; set; }
        int PSO_Quantity { get; set; }
        DateTime? PSO_Completed { get; set; }
        string PSO_Comments { get; set; }
        int RN_UN_IX { get; set; }
        string RN_Status { get; set; }
        string PSO_Status { get; set; }
        string RCI_Step { get; set; }
        string RCI_Instruction  { get; set; }
        string BH_First_Serial { get; set; }

    }

}