using System;

namespace iERC_API.Controllers

{
    public class Shortage : IShortage
    {

        public Shortage()
        {
        }

        public int CSD_IX { get; set; }
        public int CSD_RCH_IX { get; set; }
        public string CSD_Pin { get; set; }
        public string CSD_Detail { get; set; }
        public int CSD_Qty { get; set; }
        public string CSD_Location { get; set; }
        public string CSD_Confirmed_Fitted { get; set; }

        // Additional Fields
        public int CSD_PRS_IX { get; set; }
        public int CSD_Parent_PRS_IX { get; set; }
        public int CSD_PRC_IX { get; set; }
        public int CSD_UN_IX_Reported { get; set; }
        public string CSD_UN_Initials_Reported { get; set; }
        public int CSD_UN_IX_Fitted_By { get; set; }
        public DateTime? CSD_Fitted { get; set; }
        public string CSD_Process { get; set; }

    }

}