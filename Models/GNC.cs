using System;

namespace iERC_API.Controllers

{
    public class GNC : IGNC
    {

        public GNC()
        {
        }
        
        public int GNCIndex { get; set; }
        public string PinNumber { get; set; }
        public int GNCNumber { get; set; }
        public DateTime Raised { get; set; }
        public string Description { get; set; }
        public string PreventiveAction { get; set; }
        public string Status { get; set; }
        public Boolean Accepted { get; set; }

    }

}