using System;

namespace iERC_API.Controllers

{
    public interface IYieldCube
    {

        string Pin { get; set; }
        int Batch { get; set; }
        int Job { get; set; }
        int Qty { get; set; }
        int Made { get; set; }
        DateTime Built { get; set; }
        int Affected { get; set; }
        int Defects { get; set; }
        Decimal Cost { get; set; }
        Decimal Rework { get; set; }
        Double Yield { get; set; }
        int PPM { get; set; }
        int Ranking { get; set; }

    }


}