using System;

namespace iERC_API.Controllers

{
    public class LiveBatches : ILiveBatches
    {
        public LiveBatches()
        {
        }

        public string PinNumber { get; set; }
        public int GPinIndex { get; set; }
        public int JobNumber { get; set; }
        public int JobIndex { get; set; }
        public int BatchNumber { get; set; }
        public int BatchIndex { get; set; }
        public int RouteCardHdrIndex { get; set; }
        public string ClientCode { get; set; }
        public string ClientPart { get; set; }
        public string FirstSerial { get; set; }
        public DateTime BuildDate { get; set; }
        public DateTime QMPCreated { get; set; }
        public string QMPBy { get; set; }
        public string Engineer { get; set; }
        public int Quantity { get; set; }
        public string BatchComment { get; set; }
        public string JobDescription { get; set; }
        public string GDescription { get; set; }
        public string ClientOrder { get; set; }
        public string PMR { get; set; }
        public Boolean Alert { get; set; }
        public string AlertComment { get; set; }
        public Boolean AOI { get; set; }
        public int GNCS { get; set; }
        public int ActiveTLP { get; set; }
        public int ActiveSLP { get; set; }
        public Boolean AlertSLP { get; set; }
        public int Class { get; set; }
        public int Step { get; set; }
        public string ERCInstructions { get; set; }
        public Boolean StockAllocated { get; set; }
        public string Finish { get; set; }
        public Boolean Visible { get; set; }
        public int QAAlerts { get; set; }
    }

}

