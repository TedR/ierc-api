using System;

namespace iERC_API.Controllers

{
    public interface IPreviousBatchInfo
    {
        string StepText { get; set; }
        int Quantity { get; set; }
    }

}