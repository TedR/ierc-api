using System;

namespace iERC_API.Controllers

{
    public class ProcessModel : IProcessModel
    {
        public ProcessModel()       
        {
        }
        public string PinNumber { get; set; }
        public int JobNumber { get; set; }
        public int JobIndex { get; set; }
        public int BatchNumber { get; set; }
        public int BatchIndex { get; set; }
        public int RouteCardHdrIndex { get; set; }
        public int ParentIndex { get; set; }
        public int ParentPRCIndex { get; set; }
        public int UniqueStepID { get; set; }
        public int ProcessID { get; set; }
        public string ProcessName { get; set; }
        public string StepText { get; set; }
        public string TTS_TEAM_1 { get; set; }
        public int TTSIndex_1 { get; set; }
        public int TS1 { get; set; }
        public string TTS_TEAM_2 { get; set; }
        public int TTSIndex_2 { get; set; }
        public int TS2 { get; set; }
        public int Authorised_1 { get; set; }
        public int Authorised_2 { get; set; }
        public int Ordering { get; set; }
        public int Position { get; set; }
        public int InstIndex { get; set; }
        public string Instruction { get; set; }
        public int ScanTypeIndex { get; set; }
        public string ScanTypeText { get; set; }
        public int ExpectedScans { get; set; }
        public int ScanCount { get; set; }
        public decimal ProgressPercent { get; set; }
        public int ActiveTLP { get; set; }
        public int ActiveSLP { get; set; }
        public int EnteredQuantity { get; set; }
        public int ProcStatus { get; set; }
        public int Signatures { get; set; }
        public string TT1SIG { get; set; }
        public string TT2SIG { get; set; }
        public int Drawings { get; set; }
        public int QAAlerts { get; set; }

    }


}