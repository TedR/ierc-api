using System;

namespace iERC_API.Controllers

{
    public class ProcessComment : IProcessComment
    {

        public ProcessComment()
        {
        }

        public int PUC_IX { get; set; }
        public int PUC_PCH_IX { get; set; }
        public int PUC_PRC_IX { get; set; }
        public int PUC_UN_IX { get; set; }
        public DateTime PUC_Edited { get; set; }
        public string PUC_Comments { get; set; }
        public string UN_Initials { get; set; }
        public string UN_Full_Name { get; set; }
        public string TLP { get; set; }
        public string SLP { get; set; }

    }

}