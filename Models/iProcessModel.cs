using System;

namespace iERC_API.Controllers

{
    public interface IProcessModel
    {
        string PinNumber { get; set; }
        int JobNumber { get; set; }
        int JobIndex { get; set; }
        int BatchNumber { get; set; }
        int BatchIndex { get; set; }
        int RouteCardHdrIndex { get; set; }
        int ParentIndex { get; set; }
        int ParentPRCIndex { get; set; }
        int UniqueStepID { get; set; }
        int ProcessID { get; set; } 
        string ProcessName { get; set; }
        string StepText { get; set; }
        string TTS_TEAM_1 { get; set; }
        int TTSIndex_1 { get; set; }
        int TS1 { get; set; }
        string TTS_TEAM_2 { get; set; }
        int TTSIndex_2 { get; set; }
        int TS2 { get; set; }
        int Authorised_1 { get; set; }
        int Authorised_2 { get; set; }
        int Ordering { get; set; }
        int Position { get; set; }
        int InstIndex { get; set; }
        string Instruction { get; set; }       
        int ScanTypeIndex { get; set; }
        string ScanTypeText { get; set; }
        int ExpectedScans { get; set; }
        int ScanCount { get; set; }
        decimal ProgressPercent { get; set; }
        int ActiveTLP { get; set; }
        int ActiveSLP { get; set; }
        int EnteredQuantity { get; set; }   
        int ProcStatus { get; set; }
        int Signatures { get; set; }
        string TT1SIG { get; set; }
        string TT2SIG { get; set; }
        int Drawings { get; set; }
        int QAAlerts { get; set; }

    }

}