using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers
{
    public class TimeReason : ITimeReason
    {


        public TimeReason()
        {
        }

        public int PTR_IX { get; set; }
        public String PTR_Reason { get; set; }
        public Boolean PTR_Display { get; set; }
        public int PTR_Order { get; set; }

    }
}
