using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IRequestNotification
    {

        int RN_IX { get; set; }
        int RN_RCH_IX { get; set; }
        int RN_PRC_IX { get; set; }
        int RN_RCI_IX { get; set; }
        int RN_RCI_TTS_IX { get; set; }
        int RN_UN_IX { get; set; }
        int RN_PDR_IX { get; set; }
        string RN_Tracking_ID { get; set; }
        DateTime? RN_Date_Added { get; set; }
        string RN_Status { get; set; }
        string RN_User_Name { get; set; }

    }

}