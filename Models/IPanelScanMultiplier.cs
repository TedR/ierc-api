namespace iERC_API.Controllers

{
    public interface IPanelScanMultiplier
    {
        int PSN_IX { get; set; }
        int PSN_QOB { get; set; }
        int PSN_PIN_IX { get; set; }
        int PIN_Panel_Number { get; set; }
        bool PIN_Validated { get; set; }
    }

}