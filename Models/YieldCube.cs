using System;

namespace iERC_API.Controllers

{
    public class YieldCube : IYieldCube
    {

        public YieldCube()
        {
        }
        public string Pin { get; set; }
        public int Batch { get; set; }
        public int Job { get; set; }
        public int Qty { get; set; }
        public int Made { get; set; }
        public DateTime Built { get; set; }
        public int Affected { get; set; }
        public int Defects { get; set; }
        public Decimal Cost { get; set; }
        public Decimal Rework { get; set; }
        public Double Yield { get; set; }
        public int PPM { get; set; }
        public int Ranking { get; set; }


    }

}