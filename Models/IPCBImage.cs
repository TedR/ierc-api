namespace iERC_API.Controllers
{
    public interface IPCBImage
    {

        string Param1 { get; set; }
        string Param2 { get; set; }
        string Param3 { get; set; }

    }
}
