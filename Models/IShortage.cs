using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IShortage
    {

        int CSD_IX { get; set; }
        int CSD_RCH_IX { get; set; }
        string CSD_Pin { get; set; }
        string CSD_Detail { get; set; }
        int CSD_Qty { get; set; }
        string CSD_Location { get; set; }
        string CSD_Confirmed_Fitted { get; set; }

        // Additional Fields

        int  CSD_PRS_IX { get; set; }
        int CSD_Parent_PRS_IX { get; set; }
        int CSD_PRC_IX { get; set; }
        int CSD_UN_IX_Reported { get; set; }
        string CSD_UN_Initials_Reported { get; set; }
        int CSD_UN_IX_Fitted_By { get; set; }
        DateTime? CSD_Fitted { get; set; }
        string CSD_Process { get; set; }







    }

}