using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class OracleStubHeader : IOracleStubHeader
    {

        public OracleStubHeader()
        {
        }


        public Int32 JH_IX { get; set; }
        public Int32 JH_Job_Number { get; set; }
        public Int32 BH_IX { get; set; }
        public Int32 BH_Number { get; set; }
        public Int32 BH_Qty { get; set; }
        public Boolean BH_Panelised { get; set; }
        public Int32 PH_PMR_Level { get; set; }
        public Int32 PSN_STA_IX { get; set; }
        public String BH_Pin { get; set; }
        public String JH_Description { get; set; }
        public String GPIN_Description { get; set; }
        public String JH_Client_Code { get; set; }
        public String BH_Comment { get; set; }


    }

}