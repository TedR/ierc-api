using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IAOIImage
    {

    string IMG_Pin { get; set; }
    string IMG_PMR { get; set; }
    string IMG_Side { get; set; }
    string IMG_Path { get; set; }
    string IMG_Location { get; set; }
 


    }

}