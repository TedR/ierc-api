using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IUserRoles
    {
 
        int UserRoleIndex { get; set; }
        string UserInitials { get; set; }
        string UserName { get; set; }
        string UserTitle { get; set; }
        int UserID { get; set; }
        int UserRights {get; set; }
        int DeviceIndex { get; set; }
        string DeviceToken { get; set; }
        string DeviceName { get; set; }
        string DeviceUUID { get; set; }
        int TTSIndex { get; set; }
        string TTSTeam { get; set; }

    }

}