using System;

namespace iERC_API.Controllers

{
    public interface IRCI_Instruction
    {
        int RCI_RCH_IX { get; set ;}
        int RCI_PRC_IX { get; set; }
        string RCI_Instructions { get; set; }
    }

}