using System;

namespace iERC_API.Controllers

{
    public interface IScanHistory
    {

        int PSH_IX { get; set; }
        int PSH_PSN_IX { get; set; }
        int PSH_PRC_IX { get; set; }
        int PSH_PRS_IX { get; set; }
        int PSH_RCI_IX { get; set; }
        int PSH_BH_IX { get; set; }
        int PSH_ST_IX { get; set; }
        int PSH_Result { get; set; }
        int PSH_UN_IX { get; set; }
        int PSH_TTS_IX { get; set; }
        int PSH_Quantity { get; set; }
        string PSH_By { get; set; }
        DateTime? PSH_DateTime { get; set; }
        string Device_Name { get; set; }
        int PSN_PIN_IX { get; set; }

    }

}