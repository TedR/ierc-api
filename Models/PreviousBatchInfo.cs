using System;

namespace iERC_API.Controllers

{
    public class PreviousBatchInfo : IPreviousBatchInfo
    {

        public PreviousBatchInfo()
        {
        }

        public string StepText { get; set; }
        public int Quantity { get; set; }

    }

}