using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class FeedbackModel : IFeedbackModel
    {

        public FeedbackModel()
        {
        }

        public  int RCH_IX { get; set; }
        public string RCH_Last_Edit_By { get; set; }
        public DateTime RCH_Last_Edit_Date { get; set; }
        public string RCH_Feedback { get; set; }


    }

}