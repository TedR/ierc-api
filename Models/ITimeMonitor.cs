using System;

namespace iERC_API.Controllers

{
    public interface ITimeMonitor
    {

        int PTM_IX { get; set; }
        int PTM_RCH_IX { get; set; }
        int PTM_PRC_IX { get; set; }
        int PTM_RCI_IX { get; set; }
        int PTM_UN_IX { get; set; }
        DateTime? PTM_Event_Time { get; set; }
        int? PTM_PTS_IX { get; set; }
        int? PTM_PTR_IX { get; set; }

    }

}