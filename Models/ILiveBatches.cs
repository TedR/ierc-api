using System;

namespace iERC_API.Controllers

{
    public interface ILiveBatches {

        string PinNumber { get; set; }
        int GPinIndex { get; set;  }
        int JobNumber { get; set; }
        int JobIndex { get; set; }
        int BatchNumber { get; set; }
        int BatchIndex { get; set; }
        int RouteCardHdrIndex { get; set; }
        string ClientCode { get; set; }
        string ClientPart { get; set; }
        string FirstSerial { get; set; }
        DateTime BuildDate { get; set; }
        DateTime QMPCreated { get; set; }
        string QMPBy { get; set; }
        string Engineer { get; set; }
        int Quantity { get; set; }
        string BatchComment {get; set; }
        string JobDescription { get; set; }
        string GDescription { get; set; }
        string ClientOrder { get; set; }
        string PMR { get; set; }
        Boolean Alert { get; set; }
        string AlertComment { get; set; }
        Boolean AOI { get; set; }
        int GNCS { get; set; }
        int ActiveTLP { get; set; }
        int ActiveSLP { get; set; }
        Boolean AlertSLP { get; set; }
        int Class { get; set; }
        int Step { get; set; }
        string ERCInstructions { get; set ;}
        Boolean StockAllocated { get; set; }
        string Finish { get; set; }
        Boolean Visible { get; set; }
        int QAAlerts { get; set; }        
    }

}

