using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers
{
    public class LegacyMap : ILegacyMap
    {


        public LegacyMap()
        {
        }

        public int LM_IX { get; set; }
        public string LM_Legacy_Process { get; set; }
        public int LM_Legacy_Bitfield { get; set; }
        public int LM_Legacy_Bitfield_2 { get; set; }
        public int LM_PRS_Parent_Step_IX { get; set; }
        public int LM_PRS_Step_IX { get; set; }
        public string LM_Parent_Step_Text { get; set; }
        public string LM_PRS_Step_Text { get; set; }
        public int LM_SAT_Side { get; set; }


    }
}
