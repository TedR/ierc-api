
using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class RequestNotification : IRequestNotification
    {

        public RequestNotification()
        {
        }

        public int RN_IX { get; set; }
        public int RN_RCH_IX { get; set; }
        public int RN_PRC_IX { get; set; }
        public int RN_RCI_IX { get; set; }
        public int RN_RCI_TTS_IX { get; set; }
        public int RN_UN_IX { get; set; }
        public int RN_PDR_IX { get; set; }
        public string RN_Tracking_ID { get; set; }
        public DateTime? RN_Date_Added { get; set; }
        public string RN_Status { get; set; }
        public string RN_User_Name { get; set; }

    }

}