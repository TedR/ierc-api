using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class UserRoles : IUserRoles
    {

        public UserRoles()
        {
        }

        public int UserRoleIndex { get; set; }
        public string UserInitials { get; set; }
        public string UserName { get; set; }
        public string UserTitle { get; set; }
        public int UserID { get; set; }
        public int UserRights { get; set; }
        public int DeviceIndex { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceName { get; set; }
        public string DeviceUUID { get; set; }
        public int TTSIndex { get; set; }
        public string TTSTeam { get; set; }


    }

}