using System;

namespace iERC_API.Controllers

{
    public class SignOff: ISignOff
    {

        public SignOff()
        {
        }


        public int BH_Number { get; set; }
        public string BH_PIN { get; set; }
        public int PSO_IX { get; set; }
        public string PSO_RN_Tracking_ID { get; set; }
        public int PSO_PRC_IX { get; set; }
        public int PSO_PCH_IX { get; set; }
        public int PSO_RCI_IX { get; set; }
        // public int PSO_PRS_IX { get; set; } // // This field is deprecated
        public int PSO_PRS_Step_IX { get; set; }
        public int PSO_User { get; set; }
        public string UN_Full_Name { get; set; }
        public int PSO_Approved_By { get; set; }
        public string Auth_Name { get; set; }
        public string UN_Initials { get; set; }
        public int PSO_PSN_IX { get; set; }
        public string PSO_PSN_Serial { get; set; }
        public int PSO_Quantity { get; set; }
        public DateTime? PSO_Completed { get; set; }
        public string PSO_Comments { get; set; }
        public string PSO_Status { get; set; }
        public int RN_UN_IX { get; set; }
        public string RN_Status { get; set; }
        public string RCI_Step { get; set; }
        public string RCI_Instruction { get; set; }
        public string BH_First_Serial { get; set; }

    }

}