using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IBOM
    {

        string Placement { get; set; }
        string RefDes { get; set; }
        string RP_PIN { get; set; }
        string RP_Original_PIN { get; set; }
        string ClientPart { get; set; }
        int cur_qty { get; set; }
        int SplitQty { get; set; }
        string GPIN_Description { get; set; }
        string PID_Package { get; set; }
        string Manufacturer { get; set; }
        string ManufacturersPartNumber { get; set; }

    }

}