using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface ILoginReturn
    {

        string UserInitials { get; set; }
        int UserID { get; set; }
        int UserRights {get; set; }  
        int DeviceIndex { get; set; }
        string DeviceToken { get; set; }
        string DeviceName { get; set; }
        string DeviceUUID { get; set; }
        DateTime? LastLogin { get; set; }


    }

}