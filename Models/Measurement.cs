using System;

namespace iERC_API.Controllers

{
    public class Measurement : IMeasurement
    {

        public Measurement()
        {
        }

        public Int32 PMR_IX { get; set; }
        public Int32 PMR_PRC_IX { get; set; }
        public Int32? PMR_PRS_IX { get; set; }
        public Int32? PMR_RCI_IX { get; set; }
        public Int32? PMR_BH_IX { get; set; }
        public Int32? PMR_Type { get; set; }
        public string PMR_Data { get; set; }

    }

}