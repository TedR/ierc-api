using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IUAConfirmation   
    {
        int PAC_IX { get; set; }
        int PAC_RCH_IX { get; set; }
        int PAC_PRC_IX { get; set; }
        int PAC_UN_IX { get; set; }   
        DateTime PAC_Time { get; set; }
        string PAC_Comment { get; set; }
        int PAC_Ref_Number { get; set; }



    }

}