using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IPlacement
    {
        string Position { get; set; }   
    }

}