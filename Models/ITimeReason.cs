using System;

namespace iERC_API.Controllers

{
    public interface ITimeReason
    {
        int PTR_IX { get; set; }
        String PTR_Reason { get; set; }
        Boolean PTR_Display { get; set; }
        int PTR_Order { get; set; }

    }

}