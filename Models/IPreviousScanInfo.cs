using System;

namespace iERC_API.Controllers

{
    public interface IPreviousScanInfo
    {
        string StepText { get; set; }
        bool? Scanned { get; set; }
        string Serial { get; set; }
    }

}