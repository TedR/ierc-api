using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class DrawingModel : IDrawingModel
    {


        public DrawingModel()
        {
        }

        public int DA_IX { get; set; }
        public int DA_RCH_IX { get; set; }
        public string DA_Doc_Type { get; set; }
        public string DA_File_Path { get; set; }
        public string DA_Origin { get; set; }
        public string DA_Drawing_No { get; set; }
        public DateTime? DA_Date { get; set; }
        public int DA_Qty_Issued { get; set; }


    }

}