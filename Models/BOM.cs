using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public class BOM: IBOM
    {

        public BOM()
        {
        }

        public string Placement { get; set; }
        public string RefDes { get; set; }
        public string RP_PIN { get; set; }
        public string RP_Original_PIN { get; set; }
        public string ClientPart { get; set; }
        public int cur_qty { get; set; }
        public int SplitQty { get; set; }
        public string GPIN_Description { get; set; }
        public string PID_Package { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturersPartNumber { get; set; }


    }

}