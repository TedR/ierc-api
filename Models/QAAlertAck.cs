using System;

namespace iERC_API.Controllers

{
    public class QAAlertAck : IQAAlertAck
    {

        public QAAlertAck()
        {
        }
        
        public int QAA_IX { get; set; }
        public int QAA_QA_IX { get; set; }
        public int QAA_QPL_IX { get; set; }
        public int QAA_RCH_IX { get; set; }
        public int QAA_PRC_IX { get; set; }
        public int QAA_QAT_IX { get; set; }
        public int QAA_UN_IX { get; set; }
        public string QAA_By { get; set; }
        public DateTime? QAA_Date { get; set; }

    }

}