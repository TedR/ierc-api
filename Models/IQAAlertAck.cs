using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IQAAlertAck   
    {
        int QAA_IX { get; set; }
        int QAA_QA_IX { get; set; }
        int QAA_QPL_IX { get; set; }
        int QAA_RCH_IX { get; set; }
        int QAA_PRC_IX { get; set; }
        int QAA_QAT_IX { get; set; }
        int QAA_UN_IX { get; set; }
        string QAA_By { get; set; }
        DateTime? QAA_Date { get; set; }

    }

}
