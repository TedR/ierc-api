using System;
using Microsoft.AspNetCore.Mvc;

namespace iERC_API.Controllers

{
    public interface IPanelLayout
    {

        int PCB_IX { get; set; } 
        int PCB_RCH_IX { get; set; }
        string PCB_Category { get; set; }
        string PIT_Item { get; set; }
        string PCB_Group { get; set; }
        string PCB_Item_Description { get; set; }
        int PCB_Item_Number { get; set; }
        double PCB_X_Value { get; set; } 
        double PCB_Y_Value { get; set; } 
        double PCB_Thickness { get; set; } 
        double PCB_Diameter { get; set; } 
        int PCB_Style { get; set; }
 

    }

}