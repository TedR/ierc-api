namespace iERC_API.Controllers

{
    public class PanelScanMultiplier : IPanelScanMultiplier
    {

        public PanelScanMultiplier()
        {
        }

        public int PSN_IX { get; set; }
        public int PSN_QOB { get; set; }
        public int PSN_PIN_IX { get; set; }
        public int PIN_Panel_Number { get; set; }
        public bool PIN_Validated { get; set; }
    }


}