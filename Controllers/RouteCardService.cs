using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace iERC_API.Controllers
{


    [Route("api/[controller]")]
    [ApiController]

    public class RouteCardService : ControllerBase
    {

        private readonly IRouteCardRepository _routeCardRepo;

        public RouteCardService(IRouteCardRepository routeCardRepo)
        {
            _routeCardRepo = routeCardRepo;
        }

        #region POST ROUTINES


        /// <summary>
        /// ** Provides Secure AD Login **
        /// </summary>   
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "SecureLogin")]   
        public async Task<ActionResult<LoginReturn>> SecureLogin(LoginCredentialsClass userCredentials)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Ask for Authentication
                return await Task.Run(() => _routeCardRepo.SecureLogin(userCredentials));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Updates Device Details following successful login **
        /// </summary>   
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateDevice")]
        public async Task<LoginReturn> Device(LoginCredentialsClass userCredentials)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Update Device tokens
                return await Task.Run(() => _routeCardRepo.AddDeviceDetails(userCredentials));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Adds Request Notification to SQL **
        /// </summary>   
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddRequestNotification")]
        public async Task<POSTStatus> AddRequestNotification(RequestNotification requestNotification)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Insert User Request Notification
                return await Task.Run(() => _routeCardRepo.AddRequestNotification(requestNotification));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Updates Request Notification to SQL **
        /// </summary>   
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateRequestNotification")]
        public async Task<POSTStatus> UpdateRequestNotification(RequestNotification requestNotification)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Insert User Request Notification
                return await Task.Run(() => _routeCardRepo.UpdateRequestNotification(requestNotification));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;            
        }


        /// <summary>
        /// ** Updates Works Instruction **
        /// </summary>   
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateInstruction")]
        public async Task<POSTStatus> UpdateInstruction(RCI_Instruction instruction)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Invoke update request
                return await  _routeCardRepo.UpdateInstruction(instruction);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to add a comment to any process step **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddProcessComment")]
        public async Task<ActionResult<POSTStatus>> AddProcessComment(ProcessComment processComment)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddProcessComment(processComment);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to update Feedback comments **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateFeedback")]
        public async Task<ActionResult<POSTStatus>> UpdateFeedback(FeedbackModel feedback)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.UpdateFeedback(feedback);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to record User Acceptance of a notification **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddUAConfirmation")]
        public async Task<ActionResult<POSTStatus>> AddUAConfirmation(UAConfirmation uac)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddUAConfirmation(uac);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Provides Secure AD Login **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateShortages")]
        public async Task<ActionResult<POSTStatus>> UpdateShortages(Shortage shortage)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.UpdateShortagesAsync(shortage);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to record shortage  **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "ReportShortages")]
        public async Task<ActionResult<POSTStatus>> ReportShortages(Shortage shortage)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddShortagesAsync(shortage);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to record scan data capture **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "DataCapture")]
        public async Task<ActionResult<POSTStatus>> DataCapture(ScanHistory history)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddScanHistory(history);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to record user sign off **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddUserSignoff")]
        public async Task<ActionResult<POSTStatus>> AddUserSignoff(SignOff signOff)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddUserSignoff(signOff);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to update user sign off **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UpdateUserSignoff")]
        public async Task<ActionResult<POSTStatus>> UpdateUserSignoff(SignOff signOff)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.UpdateUserSignoff(signOff);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }



        /// <summary>
        /// ** Function to record user data capture **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UserDataCapture")]
        public async Task<ActionResult<POSTStatus>> UserDataCapture(ScanHistory history)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddUserDataHistory(history);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to record user sign off **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "UserConfirmationCapture")]
        public async Task<ActionResult<POSTStatus>> UserConfirmationCapture(ScanHistory history)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddUserConfirmationHistory(history);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to record time monitor event **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddTimeMonitor")]
        public async Task<ActionResult<POSTStatus>> AddTimeMonitor(TimeMonitor timeMonitor)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddTimeMonitor(timeMonitor);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to add usage of consumable item **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "AddConsumable")]
        public async Task<ActionResult<POSTStatus>> AddConsumable(Consumable consumable)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.AddConsumable(consumable);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        #endregion


        #region GET ROUTINES

        /// <summary>
        /// ** Function to check for previous process scan **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "CheckPreviousProcessScan/{prcIndex}/{snIndex}/{scanType}/{panelIndex}")]
        public async Task<ActionResult<PreviousScanInfo>> CheckPreviousProcessScan(Int32 prcIndex, Int32 snIndex, Int32 scanType, Int32 panelIndex)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Translate ScanType
                // If 1 or 5 then pass 1, Samples omitted from validation
                // 2, Inspection not used
                // If Panel then pass 6 == scanType param
                var _scanType = (scanType == 1 || scanType == 5) ? 1 : scanType;

                return await _routeCardRepo.CheckPreviousProcessScan(prcIndex, snIndex, _scanType, panelIndex);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to check for previous batch scan **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "CheckPreviousBatchScan/{prcIndex}/{bhIndex}")]
        public async Task<ActionResult<PreviousBatchInfo>> CheckPreviousBatchScan(Int32 prcIndex, Int32 bhIndex)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Translate
                return await _routeCardRepo.CheckPreviousBatchScan(prcIndex, bhIndex);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }
        
        /// <summary>
        /// ** Function to translate client serial number **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "TranslateClientSerial")]
        public async Task<ActionResult<String>> Translate(PSNSerial serial)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Translate
                return await _routeCardRepo.TranslateSerial(serial);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to get serial **
        /// ** Returns base level serial or top level if bonded **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "TopLevelSerial")]
        public async Task<ActionResult<String>> TopLevelSerial(PSNSerial serial)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Translate
                return await _routeCardRepo.GetTopLevelSerial(serial);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return sign offs **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetSignOffs/{authorisedUser}/{batchNumber}/{showCompleted}")]
        public async Task<ActionResult<List<SignOff>>> GetSignOffs(int authorisedUser, int batchNumber, bool showCompleted)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Sign offs ...
                return await _routeCardRepo.GetSignOffs(authorisedUser, batchNumber, showCompleted);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return user roles **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetUserRoles/{elevated}")]
        public async Task<ActionResult<List<UserRoles>>> GetUserRoles(Boolean elevated)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get User Roles
                return await _routeCardRepo.UserRoles(elevated);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return Request Notifications **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetRequestNotifications/{status}")]
        public async Task<ActionResult<List<RequestNotification>>> GetRequestNotifications(String status)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get User Request Notifications
                return await _routeCardRepo.RequestNotifications(status);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to return drawings **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetDrawings/{hdrIX}")]
        public async Task<ActionResult<List<DrawingModel>>> GetDrawings(int hdrIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Drawings for Job
                return await _routeCardRepo.Drawings(hdrIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return drawings by Process **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetDrawingsByProcess/{hdrIX}/{prcIX}")]
        public async Task<ActionResult<List<DrawingModel>>> GetDrawingsByProcess(int hdrIX, int prcIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Drawings for Process
                return await _routeCardRepo.DrawingsByProcess(hdrIX, prcIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return capacity planner nodes **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetPlannerNodes/{batch}")]
        public async Task<ActionResult<List<ProdPlan>>> GetPlannerNodes(int batch)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Capacity Planner Nodes
                return await _routeCardRepo.PlannerNodes(batch);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return live Route Card Batches **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetLiveBatches/{rca}/{vis}/{alloc}/{period}/{batch}")]
        public async Task<ActionResult<List<LiveBatches>>> GetLiveBatches(Boolean rca, Boolean vis, Boolean alloc, int period, int batch)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Batches, configurable if Route Card = Active, Visibility Status in PinParts.
                return await _routeCardRepo.LiveBatches(rca, vis, alloc, period, batch);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return processes **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetProcesses/{batch}/{tlp}/{un_ix}/{myProc}")]
        public async Task<ActionResult<List<ProcessModel>>> GetProcesses(int batch, int tlp, int un_ix, Boolean myProc)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Processes
                return await _routeCardRepo.Processes(batch, tlp, un_ix, myProc);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return user role filtered jobs **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetFilteredProcesses/{batch}/{un_ix}/{myProc}")]
        public async Task<ActionResult<List<ProcessModel>>> GetFilteredProcesses(int batch, int un_ix, Boolean myProc)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Processes
                return await _routeCardRepo.FilteredProcesses(batch, un_ix, myProc);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return UA Confirmation **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetUAConfirmation/{hdrIX}/{userID}/{refIX}/{stepIX}")]
        public async Task<ActionResult<Boolean>> GetUAConfirmation(int hdrIX, int userID, int refIX, int stepIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get UAC
                return await _routeCardRepo.UAConfirmation(hdrIX, userID, refIX, stepIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return GNCS **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetGNCs/{pin}/{period}/{userID}")]
        public async Task<ActionResult<List<GNC>>> GetGNCs(string pin, int period, int userID) 
        {
            try
            {   
                var decodedPin = System.Web.HttpUtility.UrlDecode(pin).Trim();
                new RouteCardService(_routeCardRepo);
                // Get open GNCs
                return await _routeCardRepo.GNCs(decodedPin, period, userID);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return process comments **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetProcessComments/{hdrIX}/{stepIX}")]
        public async Task<ActionResult<List<ProcessComment>>> GetProcessComments(int hdrIX, int stepIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get process comments for step ID
                return await _routeCardRepo.GetProcessComments(hdrIX, stepIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return legacy mapping **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetLegacyMapping/{prs_idx}/{prs_step_ix}")]
        public async Task<ActionResult<List<LegacyMap>>> GetLegacyMapping(int prs_idx, int prs_step_ix)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get mappings for this process
                return await _routeCardRepo.GetLegacyMapping(prs_idx, prs_step_ix);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return feedback **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetFeedback/{hdrIX}")]
        public async Task<ActionResult<List<FeedbackModel>>> GetFeedback(int hdrIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get feedback for route card header index
                return await _routeCardRepo.GetFeedback(hdrIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return shortages **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetShortages/{hdrIX}/{prcIX}")]
        public async Task<ActionResult<List<Shortage>>> GetShortages(int hdrIX, int prcIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get shortages for batch
                return await _routeCardRepo.GetShortagesAsync(hdrIX, prcIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to return time monitor lookup reasons **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetTimeReasons/{showAll}")]
        public async Task<ActionResult<List<TimeReason>>> GetTimeReasons(Boolean showAll)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Time Reason Codes
                return await _routeCardRepo.GetTimeReasons(showAll);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return monitor time states **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetTimeState/{showAll}")]
        public async Task<ActionResult<List<TimeState>>> GetTimeState(Boolean showAll)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Time Reason Codes
                return await _routeCardRepo.GetTimeState(showAll);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return time monitor events **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetTimeMonitor/{rch}/{prc}/{rci}/{userID}")]
        public async Task<ActionResult<List<TimeMonitor>>> GetTimeMonitor(int rch, int prc, int rci, int userID)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Stoppage Times
                return await _routeCardRepo.GetTimeMonitor(rch, prc, rci, userID);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return consumables **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetConsumables/{rch}/{prc}/{rci}")]
        public async Task<ActionResult<List<Consumable>>> GetConsumables(int rch, int prc, int rci)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Consumables
                return await _routeCardRepo.GetConsumable(rch, prc, rci);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return trace code **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetTraceCode")]
        public async Task<ActionResult<List<TraceCode>>> GetTraceCode(TraceCode traceCode)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Trace Code
                return await _routeCardRepo.GetTraceCode(traceCode);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to get QOB for given serial **
        /// </summary>  
        [Obsolete]
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "SerialQOB/{serial}")]
        public async Task<ActionResult<Int32>> SerialQOB(Int32 serial)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get QOB
                return await _routeCardRepo.GetSerialQOB(serial);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to get QOB and Panel Scan Index for given serial **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "SerialPanelScanMultiplier/{serial}")]
        public async Task<ActionResult<PanelScanMultiplier>> SerialPanelScanMultipler(Int32 serial)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get QOB + Panel Index
                return await _routeCardRepo.GetPanelScanMultiplier(serial);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return Measurements **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetMeasurements/{prs_ix}")]
        public async Task<ActionResult<List<Measurement>>> GetMeasurements(int prs_ix)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.GetMeasurement(prs_ix);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to Insert Measurement **
        /// </summary>  
        [HttpPost]
        [Route(template: "InsertMeasurement")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult<List<Measurement>>> InsertMeasurement(Measurement measurement)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.InsertMeasurement(measurement);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to return PCBA Panel Layout **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetPanelLayout/{rch}")]
        public async Task<ActionResult<List<PanelLayout>>> GetPanelLayout(int rch)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get Panel Layout Details + FIDS
                return await _routeCardRepo.GetPanelLayout(rch);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return scan history **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetScanHistory/{stepIX}/{stIX}/{bhIX}")]
        public async Task<ActionResult<List<ScanHistory>>> GetScanHistory(int stepIX, int stIX, int bhIX)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                // Get scan history for step/scan type/batch
                return await _routeCardRepo.GetScanHistoryAsync(stepIX, stIX, bhIX);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return AOI history **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetAOIs/{pin}")]
        public async Task<ActionResult<List<AOIImage>>> GetAOIs(string pin)
        {
            try
            {
                var decodedPin = System.Web.HttpUtility.UrlDecode(pin).Trim();
                new RouteCardService(_routeCardRepo);
                // Get AOI Images
                return await Task.Run(() => _routeCardRepo.AOIImages(decodedPin));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return AOI history by PMR **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetAOIByPMR/{pin}/{pmr}")]
        public async Task<ActionResult<List<AOIImage>>> GetAOIByPMR(string pin, string pmr)
        {
            try
            {
                var decodedPin = System.Web.HttpUtility.UrlDecode(pin).Trim();
                new RouteCardService(_routeCardRepo);
                // Get AOI Images by PMR
                return await Task.Run(() => _routeCardRepo.AOIImagesByPMR(decodedPin, pmr));
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return Image **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetImage")]
        public ActionResult GetImage(PCBImage image)
        {
            try
            {
               new RouteCardService(_routeCardRepo);
               return _routeCardRepo.GetImage(image);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return PDF **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetPDF")]
        public ActionResult GetPDF(PCBImage image)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return _routeCardRepo.GetPDF(image);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return Image Path **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetImagePath")]
        public String GetImagePath(PCBImage image)
        {
            try
            {                
                return image.Param1;
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

#region " QA Alerts"
    
#endregion
        /// <summary>
        /// ** Function to return QTY QA Alerts at Batch Level **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetQtyBatchQAAlert/{rchIndex}/{bhIndex}")]
        public async Task<ActionResult<Int32>> GetQtyBatchQAAlert(Int32 rchIndex, Int32 bhIndex)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.GetQtyBatchQAAlert(rchIndex, bhIndex);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to return QA Alerts at Batch Level **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetBatchLevelQAAlert/{rchIndex}/{bhIndex}")]
        public async Task<ActionResult<List<QAAlert>>> GetBatchLevelQAAlert(Int32 rchIndex, Int32 bhIndex)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.BatchLevelQAAlert(rchIndex, bhIndex);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to Insert QA Alert at Batch Level **
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "InsertAlertAck")]
        public async Task<ActionResult<POSTStatus>> InsertAlertAck(QAAlertAck alertAck) 
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.InsertQAAlert(alertAck);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to Close QA Alert at Batch Level **
        /// </summary>  
        [HttpPatch]
        [EnableCors("CorsPolicy")]
        [Route(template: "CloseQAAlert")]
        public async Task<ActionResult<POSTStatus>> CloseAlert(QAAlert alert)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.CloseQAAlert(alert);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return QA Alerts at Process Level **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetProcessLevelQAAlert/{rchIndex}/{bhIndex}/{userIndex}")]
        public async Task<ActionResult<List<QAAlert>>> GetProcessLevelQAAlert(Int32 rchIndex, Int32 bhIndex, Int32 userIndex)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.ProcessLevelQAAlert(rchIndex, bhIndex, userIndex);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        #region "BOM"


        /// <summary>
        /// ** Function to return BOM by Placement **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetBOM/{batch}/{placement}")]
        public async Task<ActionResult<List<BOM>>> GetBOM(Int32 batch, String placement)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.GetBOM(batch, placement);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// ** Function to return Pin data by batch number **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetBomPinData/{batch}")]
        public async Task<ActionResult<List<BOMPin>>> GetBOMPinData(Int32 batch)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.GetBOMPinData(batch);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// ** Function to return Pin Placement by batch number **
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetPlacements/{batch}")]
        public async Task<ActionResult<List<Placement>>> GetPlacements(Int32 batch)
        {
            try
            {
                new RouteCardService(_routeCardRepo);
                return await _routeCardRepo.GetPlacement(batch);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        #endregion

    }
    #endregion


}

